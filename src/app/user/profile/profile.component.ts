import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { animate, style, transition, trigger } from '@angular/animations';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import '../../../assets/echart/echarts-all';
import { ProfileService, ProfileDetail } from './profile.service';
import { NgForm, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Result } from '../../services/services.service';
import { ServicesService, Generic } from '../../services/services.service';

@Component({
	selector: 'app-profile',
	templateUrl: './profile.component.html',
	styleUrls: ['./profile.component.css'],
	animations: [
		trigger('fadeInOutTranslate', [
			transition(':enter', [
				style({ opacity: 0 }),
				animate('400ms ease-in-out', style({ opacity: 1 }))
			]),
			transition(':leave', [
				style({ transform: 'translate(0)' }),
				animate('400ms ease-in-out', style({ opacity: 0 }))
			])
		])
	]
})

export class ProfileComponent implements OnInit {

	private UserForm: NgForm;
	editProfile = true;
	editProfileIcon = 'icofont-edit';

	editAbout = true;
	editAboutIcon = 'icofont-edit';

	public editor;
	public editorContent = '';
	public editorConfig = {
		placeholder: 'About Your Self'
	};

	_bussiness: any;
	public data: any;
	public ContactData: any;
	public EnterpriseData: any;
	public rowsOnPage = 10;
	public filterQuery = '';
	public sortBy = '';
	public sortOrder = 'desc';
	public result: Result;
	public sucursal: string = '';
	public sucursalLab: string = '';
	public form: FormGroup;

	profitChartOption: any;
	position = 'botton-right';
	title: string;
	msg: string;
	showClose = true;
	timeout = 5000;
	theme = 'bootstrap';
	type = 'default';

	constructor(private fb: FormBuilder, private toastyService: ToastyService, private ProfileService: ProfileService, private docServ: ServicesService) {
	}

	ngOnInit() {
		setTimeout(() => {
			this.editorContent = this.editorContent;
		}, 2800);
		this.getSucursales();
		this.prepareData();
		this.getListBussines();
		this.form = this.newElement();
	}

  /**
   * Prepara el formulario
   */
	newElement() {
		return this.fb.group({
			nueva: [null, Validators.compose([Validators.required])],
			confirma: [null, Validators.compose([Validators.required])]
		});
	}

  /**
   * Obtiene los datos
   */
	private prepareData() {
		this.data = JSON.parse(this.ProfileService.getProfile());
		this.ContactData = JSON.parse(this.ProfileService.getContactData());
		let as = JSON.parse(sessionStorage.getItem("EntrepriseDataJSON"));
		var arreglodatos = [];
		for (var i = 0; i < as.length; i++) {
			arreglodatos.push(as[i]['dato']);
		}
		this.EnterpriseData = arreglodatos;
	}

  /**
   * Obtiene las sucursales del contacto (Perfil)
   */
	private getSucursales() {
		this.docServ.sucursal().subscribe(sucursal => {
			if (sucursal) {
				for (var i = 0; i < sucursal.length; i++) {
					if (i == 0) {
						this.sucursal = sucursal[i]["label"];
					} else {
						this.sucursal += ',' + sucursal[i]["label"];
					}
				}
				this.sucursalLab = sucursal[0].label;
			}
		}, (err) => {
			console.error(err);
		});
	}

	toggleEditProfile() {
		this.editProfileIcon = (this.editProfileIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
		this.editProfile = !this.editProfile;
	}

	toggleEditAbout() {
		this.editAboutIcon = (this.editAboutIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
		this.editAbout = !this.editAbout;
	}

	onEditorBlured(quill) {
		console.log('editor blur!', quill);
	}

	onEditorFocused(quill) {
		console.log('editor focus!', quill);
	}

	onEditorCreated(quill) {
		this.editor = quill;
		console.log('quill is ready! this is current quill instance object', quill);
	}

	onContentChanged({ quill, html, text }) {
		console.log('quill content is changed!', quill, html, text);
	}

  /**
   * Guarda los datos
   * @param datos
   */
	saveUser(datos: NgForm) {
		let perfil: ProfileDetail = {
			name: datos.controls['name'].value,
			Correo_Electronico: datos.controls['mail'].value,
			R8302383: datos.controls['city'].value,
			Telefono_movil: datos.controls['tel'].value
		};

		this.ProfileService.saveReq(perfil).subscribe(perfil => {
			this.result = perfil;
			if (perfil.status === 'ok') {
				this.addToast(
					{
						title: 'Acción exitosa',
						msg: 'Los datos se editaron exitosamente',
						timeout: 5000,
						theme: 'default',
						position: 'bottom-right',
						type: 'success'
					});
			} else {
				this.addToast(
					{
						title: 'Error en el proceso',
						msg: 'No fue posible procesar la solicitud',
						timeout: 5000,
						theme: 'default',
						position: 'bottom-right',
						type: 'error'
					});
			}
		}, (err) => {
			console.error(err);
		});
		this.toggleEditProfile();
	}

	addToast(options) {
		if (options.closeOther) {
			this.toastyService.clearAll();
		}
		this.position = options.position ? options.position : this.position;
		const toastOptions: ToastOptions = {
			title: options.title,
			msg: options.msg,
			showClose: options.showClose,
			timeout: options.timeout,
			theme: options.theme,
			onAdd: (toast: ToastData) => {
				console.log('Toast ' + toast.id + ' has been added!');
			},
			onRemove: (toast: ToastData) => {
				console.log('Toast ' + toast.id + ' has been added removed!');
			}
		};

		switch (options.type) {
			case 'default': this.toastyService.default(toastOptions); break;
			case 'info': this.toastyService.info(toastOptions); break;
			case 'success': this.toastyService.success(toastOptions); break;
			case 'wait': this.toastyService.wait(toastOptions); break;
			case 'error': this.toastyService.error(toastOptions); break;
			case 'warning': this.toastyService.warning(toastOptions); break;
		}
	}

	getListBussines() {
		this.ProfileService.getListBussinesByUser().subscribe(bussines => {
			this._bussiness = bussines;
		}, (err) => {
			console.error(err);
		});
	}
	get bussines() {
		return this._bussiness;
	}

	addNewRow(form: any) {
		if (form.confirma != form.nueva) {
			alert('La nueva contraseña debe coincidir con la contraseña de confirmación');
			return;
		}
		this.sendaction(form);
	}

	sendaction(form: any) {
		this.docServ.updatePassword(sessionStorage.getItem('emailLogin'), form.nueva).subscribe(Services => {
			if (Services.status === 'ok') {
				this.addToast({
					title: 'Acción exitosa',
					msg: 'Su contraseña a sido cambiada',
					timeout: 5000,
					theme: 'default',
					position: 'top-right',
					type: 'success'
				});
			} else {
				this.addToast({
					title: 'Error',
					msg: 'Se presentó un error en el cambio de contraseña, por favor intentelo de nuevo o informe el problema',
					timeout: 5000,
					theme: 'default',
					position: 'top-right',
					type: 'error'
				});
			}
		});
	}

}
