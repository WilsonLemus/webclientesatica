import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

export interface ProfileDetail {
    name: string;
    Correo_Electronico: string;
    Telefono_movil: string;
    R8302383: string;
}

export interface Generic {
    id: Number;
    label: String;
}

export interface Result {
    status: string;
    result: String;
}

export interface Profile {
    Services: Array<ProfileDetail>;
}

@Injectable()
export class ProfileService {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    private listProfile(): Observable<any> {
        let base;
        var query = this.auth.getId();
        query = encodeURI(query);
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=1000&output=json&query=` + query + `&sessionId=` + this.auth.getToken()); // , { headers: headers });
        const request = base.pipe(
            map((data: Profile) => {
                return data;
            })
        );
        return request;
    }

    private generics(type: 'groupers' | 'materials' | 'packages'): Observable<any> {
        let base;
        base = this.http.get(`assets/json/${type}.json`);
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    public getProfile() {
        return (this.auth.GetProfile());
    }

    public getContactData() {
        return (this.auth.GetContactData());
    }
    public getEnterpriseData() {
        return (this.auth.GetEnterpriseData());
    }

    public getListBussinesByUser(): Observable<any> {
        let base;
        const userId = this.auth.getIdUserLoggedIn();
        const token = this.auth.getToken();
        let lineasDeNegocio = sessionStorage.getItem("LineasDeNegocio");
        //  base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + userId + '&relName=R8156824&fieldList=id,Nombre_Linea,Descripcin_Lnea_de_Negocio,Mostrar&sessionId=' + token + '&output=json');
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select  name,Descripcin_Lnea_de_Negocio from Linea_de_Negocio WHERE id in(${lineasDeNegocio})&sessionId=` + token + '&output=json&maxRows=2000'); // =a858106871ad4e6888f46abe2a7305bb@8012571&output=json&maxRows=2000
        const request = base.pipe(
            map((data: Document) => {
                return data;
            })
        );
        return request;
    }



    private createRequest(Contacto: ProfileDetail): Observable<any> {
        const token = this.auth.getToken();
        let base;
        let url = this.getUrlRequest(token, Contacto);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                if (data.status == "ok") {
                    return data;
                } else {
                    return Observable.throw("Error al guardar los datos");
                }
            })
        );
        return request;
    }

    private getUrlRequest(token: string, Contacto: ProfileDetail) {
        let url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Contactos&sessionId=` + token;
        if ((Contacto.name != undefined) && (Contacto.name != "")) {
            url += '&name=' + Contacto.name;
        }
        if ((Contacto.Correo_Electronico != undefined) && (Contacto.Correo_Electronico != "")) {
            url += '&Correo_Electronico=' + Contacto.Correo_Electronico;
        }
        if ((Contacto.Telefono_movil != undefined) && (Contacto.Telefono_movil != "")) {
            url += '&Telefono_movil=' + Contacto.Telefono_movil;
        }
        if ((Contacto.R8302383 != undefined) && (Contacto.R8302383 != "")) {
            url += '&R8302383=' + Contacto.R8302383; //CIUDAD;
        }
        url += '&id=' + JSON.parse(this.auth.GetProfile())[6];
        return url;
    }

    public list(): Observable<any> {
        return this.listProfile();
    }

    public grouper(): Observable<any> {
        return this.generics('groupers');
    }

    public materials(id: Number): Observable<any> {
        return this.generics('materials');
    }

    public package(id: Number): Observable<any> {
        return this.generics('packages');
    }

    public saveReq(service: ProfileDetail): Observable<any> {
        return this.createRequest(service);
    }
}
