import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {QuillEditorModule} from 'ngx-quill-editor';
import {HttpModule} from '@angular/http';
import {DataTableModule} from 'angular2-datatable';
import {AngularEchartsModule} from 'ngx-echarts';
import { AuthGuardService } from '../../authentication/auth-guard.service';
import { ProfileService } from './profile.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ServicesService } from '../../services/services.service';

export const profileRoutes: Routes = [
  {
    path: 'detalle',
    component: ProfileComponent,
    data: {
      breadcrumb: 'Perfil de usuario',
      icon: 'icofont-justify-all bg-c-green',
      status: true
    }
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(profileRoutes),
    SharedModule,
    FormsModule,
    QuillEditorModule,
    HttpModule,
    DataTableModule,
    AngularEchartsModule,
    AngularMultiSelectModule
  ],
  providers: [
    AuthGuardService,
    ProfileService,
    ServicesService
  ],
  declarations: [ProfileComponent]
})
export class ProfileModule { }
