import { Component, OnInit } from '@angular/core';
import { PqsService, Generic, PQS } from '../pqs.service';
import { Router } from '@angular/router';
import { getUrlEnvironment } from '../../shared/environment';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-datatables-pqs',
    templateUrl: './list.component.html',
})


// =========================================================================================================
// =========================================================================================================

//                     ESTE MODULO SE ENCUENTRA INHABILITADO HASTA NUEVO AVISO DEL CLIENTE.

// =========================================================================================================
// =========================================================================================================
export class ListPQS implements OnInit {
    public data: PQS;
    public generic: Generic;
    // VARIABLES TABLA
    public filterQuery = '';
    public filterQuerySuc = '';
    public filterQueryFecha = '';
    public rowsOnPage = 20;
    public sortBy = 'name';
    public sortOrder = 'desc';
    constructor(private PqsService: PqsService, private router: Router, private auth: AuthenticationService) { }

    ngOnInit() {
        this.PqsService.listHistorialPqs().subscribe(services => {
            this.data = services;
        }, (err) => {   
            console.error(err);
        });
    }
    detallePqs(pqs) {
        sessionStorage.setItem("idPqs", pqs.id);
        sessionStorage.setItem("fechaPqs", pqs.fecha);
        sessionStorage.setItem("nombrePqs", pqs.nombre);
        sessionStorage.setItem("tipoPqs", pqs.tipo);
        sessionStorage.setItem("procesoPqs", pqs.proceso);
        sessionStorage.setItem("estadoPqs", pqs.estado);
        sessionStorage.setItem("calificacionPqs", pqs.calificacion);
        sessionStorage.setItem("evidenciaPqs", pqs.evidencia);
        sessionStorage.setItem("existeEvidenciaPqs", pqs.existeEvidencia);
        sessionStorage.setItem("servicio", pqs.servicio);
        sessionStorage.setItem("calificada", pqs.calificada);
        this.router.navigate(['/pqs/detail-pqs']);
    }
    goToUrl(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Evidencia_PQS&id=` + id + "&fieldName=Adjunto&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }
    formatDate(fecha) {
        if (fecha) {
            let from: Date = new Date(fecha);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 10) ? '0' + from.getDate() : from.getDate();
            return from.getFullYear() + '/' + mon + '/' + day;
        }

        return '';
    }

}