import { Component, OnInit } from '@angular/core';
import { PqsService, PQSDetail, PlanTrabjo, Result, Generic } from '../pqs.service';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { getUrlEnvironment } from '../../shared/environment';
import { AuthenticationService } from '../../authentication/authentication.service';
import { isNull } from 'util';

@Component({
    selector: 'app-datatables-pqs',
    templateUrl: './listDetail.component.html',
})

// =========================================================================================================
// =========================================================================================================

//                     ESTE MODULO SE ENCUENTRA INHABILITADO HASTA NUEVO AVISO DEL CLIENTE.

// =========================================================================================================
// =========================================================================================================

export class ListDetailPQS implements OnInit {
    public data = [];
    public plan = [];
    public result: Result;
    public generic: Generic;
    public genericPlan: Generic;
    public calificada: any;
    public servicio: any;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    private observacionCliente = "";
    constructor(private PqsService: PqsService, private router: Router, private modalService: NgbModal, private toastyService: ToastyService, private auth: AuthenticationService) { }

    ngOnInit() {
        this.data = [{
            id: sessionStorage.getItem("idPqs"),
            fecha: sessionStorage.getItem("fechaPqs"),
            nombre: sessionStorage.getItem("nombrePqs"),
            tipo: sessionStorage.getItem("tipoPqs"),
            proceso: sessionStorage.getItem("procesoPqs"),
            estado: sessionStorage.getItem("estadoPqs"),
            calificacion: sessionStorage.getItem("calificacionPqs"),
            evidencia: sessionStorage.getItem("evidenciaPqs"),
            existeEvidencia: sessionStorage.getItem("existeEvidenciaPqs"),
            servicio: sessionStorage.getItem("servicio"),
            calificada: sessionStorage.getItem("calificada")
        }];
        this.PqsService.servicio(this.data[0].servicio).subscribe(servicio => {
            this.servicio = servicio;
        });
        this.PqsService.getPlanTrabajo(this.data[0].id).subscribe(planTrabajo => {
            for (let i = 0; i < planTrabajo.length; i++){
                if(planTrabajo[i].evidencia === undefined || planTrabajo[i].evidencia === null){
                    this.plan.push({
                        id: planTrabajo[i].id,
                        fecha: this.formatDate(planTrabajo[i].fecha),
                        nombre: planTrabajo[i].nombre,
                        estado: planTrabajo[i].estado,
                        compromiso: planTrabajo[i].compromiso,
                        fechaCierre: planTrabajo[i].fechaCierre,
                        existeEvidencia: 0
                    });
                } else {
                    this.plan.push({
                        id: planTrabajo[i].id,
                        fecha: this.formatDate(planTrabajo[i].fecha),
                        nombre: planTrabajo[i].nombre,
                        estado: planTrabajo[i].estado,
                        compromiso: planTrabajo[i].compromiso,
                        fechaCierre: planTrabajo[i].fechaCierre,
                        existeEvidencia: 1
                    });
                }
            }
        }, (err) => {
            console.error(err);
        });
    }
    getOut() {
        sessionStorage.removeItem("idPqs");
        sessionStorage.removeItem("fechaPqs");
        sessionStorage.removeItem("nombrePqs");
        sessionStorage.removeItem("tipoPqs");
        sessionStorage.removeItem("procesoPqs");
        sessionStorage.removeItem("estadoPqs");
        sessionStorage.removeItem("calificacionPqs");
        sessionStorage.removeItem("evidenciaPqs");
        sessionStorage.removeItem("existeEvidenciaPqs");
        sessionStorage.removeItem("servicio");
        sessionStorage.removeItem("calificada");
        setTimeout(() => {
            this.router.navigate(['pqs/pqs/historial-pqs']);
        }, 2000);
    }
    open(content) {
        this.modalService.open(content);
    }
    calificaPqs(id, calificacion, observacion) {
        this.PqsService.calificaPqs(id, calificacion, observacion).subscribe(Services => {
            this.result = Services;
            if (Services.status === 'ok') {
                this.addToast(
                    {
                        title: 'Acción exitosa',
                        msg: 'Calificación registrada!',
                        timeout: 5000,
                        theme: 'default',
                        position: 'bottom-right',
                        type: 'success'
                    });
            }
        })
        this.getOut();
    }
    guardarObservacion(args){
        this.observacionCliente = "'" + args + "'";
    }
    /**
    * Abre la evidencia
    * @param id
    */
    goToUrl(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Evidencia_PQS&id=` + id + "&fieldName=Adjunto&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }
    goToUrl2(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Tareas&id=` + id + "&fieldName=Evidencia&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

    formatDate(fecha) {
        if (fecha) {
            let from: Date = new Date(fecha);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 10) ? '0' + from.getDate() : from.getDate();
            return from.getFullYear() + '/' + mon + '/' + day;
        }

        return '';
    }

}