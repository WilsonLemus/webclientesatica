import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { PqsDetailComponent } from './detail/pqs.component';
import { ListPQS } from './list/list.component';
import { ListDetailPQS } from './listDetail/listDetail.component';
import { DataFilterPipe } from './data-filter.pipe';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { PqsRoutes } from './pqs.routing';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { DataTableModule } from 'angular2-datatable';
import { AngularEchartsModule } from 'ngx-echarts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PqsService } from './pqs.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        HttpModule,
        CommonModule,
        RouterModule.forChild(PqsRoutes),
        SharedModule,
        HttpClientModule,
        AuthenticationModule,
        DataTableModule,
        AngularEchartsModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        AuthGuardService,
        PqsService,
        NgbActiveModal,
        NgbModal,
        PqsDetailComponent,
    ],
    declarations: [PqsDetailComponent, ListPQS, ListDetailPQS, DataFilterPipe]
})

export class PqsModule { }