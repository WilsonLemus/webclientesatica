import { Routes } from '@angular/router';
import { PqsDetailComponent } from './detail/pqs.component';
import { ListPQS } from './list/list.component';
import { ListDetailPQS } from './listDetail/listDetail.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const PqsRoutes: Routes = [
  {
    path: 'pqs/crear-pqs',
    component: PqsDetailComponent,
    data: {
      breadcrumb: 'Crear PQS',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'pqs/historial-pqs',
    component: ListPQS,
    data: {
      breadcrumb: 'Historial de PQS',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'detail-pqs',
    component: ListDetailPQS,
    data: {
      breadcrumb: 'Detalle de PQS',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
]