import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { PqsService, Generic, Result } from '../pqs.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-pqs',
    templateUrl: './pqs.component.html',
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],

})

export class PqsDetailComponent implements OnInit {
    pqsForm = new FormGroup({
        tipoPqs: new FormControl(''),
        proceso: new FormControl(''),
        servicio: new FormControl(''),
        motivo: new FormControl(''),
        sucursal: new FormControl(''),
        detalle: new FormControl(''),
        evidencia: new FormControl(''),
    });
    public tipoPqs: any;
    public clienteProceso: any;
    public proceso: any;
    public servicio: any;
    public motivo: any;
    public sucursal: any;
    public detalle: any;
    public result: Result;
    public doc;
    public vall;
    public id;
    public generic: Generic;
    pqs: Array<Generic>;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;

    constructor(private PqsService: PqsService, private toastyService: ToastyService, private _fb: FormBuilder, private modalService: NgbModal, private router: Router, private auth: AuthenticationService) { }

    // Funciones que se realizaran al cargar la pantalla.

    ngOnInit() {
        this.pqsForm = this._fb.group({
            itemRows: this._fb.array([this.initItemRows()])
        });

        // obtiene el tipo de PQS
        this.PqsService.getTipoPqs().subscribe(tipoPqs => {
            this.tipoPqs = tipoPqs;
        }, (err) => {
            console.error(err);
        });
        //Obtiene el cliente de proceso
        this.PqsService.getClienteProceso().subscribe(clienteProceso => {
            this.PqsService.getProceso(clienteProceso[0][0]).subscribe(proceso => {
                this.proceso = proceso;
            }, (err) => {
                console.error(err);
            });
            this.clienteProceso = clienteProceso;
        }, (err) => {
            console.error(err);
        });

        //Obtiene la sucursal
        this.PqsService.getSucursal().subscribe(sucursal => {
            this.sucursal = sucursal;
        }, (err) => {
            console.error(err);
        });
    }
    open(content) {
        this.modalService.open(content);
    }
    /**
    * Inicia el formulario conb el modelo de solicitud
    */
    initItemRows() {
        return this._fb.group({
            tipoPqs: ['', [<any>Validators.required]],
            proceso: ['', [<any>Validators.required]],
            servicio: ['', [<any>Validators.required]],
            motivo: [''],
            sucursal: ['', [<any>Validators.required]],
            detalle: ['', [<any>Validators.required]],
            evidencia: [''],
        });
    }
    //Guardar y salir
    saveGetOut() {
        this.createPQS();
        // this.getOut();

    }
    getOut() {
        setTimeout(() => {
            this.router.navigate(['/pqs/pqs/historial-pqs']);
        }, 1000);
    }
    recarga() {
        location.reload();
    }
    //Obtiene el Servicio
    cargaServicio(idProceso) {
        this.PqsService.getServicio(idProceso).subscribe(servicio => {
            this.servicio = servicio;
        }, (err) => {
            console.error(err);
        });
    }
    // Obtener el Motivo
    cargaMotivo(idProcesoTipoMotivo) {
        this.PqsService.getIdServicio(idProcesoTipoMotivo).subscribe(idServicio =>{
            this.PqsService.getMotivo(idServicio).subscribe(motivo => {
                this.motivo = motivo;
            }, (err) => {
                console.error(err);
            });
        }, (err) => {
            console.error(err);
        });
    }

    volver(){
        this.router.navigate(['/dashboard']);
    }

    // Crea la PQS en Rollbase.

    createPQS() {
        const formlen = this.pqsForm.value.itemRows.length;
        this.PqsService.savePqs(this.pqsForm.value, formlen).subscribe(
            Services => {
                this.result = Services;
                let Mens = 'Su PQS se registro correctamente';

                if (Services.status === 'ok') {
                    this.PqsService.saveIdDocument(Services.id).subscribe(
                        doc => {
                            if (this.doc) {
                                this.doc.id = doc.id;
                                this.PqsService.loadDocument(this.doc);
                            }
                            this.result = doc;
                        }
                    );
                    this.addToast(
                        {
                            title: 'Acción exitosa',
                            msg: Mens,
                            timeout: 5000,
                            theme: 'default',
                            position: 'bottom-right',
                            type: 'success'
                        });
                    this.PqsService.getPqs(Services.id).subscribe(pqs => {
                        this.generic = pqs;
                    })
                }
            }
        )
    }

    // Guarda el documento en rollbase.

    saveDocumento(id) {
        if (this.doc) {
            this.doc.id = id;
            this.PqsService.loadDocument(this.doc);
        }
    }
    onFileChange(event) {
        // console.log("MY FILE: ", event.target.files)
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.doc = {
                    id: '',
                    fieldName: 'Adjunto',
                    objName: 'Evidencia_PQS',
                    fileName: file.name,
                    contentType: file.type,
                    value: reader.result.split(',')[1]
                }
                this.vall = (this.doc) ? this.doc.value : '';
            };
        }
    }

    // Notificaciones flotantes.

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
