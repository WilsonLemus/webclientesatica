import { Injectable } from "@angular/core";
import { getUrlEnvironment } from '../shared/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { map } from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

export interface Generic {
    id: number;
    label: string;
    data: string;
}

export interface PqsDetailList {
    id: String,
    fecha: String,
    nombre: String,
    tipo: String,
    proceso: String,
    estado: String,
    calificacion: String,
    servicio: String,
    calificada: String,
    evidencia: String
}

export interface PQS {
    Services: Array<PqsDetailList>;
}

export interface PqsDetail {
    tipo: string;
    proceso: string,
    servicio: string;
    motivo: string;
    sucursal: string;
    detalle: string;
    evidencia: string;
}
export interface Result {
    status: string;
    result: string;
    id: string;
}
export interface PQSDetail {
    fecha: string;
    no_pqs: string;
    tipoPqs: string,
    proceso: string;
    compromiso: string;
    estado: string;
    servicio: string;
    id: string;
}
export interface PlanTrabjo {
    name: string;
    estado: string;
    fecha: string;
    compromiso: string;
    proceso: string;
    servicio: string;
    cierre: string;
}



@Injectable()
export class PqsService {
    public id;
    public idLineaP;
    public generic: Generic;
    constructor(private http: HttpClient, private auth: AuthenticationService) { }

    // Trae el los tipos de PQS

    public getTipoPqs() {
        let url = `${getUrlEnvironment()}rest/api/getPicklist?output=json&objDefId=9483815&fieldDefId=9483935&sessionId=` + this.auth.getToken();
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae los procesos del cliente. 

    public getClienteProceso() {
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Cliente_Linea_Proceso WHERE R9961501=${cliente}  order by name&sessionId=` + this.auth.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae los Procesos del cliente

    public getProceso(idLineaP) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Linea_Proceso_Tipo WHERE  R9961507=${idLineaP} order by name &sessionId=` + this.auth.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae los servicios del cliente.

    public getServicio(idProceso) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Linea_Proceso_Tipo&id=` + idProceso + '&relName=R9962529&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae la id del servicio escodigo

    public getIdServicio(idProcesoTipoMotivo) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select R9962499 from Proceso_Tipo_Motivo WHERE  id=` + idProcesoTipoMotivo + ` &sessionId=` + this.auth.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae los motivos de los servicios

    public getMotivo(idServicio) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Tipo_de_producto_pqs&id=` + idServicio + `&relName=R9562872&fieldList=id,name&sessionId=` + this.auth.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae las sucursales del cliente.

    public getSucursal() {
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Sucursal1 WHERE R8173502=${cliente} order by name&sessionId=` + this.auth.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    public listHistorialPqs(): Observable<any> {
        return this.listHistorial();
    }

    private listHistorial(): Observable<any> {
        let base;
        const url = this.getQueryListHistorialPqs();
        base = this.http.get(url);
        const request = base.pipe(
            map((data: any) => {
                return this.processListPqs(data);
            })
        );
        return request;
    }

    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    private processListPqs(data: any) {
        var pqs = [];
        for (let i = 0; i < data.length; i++) {
            this.evidenciaPqs(data[i][2]).subscribe(evidencia => {
                if (!evidencia[0]) {
                    console.log("SIN EVIDENCIA");
                    var calificada;
                    if (data[i][8] === undefined || data[i][8] === null) {
                        calificada = 0;
                    } else {
                        calificada = 1;
                    }
                    let resu = {
                        id: data[i][0],
                        fecha: this.formatDate(data[i][1]),
                        nombre: data[i][2],
                        tipo: data[i][3],
                        proceso: data[i][4],
                        estado: data[i][5],
                        calificacion: data[i][6],
                        servicio: data[i][7],
                        calificada: calificada,
                        evidencia: 0,
                        existeEvidencia: 0
                    }
                    pqs.push(resu);
                } else {
                    console.log("CON EVIDENCIA");
                    var calificada;
                    if (data[i][8] === undefined || data[i][8] === null) {
                        calificada = 0;
                    } else {
                        calificada = 1;
                    }
                    this.generic = evidencia;
                    let resu = {
                        id: data[i][0],
                        fecha: this.formatDate(data[i][1]),
                        nombre: data[i][2],
                        tipo: data[i][3],
                        proceso: data[i][4],
                        estado: data[i][5],
                        calificacion: data[i][6],
                        servicio: data[i][7],
                        calificada: calificada,
                        evidencia: this.generic[0][0],
                        existeEvidencia: 1
                    }
                    pqs.push(resu);
                }
            });
        }

        return pqs;
    }

    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    private getQueryListHistorialPqs() {
        const contacto = JSON.parse(sessionStorage.getItem('contactoId')).value;
        var query = 'SELECT id, createdAt, name, Tipo_PQS';
        var query2 = 'value, Nombre_proceso, status';
        var query3 = 'value, Calificacion_cliente, R9562842, PQS_calificada, R9961708 from PQS WHERE R9493393=' + contacto + ' order by createdAt desc&sessionId=' + this.auth.getToken();
        let totQue = encodeURI(query) +
            encodeURIComponent('#') + encodeURI(query2) +
            encodeURIComponent('#') + encodeURI(query3);
        const url = `${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=1000&output=json&query=` + totQue;

        return url;
    }

    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    public evidenciaPqs(nombrePQS) {
        console.log("Nombre: " + nombrePQS);
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, Adjunto from Evidencia_PQS where Nombre_PQS='${nombrePQS}' &sessionId=` + this.auth.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: Generic) => {
                //    console.log(data);
                return data;
            })
        );
        return request;
    }
    public servicio(id) {
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select name from Tipo_de_producto_pqs where id=${id} &sessionId=` + this.auth.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: Generic) => {
                //    console.log(data);
                return data;
            })
        );
        return request;
    }

    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================


    public getPlanTrabajo(id) {
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select name,  status` + encodeURIComponent('#') + `value, 
        createdAt, Tarea, Fecha_de_cierre, id, Evidencia from Tareas where R9484387=${id}  order by name&sessionId=` + this.auth.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: any) => {
                let tot = [];
                for (let i = 0; i < data.length; i++) {
                    tot.push({
                        nombre: data[i][0],
                        estado: data[i][1],
                        fecha: this.formatDate(data[i][2]),
                        compromiso: data[i][3],
                        fechaCierre: this.formatDate(data[i][4]),
                        id: data[i][5],
                        evidencia: data[i][6]
                    });
                }
                return tot;

            })
        );
        return request;
    }


    // Crea la PQS en rollbase.

    private createPqs(pqs: any, formlen, ): Observable<any> {
        let base;
        for (let i = 0; i < formlen; i++) {
            let url = this.getQueryCreate(pqs, i);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }

    }

    // Trae el id de la PQS en rollbase
    public getPqs(id) {
        //console.log("ELDELPLAN", id);
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from PQS where id=${id} &sessionId=` + this.auth.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }


    // Crea la PQS en rollbase

    private getQueryCreate(pqs: any, i: number) {
        const token = this.auth.getToken();
        let idSucursal = sessionStorage.getItem('idSucursales');
        const cliente = this.auth.getIdUserLoggedIn();
        const contacto = JSON.parse(sessionStorage.getItem('contactoId')).value;
        const procedencia = 10994962;
        let params = `&Procedencia=${+procedencia}&R9493393=${+contacto}&Tipo_PQS=${+pqs.itemRows[i].tipoPqs}&R9960752=${+pqs.itemRows[i].proceso}&R9977557=${+pqs.itemRows[i].servicio}&R10821020=${+pqs.itemRows[i].motivo}&Detalle=${pqs.itemRows[i].detalle}&R10861025=${pqs.itemRows[i].sucursal}&R9493390=${cliente}&R9976048 in(${idSucursal})`;
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=PQS&sessionId=` + token + params;
        return url;
    }

    // Guarda el documento cargado en la PQS.

    private getSaveDocument(idPqs: any) {
        const url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Evidencia_PQS&sessionId=` + this.auth.getToken() + `&Descripcion=Evidencia&R9961708=` + idPqs;
        return url;
    }

    private saveDocument(id): Observable<any> {
        let base;
        try {
            let url = this.getSaveDocument(id);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    //   console.log(data);
                    return data;
                })
            );
            return request;

        } catch (err) {
            console.log(err);
        }
    }


    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    public calificaPqs(id: number, calificacion: string, observacion: string): Observable<any> {
        const url = `${getUrlEnvironment()}rest/api/updateRecord?output=json&useIds=true&objName=PQS&sessionId=` + this.auth.getToken() + `&id=` + id + `&Calificacion_cliente=` + calificacion + `&Observacion_Cliente=` + observacion;
        return this.http.post(url, null)
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });
    }

    public savePqs(pqs: any, Formlen: number): Observable<any> {
        return this.createPqs(pqs, Formlen);
    }
    public saveIdDocument(id): Observable<any> {
        return this.saveDocument(id);
    }

    encodeQueryData(data) {
        try {
            const ret = [];
            for (const d in data)
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            return ret.join('&');
        } catch (s) {
            alert('s ' + s);
        }
    }

    buildUrlParams(object) {
        try {
            const urlParams = [];
            for (const key in object) {
                if (object.hasOwnProperty(key)) {
                    urlParams.push(key + '=' + object[key]);
                }
            }
            return urlParams.join('&');
        } catch (r) {
            alert(r);
        }
    }
    // =========================================================================================================

    //                     FUNCIÓN INHABILITADA HASTA NUEVA ORDEN DEL CLIENTE

    // =========================================================================================================

    public loadDocument(content) {
        const tok = this.auth.getToken();
        var xhr = new XMLHttpRequest();
        var url = `${getUrlEnvironment()}rest/api/setBinaryData?output=json&sessionId=` + tok;
        var params = this.buildUrlParams(content);
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {

            }
        }
        var eqd = this.encodeQueryData(content);

        xhr.onprogress = function (e) {
            try {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                }
            } catch (f) {
                alert("f " + f);
            }
        };

        xhr.onload = function (a) {
            console.log("onload");
        };
        xhr.onerror = function () {
            console.log("error");
        };
        xhr.onabort = function () {
            console.log("abort");
        };
        xhr.ontimeout = function () {
            console.log("timeout");
        };
        xhr.onloadstart = function () {
        };
        xhr.onloadend = function () {
        };
        xhr.send(eqd);
    }
    formatDate(fecha) {
        if (fecha) {
            let from: Date = new Date(fecha);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 10) ? '0' + from.getDate() : from.getDate();
            return from.getFullYear() + '/' + mon + '/' + day;
        }

        return '';
    }
}