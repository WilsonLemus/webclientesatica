import { Injectable } from "@angular/core";
import { getUrlEnvironment } from '../shared/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { map } from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

export interface Generic {
    id: number;
    label: string;
    data: string;
}

export interface Result {
    status: string;
    result: string;
    id: string;
}


@Injectable()
export class EncuestasService {

    constructor(private http: HttpClient, private auth: AuthenticationService) { }

    // Trae las sucursales de rollbase.

    private Sucursales() {
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + cliente + '&relName=R8173502&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Guarda la id de la sucursal en un item de sesión.

    private IdSucursal(ans: any, Formlen: number) {
        for (let i = 0; i < Formlen; i++) {
            sessionStorage.setItem('idSucursalAns', ans.itemRows[i].sucursalAns);
            return 0;
        }
    }

    // Trae la ide de la sucursal del item de sesión.

    private obtenerIdSucursal() {
        let idSucursal = sessionStorage.getItem("idSucursalAns");
        return idSucursal;
    }

    // Trae los servicios de rollbase.

    private Servicios() {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT id,name FROM Tipo_de_producto_pqs&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae el año de creacion y estado de la encuesta de la sucursal seleccionada.

    private comprobarEncuesta(idSucursal){
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT anio_creacion,status FROM Encuestas WHERE R10950995=`+ idSucursal +`&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Crea la encuesta en rollbase.

    private createEncuesta(Encuestas = [], idSucursal): Observable<any> {
        let base;
        let url = this.getQueryCreateEncuesta(Encuestas, idSucursal);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getQueryCreateEncuesta(Encuestas = [], idSucursal) {
        const token = this.auth.getToken();
        var param2 = '&R10951235=';
        console.log(Encuestas);
        for (let i = 0; i < Encuestas.length; i++) {
            var escrito;
            if (i == Encuestas.length - 1) {
                escrito = Encuestas[i];
            } else {
                escrito = Encuestas[i] + ",";
            }
            param2 = param2 + escrito;
        }
        let params = `&R10950995=${+idSucursal}` + param2;
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Encuestas&sessionId=` + token + params;
        return url;
    }

    // Crea el consecutivo de la encuesta en rollbase.

    private createConsecutivo(deudor, regional, idEncuesta) {
        if ((regional == 10859164) || (regional == 8214444) || (regional == 8416559) || (regional == 8433390)) //bogota
        {
            regional = "01";
        }
        else if ((regional == 8214447) || (regional == 8416560) || (regional == 8416554)) //medellin
        {
            regional = "02";
        } else if ((regional == 8214449) || (regional == 8416561) || (regional == 8416556)) //cali
        {
            regional = "03";
        } else if ((regional == 8214452) || (regional == 8416562) || (regional == 8416557)) //barranquilla
        {
            regional = "04";
        } else if ((regional == 8214467) || (regional == 8416563) || (regional == 8416558)) //bucaramanga
        {
            regional = "05";
        } else if (regional == 8214456) //cartagena
        {
            regional = "06";
        }
        console.log("Regional: " + regional);
        console.log("Deudor: " + deudor);
        console.log("Id Encuesta: " + idEncuesta);
        var año= new Date();
        var fechaCreacion=año.getFullYear();
        var consecutivo = "ES"+"-"+deudor+"-"+regional+"-"+fechaCreacion;
        console.log("Consecutivo: " + consecutivo);
        let base;
        let url = this.getQueryCreateConsecutivo(consecutivo, idEncuesta);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getQueryCreateConsecutivo(consecutivo, idEncuesta) {
        const token = this.auth.getToken();
        let params = `&Cons_encuesta=${consecutivo}`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Encuestas&id=${idEncuesta}&sessionId=` + token + params;
        return url;
    }

    // Trae el id de la encuesta, teniendo en cuenta el año, la fecha de creación.

    private getIdEncuesta(idSucursal){
        var año= new Date();
        var fechaCreacion=año.getFullYear();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT id FROM Encuestas WHERE anio_creacion = ` + fechaCreacion + ` and R10950995=`+ idSucursal +`&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
                
            })
        );
        return request;
    }

    // Trae la regional del cliente en rollbase.

    private getRegional() {
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT R8213751 FROM Clientes WHERE id = ` + cliente + `&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // trae el deudor del cliente en rollbase.

    private getDeudor() {
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT Deudor FROM Clientes WHERE id = ` + cliente + `&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae las preguntas relacionadas con los servicios seleccionados.

    private obtenerPreguntas(){
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT id,Pregunta,R10936778 FROM Pregunta&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Trae los servicios de la encuesta.

    private obtenerServiciosEncuesta(){
        let idEncuesta = sessionStorage.getItem('idEncuesta');
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Encuestas&id=` + idEncuesta + `&relName=R10951235&fieldList=id,name&sessionId=` + this.auth.getToken() + '&output=json');
        // base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT R10951235 FROM Encuestas WHERE Cons_encuesta = '` + idEncuesta +`'&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
                
            })
        );
        return request;
    }

    // GUARDAR ENCUESTA

    private letEncuesta(calificacion = []){
        let base;
        let idEncuesta = sessionStorage.getItem('idEncuesta');
        for (let i = 0; i < calificacion.length; i++){
            let url = this.getSetQuery(idEncuesta, calificacion, i);
            // console.log(url);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }
    }

    private getSetQuery(idEncuesta, calificacion = [], i: number){
        const token = this.auth.getToken();
        let params = `&R10933057=${+calificacion[i].id}&Observaciones=${calificacion[i].observacion}&Calficacion_de_la_pregunta=${+calificacion[i].calificacion}&R10952036=${idEncuesta}&R10952209=${+calificacion[i].servicio}`;
        // console.log(params);
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Evaluacin_encuesta_de_satisfaccin&sessionId=` + token + params;
        return url; 
    }

    // Cambia el estado de calificacion de la encuesta.

    private encuestaCalificada(){
        let base;
        let encuesta = sessionStorage.getItem('idEncuesta');
        let url = this.getCheckQuery(encuesta);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getCheckQuery(idEncuesta){
        const token = this.auth.getToken();
        let params = `&Encuesta_calificada=1`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Encuestas&id=${idEncuesta}&sessionId=` + token + params;
        return url;
    }

    // ACTUALIZAR PORCENTAJE ENCUESTA

    private actualizarPorcentajeEncuesta(porcentaje){
        let base;
        let encuesta = sessionStorage.getItem('idEncuesta');
        let url = this.getCheckQueryEncuesta(encuesta, porcentaje);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getCheckQueryEncuesta(idEncuesta, porcentaje){
        const token = this.auth.getToken();
        let params = `&Encuesta_calificada=true&status=12661219&Calificacion_de_encuesta=${porcentaje}`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Encuestas&id=${idEncuesta}&sessionId=` + token + params;
        return url;
    }

    // ACTUALIZAR PORCENTAJE ENCUESTA

    private guardarCreador(usuarioCreador){
        let base;
        let encuesta = sessionStorage.getItem('idEncuesta');
        let url = this.getCheckQueryCreador(encuesta, usuarioCreador);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getCheckQueryCreador(idEncuesta, usuarioCreador){
        const token = this.auth.getToken();
        let params = `&Email_interventor=${usuarioCreador}`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Encuestas&id=${idEncuesta}&sessionId=` + token + params;
        return url;
    }

    // METODOS PUBLIC

    public actualizarPorcentaje(porcentaje){
        return this.actualizarPorcentajeEncuesta(porcentaje);
    }

    public comprobarEncuestaValida(idSucursal){
        return this.comprobarEncuesta(idSucursal);
    }

    public guardarEncuesta(calificacion = []){
        return this.letEncuesta(calificacion);
    }

    public getServiciosEncuesta(){
        return this.obtenerServiciosEncuesta();
    }

    public getPreguntas(){
        return this.obtenerPreguntas();
    }

    public Regional(){
        return this.getRegional();
    }

    public Deudor(){
        return this.getDeudor();
    }

    public IdEncuesta(idSucursal){
        return this.getIdEncuesta(idSucursal);
    }

    public saveConsecutivo(deudor, regional, idEncuesta) {
        return this.createConsecutivo(deudor, regional, idEncuesta);
    }

    public guardarUsuarioCreador(usuarioCreador){
        return this.guardarCreador(usuarioCreador);
    }

    public saveEncuesta(Encuestas = [], idSucursal): Observable<any> {
        return this.createEncuesta(Encuestas, idSucursal);
    }

    public getSucursales() {
        return this.Sucursales();
    }

    public saveIdSucursal(ans: any, Formlen: number) {
        return this.IdSucursal(ans, Formlen);
    }

    public getIdSucursal() {
        return this.obtenerIdSucursal();
    }

    public getServicios() {
        return this.Servicios();
    }

    public checkEncuestaCalificada(){
        return this.encuestaCalificada();
    }

}