import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncuestasService, Generic, Result } from '../encuestas.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-pqs',
    templateUrl: './list.component.html',
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],

})

export class EncuestasListComponent implements OnInit {
    private data: any;
    private preguntas: any;
    private porcentajeAprobado: any;
    private preguntasEscogidas=[];
    private nivelServicio: any;
    private calificaciones = [];
    private porcentajeServicios = [];
    private rowsOnPage = 5;
    private sortBy = 'name';
    private sortOrder = 'desc';
    private result: Result;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    public porcentajeCliente = 0;

    constructor(private EncuestasService: EncuestasService, private toastyService: ToastyService, private _fb: FormBuilder, private modalService: NgbModal, private router: Router, private auth: AuthenticationService) { }

    ngOnInit() {
        this.obtenerPreguntas();
    }

    // Trae las preguntas dependiendo de los serivicos escogidos.

    obtenerPreguntas(){
        this.EncuestasService.getServiciosEncuesta().subscribe(servicios => {
            this.data = servicios;
                this.EncuestasService.getPreguntas().subscribe(preguntas => {
                    this.preguntas = preguntas;
                        for (let i = 0; i < this.data.length; i++){
                            for (let j = 0; j < this.preguntas.length; j++){
                                if (this.data[i].id == this.preguntas[j][2]){
                                    console.log(this.preguntas[j][1]);
                                    this.preguntasEscogidas.push({
                                        id: this.preguntas[j][0],
                                        pregunta: this.preguntas[j][1],
                                        servicio: this.data[i].id, 
                                        nombreServicio: this.data[i].name,
                                        aplica: 0
                                    });
                                    this.calificaciones.push({
                                        id: this.preguntas[j][0],
                                        calificacion: '0',
                                        observacion: '',
                                        aplica: 0,
                                        servicio: this.data[i].id
                                    }); 
                                }
                            }
                        }
                        this.crearVariablesServicio();
                }, (err) => {
                    console.log(err);
                });
        }, (err) => {
            console.log(err);
        })
    }

    // En un JSON guarda los porcentajes de los diferentes servicios que se califican en la encuesta.

    crearVariablesServicio(){
        for (let i = 0; i < this.preguntasEscogidas.length; i++){
            if (this.porcentajeServicios.length == 0){
                this.porcentajeServicios.push({
                    idServicio: this.preguntasEscogidas[i].servicio,
                    nombreServicio: this.preguntasEscogidas[i].nombreServicio,
                    porcentaje: 0
                });
            } else {
                var valido = false;
                for (let j = 0; j < this.porcentajeServicios.length; j++){
                    if (this.porcentajeServicios[j].idServicio != this.preguntasEscogidas[i].servicio){
                        valido = true;
                    } else {
                        valido = false;
                    }
                }
                if (valido) {
                    this.porcentajeServicios.push({
                        idServicio: this.preguntasEscogidas[i].servicio,
                        nombreServicio: this.preguntasEscogidas[i].nombreServicio,
                        porcentaje: 0
                    });
                }
            }
        }
        // console.log(this.porcentajeServicios);
    }

    // Realiza un porcentaje de los servicios.

    porcentajeCalificadoServicios(servicio){
        // console.log("SERVICIO: " + servicio);
        var calificacionTotal = 0;
        var aplicados = 0;
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].aplica == 0){
                if (this.calificaciones[i].servicio == servicio){
                    calificacionTotal = calificacionTotal + parseInt(this.calificaciones[i].calificacion);
                    aplicados++;
                }
            }
        }
        var formula = calificacionTotal/aplicados;
        var porcentajeServicio = Math.round(formula * 100)/100;
        for (let i = 0; i < this.porcentajeServicios.length; i++){
            if (this.porcentajeServicios[i].idServicio == servicio) {
                this.porcentajeServicios[i].porcentaje = porcentajeServicio;
            }
        }
        console.log(this.porcentajeServicios);
    }

    // Quita las preguntas que no aplican para la encuesta.

    quitarServicio(check){
        var split = check.split("-");
        var boolValue = (/true/i).test(split[1]) //returns true
        if (boolValue) {
            for (let i = 0; i < this.calificaciones.length; i++){
                if (this.calificaciones[i].id == split[0]) {
                    this.calificaciones[i].aplica = 1;
                    this.calificaciones[i].calificacion = '0';
                    this.selectReadOnly(split[0]);
                    this.porcentajeCalificado();
                    this.porcentajeCalificadoServicios(split[2]);
                    return;
                }
            }
        } else {
            for (let i = 0; i < this.calificaciones.length; i++){
                if (this.calificaciones[i].id == split[0]) {
                    this.calificaciones[i].aplica = 0;
                    this.selectReadOnly(split[0]);
                    this.porcentajeCalificado();
                    this.porcentajeCalificadoServicios(split[2]);
                    return;
                }
            }
        }
    }

    // Si la pregunta no aplica para la encuesta, los campos de calificacion y observación los coloca en READ ONLY.

    selectReadOnly(idPregunta){
        for (let i =0; i < this.preguntasEscogidas.length; i++){
            if (this.preguntasEscogidas[i].id == idPregunta){
                if (this.preguntasEscogidas[i].aplica == 0){
                    this.preguntasEscogidas[i].aplica = 1;
                } else {
                    this.preguntasEscogidas[i].aplica = 0;
                }
            }
        }
    }

    // Guarda la calificacón de la pregunta

    guardarCalificacion(calificacion) {
        var split = calificacion.split("-");
        for (let i = 0; i < this.calificaciones.length; i++){
            if (split[0] == this.calificaciones[i].id){
                this.calificaciones[i].calificacion = split[1];
            }
        }
        this.porcentajeCalificadoServicios(split[2]);
        this.porcentajeCalificado();
    }

    // Guarda la observación de la pregunta

    guardarObservacion(Obs) {
        var split = Obs.split("-");
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (split[0] == this.calificaciones[i].id) {
                this.calificaciones[i].observacion = split[1];
            }
        }
        // console.log(this.calificaciones);
    }

    // Realiza un porcentaje General de la encuesta.

    porcentajeCalificado(){
        var calificacionTotal = 0;
        var aplicados = 0;
        // console.log(this.calificaciones);
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].aplica == 0){
                calificacionTotal = calificacionTotal + parseInt(this.calificaciones[i].calificacion);
                aplicados++;
            }
        }
        var formula = calificacionTotal/aplicados;
        this.porcentajeCliente = Math.round(formula * 100)/100;
    }

    // Revisa si hay preguntas por calificar.

    porCalificado(){
        var calificacionTotal = 0;
        var aplicados = 0;
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].aplica == 0){
                calificacionTotal = calificacionTotal + parseInt(this.calificaciones[i].calificacion);
                aplicados++;
            }
        }
        // console.log("Cantidad total: " + this.calificaciones.length);
        // console.log("Aplicados: " + aplicados);
        // console.log("Cantidad aprobados: " + calificacionTotal);
        var formula = calificacionTotal/aplicados;
        this.porcentajeAprobado = Math.round(formula * 100)/100;
        // console.log("Resultado: " + this.porcentajeAprobado + "%");
        if (formula < 2) {
            this.nivelServicio = "Muy malo";
        } else if (formula >= 2 && formula < 3) {
            this.nivelServicio = "Malo";
        } else if (formula >= 3 && formula < 4) {
            this.nivelServicio = "Bueno";
        } else if (formula >= 4 && formula < 5) {
            this.nivelServicio = "Muy bueno";
        } else if (formula == 5) {
            this.nivelServicio = "Excelente";
        }
    }

    // VENTANA DE NOTIFICACIÓN

    open(content){
        var calificadas = 0;
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].aplica == 0){
                if (this.calificaciones[i].calificacion == 1.0 || this.calificaciones[i].calificacion == 2.0 || this.calificaciones[i].calificacion == 3.0 || this.calificaciones[i].calificacion == 4.0 || this.calificaciones[i].calificacion == 5.0) {
                    calificadas++;
                }
            } else if (this.calificaciones[i].aplica == 1){
                calificadas++;
            }
        }
        if (calificadas != this.preguntasEscogidas.length){
            let Mens = 'Tiene preguntas por calificar';
            // alert(Mens);
            this.addToast({
                title: 'Error',
                msg: Mens,
                timeout: 5000,
                theme: 'default',
                position: 'bottom.right',
                type: 'warning'
            });
        } else {
            this.porCalificado();
            this.modalService.open(content);
        }
    }

    getOut() {
        setTimeout(() => {
            this.router.navigate(['/dashboard']);
        }, 1000);
    }

    // GUARDAR ANS

    saveGetOut() {
        this.saveEncuesta();
    }

    // Guarda la encuesta.

    saveEncuesta(){
        for (let i = 0; i < this.calificaciones.length; i++){
            if (this.calificaciones[i].aplica == 0){
                var calificacion = [{
                    id: this.calificaciones[i].id,
                    calificacion: this.calificaciones[i].calificacion,
                    observacion: this.calificaciones[i].observacion,
                    servicio: this.calificaciones[i].servicio    
                }];
                this.EncuestasService.actualizarPorcentaje(this.porcentajeCliente).subscribe(porcentaje =>{
                    this.result = porcentaje;
                })
                this.EncuestasService.guardarEncuesta(calificacion).subscribe(services =>{
                    this.result = services;
                })
            }
        }
        this.EncuestasService.checkEncuestaCalificada().subscribe(check => {
            this.result = check;
            let Mens = 'Su Encuesta se guardo correctamente';
                    
            if (check.status === 'ok') {
                this.addToast({
                    title: 'Acción exitosa',
                    msg: Mens,
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom.right',
                    type: 'success'
                });
                sessionStorage.removeItem('idEncuesta');
            }
        })
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}