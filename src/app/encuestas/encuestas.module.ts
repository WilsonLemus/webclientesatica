import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { EncuestasDetailComponent } from './detail/encuestas.component';
import { EncuestasListComponent } from './list/list.component';
import { DataFilterPipe } from './data-filter.pipe';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { EncuestasRoutes } from './encuestas.routing';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { DataTableModule } from 'angular2-datatable';
import { AngularEchartsModule } from 'ngx-echarts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EncuestasService } from './encuestas.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        HttpModule,
        CommonModule,
        RouterModule.forChild(EncuestasRoutes),
        SharedModule,
        HttpClientModule,
        AuthenticationModule,
        DataTableModule,
        AngularEchartsModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        AuthGuardService,
        EncuestasService,
        NgbActiveModal,
        NgbModal,
        EncuestasDetailComponent,
        EncuestasListComponent, 
    ],
    declarations: [EncuestasDetailComponent, EncuestasListComponent, DataFilterPipe]
})

export class EncuestasModule { }