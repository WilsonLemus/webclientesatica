import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { EncuestasService, Generic, Result } from '../encuestas.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-pqs',
    templateUrl: './encuestas.component.html',
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],

})

export class EncuestasDetailComponent implements OnInit {
    encuestasForm = new FormGroup({
        sucursalEncuestas: new FormControl(''), 
    });
    private sucursalEncuestas: any;
    private servicios: any;
    public tipoContacto = sessionStorage.getItem("tipoContacto")
    private prueba: any;
    private idSucursal: any;
    private serviciosEscogidos = [];
    public result: Result;
    public nombreContacto = this.auth.getContactoNombre();
    private usuarioCreador = sessionStorage.getItem("correoUsuario");
    private contEncuesta = 0;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    private sucursalEscogida = false;


    constructor(private EncuestasService: EncuestasService, private toastyService: ToastyService, private _fb: FormBuilder, private modalService: NgbModal, private router: Router, private auth: AuthenticationService) { }

    ngOnInit() {
        // console.log(this.usuarioCreador);
        this.encuestasForm = this._fb.group({
            itemRows: this._fb.array([this.initItemRows()])
        }); 

        // Obtiene las sucursales

        this.EncuestasService.getSucursales().subscribe(sucursales => {
            this.sucursalEncuestas = sucursales;
        }, (err) => {
            console.error(err);
        });
    }

    // Realiza validaciónes para las sucursales, revisa si la encuesta ya se califico, si esta pendiente por calificar o si se encuentra realizada en el mismo año.

    open(content){
        this.EncuestasService.comprobarEncuestaValida(content).subscribe(datos => {
            // console.log(datos);
            var añoHoy= new Date();
            var fechaHoy=añoHoy.getFullYear();
            // ID Estado Vigente: 12661217
            if (datos[0] == null) {
                // console.log("No hay nada");
                this.sucursalEscogida = true;
                this.idSucursal = content;
                this.EncuestasService.getServicios().subscribe(servicios => {
                    this.servicios = servicios;
                }, (err) => {
                    console.log(err);
                });
                return;
            } else if (fechaHoy == datos[0][0] && datos[0][1] != 12661217){
                let Mens = "La sucursal ya ha sido evaluada, Seleccione otra sucursal.";
                this.addToast({
                    title: 'Error',
                    msg: Mens,
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom.right',
                    type: 'warning'
                });
                return;
            } else if (datos[0][1] == 12661217 && fechaHoy == datos[0][0]){ 
                this.contEncuesta = 1;
                this.idSucursal = content;
                let Mens = "Puede continuar evaluando la encuesta oprimiendo el botón 'Continuar Encuesta'.";
                this.addToast({
                    title: 'Continuar Encuesta',
                    msg: Mens,
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom.right',
                    type: 'info'
                });
                return;
            }
        }, (err) => {
            console.log(err);
        });
    }

    // Guarda si el servicio aplica o no a la encuesta.

    guardarServicio(check){
        var split = check.split("-");
        var boolValue = (/true/i).test(split[1]) //returns true
        if (boolValue) {
            console.log("Añadir");
            this.serviciosEscogidos.push(split[0]);
            // console.log(this.serviciosEscogidos);
        } else {
            console.log("Remover");
            var pos = this.serviciosEscogidos.indexOf(split[0]);
            this.serviciosEscogidos.splice(pos, 1);
            // console.log(this.serviciosEscogidos);
        }
    }

    // Trae los datos de la encuesta.

    cargarDatos(){
        this.sucursalEscogida = true;
        this.EncuestasService.Regional().subscribe(servicios => {
            this.prueba = servicios;
        }, (err) => {
            console.log(err);
        });
        console.log(this.prueba);
    }

    // Permite continuar la encuesta.

    continuarEncuesta(){
        this.EncuestasService.IdEncuesta(this.idSucursal).subscribe(id => {
            sessionStorage.setItem('idEncuesta', id);
            this.router.navigate(['pqs/callificar-encuestas']);
        }, (err) => {
            console.log(err);
        });
    }

    // Crea la encuesta en rollbase y procede a la calificación.

    generarEncuesta(){
        if (this.serviciosEscogidos.length == 0){
            alert("Tiene que escoger almenos un servicio");
            return;
        } else {
            this.EncuestasService.saveEncuesta(this.serviciosEscogidos, this.idSucursal).subscribe(services => {
                this.result = services;
                let Mens = 'Su Encuesta se genero correctamente';

                this.EncuestasService.Regional().subscribe(reg => {
                    this.EncuestasService.Deudor().subscribe(deu => {
                        this.EncuestasService.IdEncuesta(this.idSucursal).subscribe(id => {
                            this.EncuestasService.saveConsecutivo(deu, reg, id).subscribe(servicios => {
                                this.result = servicios;
                                sessionStorage.setItem('idEncuesta', id);
                                this.EncuestasService.guardarUsuarioCreador(this.usuarioCreador).subscribe(usuario => {
                                    this.result = usuario;
                                    this.router.navigate(['pqs/callificar-encuestas']);
                                });
                            }, (err) => {
                                console.log(err);
                            });
                        }, (err) => {
                            console.log(err);
                        });
                    }, (err) => {
                        console.log(err);
                    });
                }, (err) => {
                    console.log(err);
                });

                if (services.status === 'ok') {
                    this.addToast({
                        title: 'Acción exitosa',
                        msg: Mens,
                        timeout: 5000,
                        theme: 'default',
                        position: 'bottom.right',
                        type: 'success'
                    });
                }


            });
        }
        // const formlen = this.encuestasForm.value.itemRows.length;
        // this.EncuestasService.saveIdSucursal(this.encuestasForm.value, formlen);
    }

    /**
    * Inicia el formulario conb el modelo de solicitud
    */
    initItemRows() {
        return this._fb.group({
            sucursalEncuestas: ['', [<any>Validators.required]],
        });
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

}

