import { Routes } from '@angular/router';
import { EncuestasDetailComponent } from './detail/encuestas.component';
import { EncuestasListComponent } from './list/list.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const EncuestasRoutes: Routes = [
  {
    path: 'pqs/encuestas',
    component: EncuestasDetailComponent,
    data: {
      breadcrumb: 'Generar Encuesta',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'callificar-encuestas',
    component: EncuestasListComponent,
    data: {
      breadcrumb: 'Calificar Encuesta',
    },
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
]