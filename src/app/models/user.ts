export interface User {

    email: String;
    id:String
    firstName: String;
    familyName: String;
    middleName: String;
    telephone: String;
    department: String;
    citySubdivisionName: String;
    cityName: String;
    addressLine: String;
    country: String;
    hash: String;
    salt?: String;
}

interface TokenResponse {
    token: string;
}

export interface TokenPayload {
    email: string;
    password: string;
    name?: string;
}

