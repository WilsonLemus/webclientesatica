import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { ManifestService } from '../Manifest.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './listrma.component.html',
  styleUrls: ['./listrma.component.css']
})
export class ListrmaComponent implements OnInit {

  public filterQuery = '';
  public filterQuerySolicitud = '';
  public filterQueryMaterial = '';
  public filterQueryBranch = '';
  public filterQueryFecha = '';
  public rowsOnPage = 5;
  public sortBy = 'No_manifiesto';
  public sortOrder = 'desc';
  public data;

  constructor(
    private docServ: ManifestService,
    private auth: AuthenticationService,
    private servServ: ServicesService
  ) { }

  /**
   * Obtiene las sucursales y los certificados por sucursal
   */
  ngOnInit() {
    this.servServ.sucursal().subscribe(sucursal => {
      this.docServ.getManifest(sucursal).subscribe(Manifest => {
        this.data = Manifest;
      }, (err) => {
        console.error(err);
      });
    });

  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * Abre el pdf
   * @param id
   */
  goToUrl(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Manifiestos&id=` + id + "&fieldName=RMA&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }


  public doc;

}
