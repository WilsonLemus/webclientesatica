import { Routes} from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const DashboardRoutes: Routes = [{
  path: '',
  component: DashboardComponent,
  data: {
    breadcrumb: 'Dashboard',
    icon: 'icofont-home bg-c-blue',
    status: false
  },
  canActivate: [ AuthGuardService ]
}];