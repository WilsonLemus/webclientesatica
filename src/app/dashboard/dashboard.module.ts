import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { DashboardRoutes } from './dashboard.routing';
import { SharedModule } from '../shared/shared.module';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpClientModule } from '@angular/common/http';
import { ProfileService } from '../user/profile/profile.service';
import { CountData } from './dashboard.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(DashboardRoutes),
    SharedModule,
    HttpClientModule
    // AuthenticationModule
  ],
  providers: [
    AuthenticationService,
    AuthGuardService,
    ProfileService,
    CountData,
    NgbActiveModal,
    NgbModal,
  ],
  declarations: [DashboardComponent]
})

export class DashboardModule { }
