import { Component, OnInit } from '@angular/core';
import { CountData } from './dashboard.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { Count, Linea } from './dashboard.service';
import { getUrlEnvironment } from '../shared/environment';
import { ServicesService } from '../services/services.service';

declare const $: any;
declare var Morris: any;

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})

export class DashboardComponent implements OnInit {

  public CountMaterials: Count;
  public CountServices: Count;
  public tip;
  public linea;
  CountCotizaciones;
  idsLineas;
  CountFacturas;
  CountCreated;
  CountCertificates;
  CountFechaServicio;
  public data;
  public idLinea = "";
  public nameLinea: Linea;
  public datoSociedad;

  constructor(private Data: CountData, private ProfileService: AuthenticationService,    private docServ: ServicesService) { }

  /**
   * Obtiene los datos del dashboard
   */
  ngOnInit() {
  
    let IdCLient = this.ProfileService.getId();
    let idSucursal = sessionStorage.getItem('idSucursales');
    //console.log(idSucursal+"Hola Chino!");
    let idLineaN = sessionStorage.getItem('LineaNegocio');
    //alert("SESION" +idLineaN);
    let SessionId = this.ProfileService.getToken();
    //Tip Ambiental QA
    //this.tip = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=15379874&fieldName=Tip_Ambiental`;
    //Tip Abmiental PRD 
    this.tip = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=15628901&fieldName=Tip_Ambiental`;
    this.Data.executePreImg(SessionId).subscribe(Materials => {
      //this.CountMaterials= Materials;
    }, (err) => {
      console.error(err);
    });



    this.Data.getCountCotizacionesDb(IdCLient, SessionId).subscribe(Materials => {
      this.CountCotizaciones = Materials;
    }, (err) => {
      console.error(err);
    });

    this.Data.getCountFacturaslDb(IdCLient, SessionId).subscribe(Materials => {
      this.CountFacturas = Materials;
    }, (err) => {
      console.error(err);
    });


    this.Data.getCountMaterialDb(IdCLient, SessionId, idSucursal, idLineaN).subscribe(Materials => {
      this.CountMaterials = Materials;
    }, (err) => {
      console.error(err);
    });

    this.Data.getCountServicesDb(IdCLient, SessionId, idSucursal, idLineaN).subscribe(Materials => {
      this.CountServices = Materials;
    }, (err) => {
      console.error(err);
    });


    this.Data.getCountCertificadosDb(IdCLient, SessionId, idSucursal, idLineaN).subscribe(Certificates => {
      this.CountCertificates = Certificates;
    }, (err) => {
      console.error(err);
    });


    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 3;
    var yyyy = today.getFullYear();
    for (let i = 3; i < mm; i++) {
      let n = (i >= 10) ? '-' : '-0';
      this.getDataDash(IdCLient, SessionId, i, yyyy + n + i, idSucursal, idLineaN);
    }

    setTimeout(() => {
      this.printDash(this.data)
    }, 3000);
    
  }

  /**
   * Prepara los datos para imprimir en el dashboard
   * @param IdCLient
   * @param SessionId
   * @param month
   * @param year
   */
  private getDataDash(IdCLient: string, SessionId: string, month, year, idSucursal, idLineaN) {
    this.data = [];
    this.Data.getCountDashboardDb(IdCLient, SessionId, 'createdAt', month, idSucursal, idLineaN).subscribe(materials => {
      this.Data.getCountDashboardDb(IdCLient, SessionId, 'Compromiso_Servicio', month, idSucursal, idLineaN).subscribe(servicio => {
        this.data.push(
          {
            period: year,
            created: materials,
            service: servicio,
          });
      });
    }, (err) => {
      console.error(err);
    });
  }

  /**
   * Imprime los datos en el dashboard
   * @param ele
   */
  private printDash(ele) {
    this.data.sort(function (a, b) {
      return a.period.localeCompare(b.period);
    });

    let arr = {
      element: 'morris-extra-area',
      data: this.data,
      lineColors: ['#fb9678', '#7E81CB'],
      xkey: 'period',
      ykeys: ['created', 'service'],
      labels: ['Solicitudes creadas', 'Compromiso de servicio'],
      pointSize: 0,
      lineWidth: 0,
      resize: true,
      fillOpacity: 0.8,
      behaveLikeLine: true,
      gridLineColor: '#5FBEAA',
      hideHover: 'auto'
    }

    try {
      Morris.Bar(arr);
    } catch (e) {
    //  console.log('capturado');
    }

  }
}
