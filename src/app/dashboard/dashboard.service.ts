import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { getUrlEnvironment } from '../shared/environment';
import { AuthenticationService } from '../authentication/authentication.service';

export interface Count {
    value: string;
}
export interface Linea {
    id: number;
    name: string;
}
@Injectable()
export class CountData {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    public getCountMaterialDb
    (ClientId: string, sessionid: string, idSucursal: string, idLineaN) {
        let query = encodeURI("select count(id) from Tabla_Precios where R_Cliente_TP='") + ClientId + encodeURI("'and id_Sucursal in("+ idSucursal +") and Linea_de_Negocio="+idLineaN);
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;

    }
    public getCountFacturaslDb(ClientId: string, sessionid: string) {
        let query = `select count(id) from Factura1 where R9311861=${ClientId} and (status=8739141 or status=8715053) and Factura_SAP is not null`;
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }

    public getCountCotizacionesDb(ClientId: string, sessionid: string) {
        let query = encodeURI("select count(id) from Cotizacin_Nuevo_Material where R8604192='") + ClientId + encodeURI("'");
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }
    public getCountCertificadosDb(ClientId: string, sessionid: string, idSucursal: string, idLineaN: string) {
        // let query = encodeURI("select count(id) from Solicitud_Servicio where R8634058='") + ClientId + encodeURI("' and id_Sucursal in(" + idSucursal + ") and Lnea_de_Negocio="+idLineaN);
        let query = `SELECT count(id) FROM Solicitud_Servicio WHERE Estado_Certificado = 9231294 AND R8634058 = ${this.auth.getIdUserLoggedIn()} and Sucursal in(${idSucursal}) and Lnea_de_Negocio=${idLineaN} ORDER BY id`;
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }


    public executePreImg(sessionid) {
        let url = `${getUrlEnvironment()}rest/api/runTrigger?sessionId=` + sessionid + "&id=8014444&triggerId=wSEmcqsNR0GLvMezbrbfMg&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }

    public getCountServicesDb(ClientId: string, sessionid: string, idSucursal: string, idLineaN: string) {
        let query = encodeURI("select count(id) from Solicitud_Servicio where R8634058='") + ClientId + encodeURI("' and Sucursal in(") + idSucursal + encodeURI(") and Lnea_de_Negocio='") + idLineaN + encodeURI("'");
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }

    public getCountDashboardDb(ClientId: string, sessionid: string, campo, valor, idSucursal: string, idLineaN: string) {
        let query = encodeURI("select count(id) from Solicitud_Servicio where R8634058='") + ClientId + encodeURI("'and Sucursal in(") + idSucursal + encodeURI(") and Lnea_de_Negocio='") + idLineaN + encodeURI("' and ") + encodeURIComponent('#') + encodeURI(`MONTH(${campo})=${valor}`);
        let url = `${getUrlEnvironment()}rest/api/selectValue?query=` + query + "&sessionId=" + sessionid + "&output=json";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Count) => {
                return data.value;
            })
        );
        return request;
    }
}