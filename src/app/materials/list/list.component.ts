import { Component, OnInit } from '@angular/core';
import { MaterialsService, Materials } from '../materials.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public data: Materials;
  public filterQuery = '';
  public rowsOnPage = 15;
  public sortBy = 'id';
  public sortOrder = 'asc';

  constructor(
    private docServ: MaterialsService,
    private auth: AuthenticationService
  ) { }

  /**
   * Lista los materiales
   */
  ngOnInit() {
   this.docServ.list().subscribe(materials => {
      this.data = materials;
    }, (err) => {
      console.error(err);
    });
  }

  detail(detail: Materials) {
  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * abre el pdf
   * @param id
   */
  goToUrl(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Tabla_Precios&id=` + id + "&fieldName=Hoja_de_Seguridad&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }

  openSecure() {
    let url = `${getUrlEnvironment()}storage/servlet/Image?c=8012571&fileName=tmp_1175069585030383508.pdf&contentType=application%2Fpdf&suggestedName=Hoja+de+Seguridad+de+Materiales.pdf`;
    window.open(url, '_blank');
  }

  public doc;

  /**
   * Sube el doc de seguridad
   * @param event
   * @param id
   */
  onFileChange(event, id) {
    console.log("Hola");
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.doc = {
          id: id,
          fieldName: 'Hoja_de_Seguridad',
          fileName: file.name,
          contentType: file.type,
          value: reader.result.split(',')[1]
        }
        this.docServ.loadSecure(this.doc);
      };
    }
  }

}
