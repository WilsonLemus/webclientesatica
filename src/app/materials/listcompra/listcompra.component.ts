import { Component, OnInit } from '@angular/core';
import { MaterialsService, Materials } from '../materials.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './listcompra.component.html',
  styleUrls: ['./listcompra.component.css']
})
export class ListcompraComponent implements OnInit {

  public data: Materials;
  public filterQuery = '';
  public rowsOnPage = 5;
  public sortBy = 'id';
  public sortOrder = 'asc';

  constructor(
    private docServ: MaterialsService,
    private auth: AuthenticationService
  ) { }

  /**
   * Lista los materiales
   */
  ngOnInit() {
   this.docServ.listCompra().subscribe(materials => {
      this.data = materials;
    }, (err) => {
      console.error(err);
    });
  }

  detail(detail: Materials) {
  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * abre el pdf
   * @param id
   */
  goToUrl(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Tabla_Precios&id=` + id + "&fieldName=Hoja_de_Seguridad&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }

  openSecure() {
    let url = `${getUrlEnvironment()}storage/servlet/Image?c=8012571&fileName=tmp_1175069585030383508.pdf&contentType=application%2Fpdf&suggestedName=Hoja+de+Seguridad+de+Materiales.pdf`;
    window.open(url, '_blank');
  }

}
