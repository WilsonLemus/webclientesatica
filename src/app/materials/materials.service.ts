import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

export interface MaterialsDetail {
    Cdigo_SAP: string;
    R8694571: string;
    Nom_Agrupador: string;
    Nom_Material: string;
    hoja: string;
    tratamiento: string;
    NombreCliente: string;
}

export interface Generic {
    id: Number;
    label: string;
}

export interface Tratamiento {
    id: Number;
    label: string;
    precio: string;
    unidad: string;
}

export interface Result {
    status: string;
    result: string;
    id: string;
}

export interface Status {
    id: string;
    status: string;
    motive: string;
}

export interface Materials {
    Materials: Array<MaterialsDetail>;
}

@Injectable()
export class MaterialsService {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }
    private datos;

    private listMaterials(): Observable<any> {
        let base;
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        var query = encodeURI("select id,Nom_Agrupador,Nom_Material,Valor_Compra,Frecuencia") + encodeURIComponent("#") + encodeURI("code,Das_de_servicio") + encodeURIComponent("#") + encodeURI("value,id,Nom_Tto,Nombre_Sucursal,NombreCliente from Tabla_Precios where R_Cliente_TP=" + this.auth.getId() + " and id_Sucursal in(" + idSucursal + ") and Linea_de_Negocio="+idLineaN+" order by id");
      // console.log(query);
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=10000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());
        const request = base.pipe(
            map((data: Materials) => {
              //  console.log(data);
                return data;

            })
        );
        return request;
    }

    public listCompra(): Observable<any> {
        let base;
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        var query = encodeURI("select id,NombreAgrupador,Nombre_Sucursal,NombreMaterial,Precio from Precios_de_Compra where RPCompra_Cte=" + this.auth.getId() + " and R9084365 in(" + idSucursal + ") and Linea_de_Negocio="+idLineaN+" order by id desc");
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=10000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());
        const request = base.pipe(
            map((data: Materials) => {
                return data;
            })
        );
        return request;
    }

    private generics(type: 'groupers' | 'materials' | 'treatments' | 'materialuser' | 'materialusercompra', idnum: string): Observable<any> {
        let base;
        const userId = this.auth.getIdUserLoggedIn();
        var query = this.getQuery(type, userId, idnum);
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=100000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());
        this.datos = base.pipe(
            map(data => {
                var s = JSON.stringify(data);
                s = JSON.parse(s);
                if (type == 'treatments') {
                    var result = new Array<Generic>();
                    for (var i = 0; i < s.length; i++) {
                        let as;
                        as = { 'id': s[i][0], 'label': s[i][1], 'precio': s[i][2], 'unidad': s[i][3] };
                        result.push(as);
                    }
                    return result;
                } else {
                    var resultT = new Array<Tratamiento>();
                    for (var i = 0; i < s.length; i++) {
                        let as;
                        as = { 'id': s[i][0], 'label': s[i][1] };
                        resultT.push(as);
                    }
                    return resultT;
                }
            })
        );
        return this.datos;
    }

    private getQuery(type: string, userId: string, idnum: string) {
        var query;
        switch (type) {
            case 'treatments':
                query = "select R9482977,Nombre_Tto,Precio,R9483033 from Tratamientos_Negociados where R9482973 = " + userId + " order by Nombre_Tto desc ";
                break;
            case 'groupers':
                query = "select id, name from Agrupador order by name desc";
                break;
            case 'materials':
                query = "select id, name from Material where R8146476=" + idnum + " order by name desc";
                break;
            case 'materialuser':
                query = `select distinct R_Material_TP, from Tabla_Precios where R_Material_TP is not null and R_Cliente_TP=${userId}`;
                break;
            case 'materialusercompra':
                query = 'select distinct RPCompra_Material from Precios_de_Compra where RPCompra_Cte=' + userId;
                break;
        }
        query = encodeURI(query);
        return query;
    }

    public loadSecure(content) {
        const tok = this.auth.getToken();
        var xhr = new XMLHttpRequest();
        var url = `${getUrlEnvironment()}rest/api/setBinaryData?output=json&sessionId=` + tok;
        var params = this.buildUrlParams(content);
        xhr.open("POST", url, true);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {

            }
        }
        var eqd = this.encodeQueryData(content);

        xhr.onprogress = function (e) {
            try {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                }
            } catch (f) {
                alert("f " + f);
            }
        };

        xhr.onload = function (a) {
            console.log("onload");
        };
        xhr.onerror = function () {
            console.log("error");
        };
        xhr.onabort = function () {
            console.log("abort");
        };
        xhr.ontimeout = function () {
            console.log("timeout");
        };
        xhr.onloadstart = function () {
        };
        xhr.onloadend = function () {
        };
        xhr.send(eqd);
    }

    encodeQueryData(data) {
        try {
            var ret = [];
            for (var d in data)
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            return ret.join('&');
        } catch (s) {
            alert("s " + s);
        }
    }

    buildUrlParams(object) {
        try {
            var urlParams = [];
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    urlParams.push(key + "=" + object[key]);
                }
            }
            return urlParams.join('&');
        } catch (r) {
            alert(r);
        }
    }


    private createRequest(service: any, TotalMaterials: number, valorCompra: string, unidad: string): Observable<any> {
        let base;

        for (let i = 0; i < TotalMaterials; i++) {
            let url = this.getQueryCreate(service, i, valorCompra, unidad);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }
    }

    private getQueryCreate(service: any, i: number, valorCompra: string, unidad: string) {
        const userId = this.auth.getIdUserLoggedIn();
        const token = this.auth.getToken();
        let url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Tabla_Precios&sessionId=${token}&R_Cliente_TP=${userId}`;
        url += '&R8694571=' + service.itemRows[i].treatment;
        url += '&R_Agrupa_TP=' + service.itemRows[i].grouper;
        url += '&R_Material_TP=' + service.itemRows[i].material;
        url += '&Valor_Compra=' + valorCompra;
        url += '&R8151490=' + unidad;
        url += '&Tipologia=8699021';
        if (service.itemRows[i].recuItems.length > 0 && service.itemRows[i].selectedItems.length > 0) {
            url += '&Frecuencia=' + service.itemRows[i].recuItems[0].id;
            let d = service.itemRows[i].selectedItems[0].id;
            for (let a = 1; a < service.itemRows[i].selectedItems.length; a++) {
                d += '|' + service.itemRows[i].selectedItems[a].id;
            }
            url += '&Das_de_servicio=' + d;
        }
        return url;
    }

    public list(): Observable<any> {
        return this.listMaterials();
    }

    public treatments(idnum: string): Observable<any> {
        return this.generics('treatments', idnum);
    }
    public grouper(idnum: string): Observable<any> {
        return this.generics('groupers', idnum);
    }
    public materials(idnum: string): Observable<any> {
        return this.generics('materials', idnum);
    }

    public saveReq(service: MaterialsDetail, TotalMaterials: number, valorCompra: string, unidad: string): Observable<any> {
        return this.createRequest(service, TotalMaterials, valorCompra, unidad);
    }

    public treatment(idnum: string): Observable<any> {
        return this.generics('treatments', idnum);
    }

    public materialuser(): Observable<any> {
        return this.generics('materialuser', '');
    }

    public materialusercompra(): Observable<any> {
        return this.generics('materialusercompra', '');
    }
}
