import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {QuillEditorModule} from 'ngx-quill-editor';
import {HttpModule} from '@angular/http';
import {DataTableModule} from 'angular2-datatable';
import {AngularEchartsModule} from 'ngx-echarts';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { MaterialsService } from './materials.service';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ServicesService } from '../services/services.service';
import { ListcompraComponent } from './listcompra/listcompra.component';
import { DataFilterPipe } from '../customerservice/data-filter.pipe';

export const materialRoutes: Routes = [
  {
    path: 'materiales',
    component: ListComponent,
    data: {
      breadcrumb: 'Materiales',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'compra',
    component: ListcompraComponent,
    data: {
      breadcrumb: 'Materiales',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'crear-material',
    component: DetailComponent,
    data: {
      breadcrumb: 'Crear material',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(materialRoutes),
    SharedModule,
    FormsModule,
    QuillEditorModule,
    HttpModule,
    DataTableModule,
    AngularEchartsModule,
    AngularMultiSelectModule
  ],
  providers: [
    MaterialsService,
    AuthGuardService,
    ServicesService
  ],
  declarations: [ListComponent, DetailComponent, ListcompraComponent, DataFilterPipe]
})
export class MaterialsModule { }
