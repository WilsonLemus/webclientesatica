import { Component, OnInit, ViewChild, trigger, state, animate, transition, style } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MaterialsService,  Generic, Result, Status, MaterialsDetail, Tratamiento } from '../materials.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { ServicesService } from '../../services/services.service';
import { Subscription } from 'rxjs';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css'],
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],
})

export class DetailComponent implements OnInit {
    public invoiceForm: FormGroup;
    statusView: boolean;
    statusSuccess: boolean;
    rechazarDiv: boolean;
    title: string;
    msg: string;
    data: Generic;
    material: Generic;
    public groupuser: Subscription;

    treats: Array<Tratamiento>;
    result: Result;
    materialsNew: Array<MaterialsDetail> = [];
    position = 'bottom-right';
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    treatmentVis = 'hidden';
    selectedItemsVis = 'hidden';
    visibility = 'hidden';
    materialVis = 'hidden';
    tipoEmba = 'hidden';
    cantEmba = 'hidden';
    resume = 'hidden';
    showme = true;
    public numSol = 0;
    public doc;
    public vall;
    idTrat;

    status: Status = {
        status: '',
        id: '',
        motive: ''
    };

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private docServ: MaterialsService,
        private toastyService: ToastyService,
        private _fb: FormBuilder,
        private generalServ: ServicesService,
    ) { }

    itemList = [];
    itemRecu = [];
    selectedItems = [];
    selectedRecuItems = [];
    recuItems = [];
    settingsRecu = {};
    settings = {};


    initItemRows() {
        return this._fb.group({
            treatment: ['', [<any>Validators.required]],
            grouper: ['', [<any>Validators.required]],
            material: ['', [<any>Validators.required]],
            //secure: ['', [<any>Validators.required]],
            selectedItems: [''],
            recuItems: [''],
            secure: ['']//[<any>Validators.required, <any>Validators.minLength(3)]]
        });
    }
    /**
     * Carga el listado de la opción elegida
     * @memberof DetailComponent
     */
    ngOnInit() {
        this.vall = null;
        this.invoiceForm = this._fb.group({
            itemRows: this._fb.array([this.initItemRows()])
        });

        this.docServ.treatment("").subscribe(treatments => {
            this.treats = treatments;
            console.log(JSON.stringify(this.treats));
        }, (err) => {
            console.error(err);
        });

        this.itemList = [
            { "id": "8184909", "itemName": "D" },
            { "id": "8184903", "itemName": "L" },
            { "id": "8184904", "itemName": "M" },
            { "id": "8184905", "itemName": "Mi" },
            { "id": "8184906", "itemName": "J" },
            { "id": "8184907", "itemName": "V" },
            { "id": "8184908", "itemName": "S" }
        ];

        this.itemRecu = [
            { "id": "S", "itemName": "Semanal" },
            { "id": "Q", "itemName": "Quincenal" },
            { "id": "M", "itemName": "Mensual" }
        ]

        this.selectedItems = [
        ];

        this.selectedRecuItems = [
        ];

        this.settings = {
            text: "Seleccione",
            selectAllText: 'Seleccione Todo',
            unSelectAllText: 'Deseleccione Todo',
            classes: "myclass custom-class"
        };

        this.settingsRecu = {
            singleSelection: true,
            text: "Seleccione",
            classes: "myclass custom-class",
        };
    }

    onChangeTreatment(event) {
        this.treatmentVis = 'shown';
        this.getGroupsByUser(event);
    }

    getGroupsByUser(id) {
        this.idTrat = id;
        this.loadGroup(id);
    }

    loadGroup(groupsids) {
        this.visibility = 'shown';
        this.generalServ.grouper(groupsids).subscribe(groupers => {
            this.data = groupers;
        }, (err) => {
            console.error(err);
        });
    }

    addNewRow(i) {
        this.createReq();
    }

    saveGetOut() {
        this.createReq();
        this.getOut();
    }

    getOut() {
        setTimeout(() => {
            this.router.navigate(['/materiales/materiales']);
        }, 1000);
    }

    deleteRow(index: number) {
        const control = <FormArray>this.invoiceForm.controls['itemRows'];
        control.removeAt(index);
    }

    onChangeGroup(event) {
        this.visibility = 'shown';
        this.generalServ.materials(event, this.idTrat).subscribe(materials => {
            this.material = materials;
        }, (err) => {
            console.error(err);
        });
    }

    onChangeMaterial($event) {
        this.selectedItemsVis = 'show';
        this.materialVis = 'shown';
    }

    /**
     * Realizart la accion de rechazo
     * @param {NgForm} myForm Formulario con motivo de rechazo
     * @memberof DetailComponent
     */
    createReq() {
        let formlen = this.invoiceForm.value.itemRows.length;
        var valorCompra = '0';
        var unidad = '0';
        
        for(let i = 0; i < this.treats.length; i++){
            if(this.treats[i].id == this.idTrat){
                valorCompra = this.treats[i].precio;
                unidad = this.treats[i].unidad;
            }   
        }

        console.log(this.invoiceForm.value);
        this.docServ.saveReq(this.invoiceForm.value, formlen, valorCompra, unidad)
            .subscribe(Services => {
                let Mens = 'El material se creó exitosamente';
                let NoMens = 'No fue posible crear el material';

                if (formlen > 1) {
                    Mens = 'Los materiales se crearon exitosamente';
                    NoMens = 'No fue posible crear los materiales';
                }

                if (Services.status === 'ok') {
                    console.log(JSON.stringify(Services));
                    const control = <FormArray>this.invoiceForm.controls['itemRows'];
                    this.numSol++;
                    if(this.doc){
                        this.doc.id = Services.id;
                        this.docServ.loadSecure(this.doc);
                    }
                    this.addToast(
                        {
                            title: 'Acción exitosa',
                            msg: Mens,
                            timeout: 5000,
                            theme: 'default',
                            position: 'bottom-right',
                            type: 'success'
                        });

                    control.removeAt(0);
                    control.push(this.initItemRows());
                    this.selectedItems = [];
                    this.recuItems = [];
                    this.doc = null;
                } else {
                    this.addToast(
                        {
                            title: 'Error en el proceso',
                            msg: NoMens,
                            timeout: 5000,
                            theme: 'default',
                            position: 'bottom-right',
                            type: 'error'
                        });
                }
            }, (err) => {
                console.error(err);
            });
    }

    reset() {
        this.data = null;
        this.material = null;
        this.materialVis = 'hidden';
        this.visibility = 'hidden';
        this.treatmentVis = 'hidden';
        this.invoiceForm.reset();
    }

    sortByWordLength = (a: any) => {
        return a.id.length;
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

    onFileChange(event) {
        console.log('entra');
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.doc = {
                    id: '',
                    fieldName: 'Hoja_de_Seguridad',
                    fileName: file.name,
                    contentType: file.type,
                    value: reader.result.split(',')[1]
                }
                this.vall = (this.doc) ? this.doc.value : '';
                //console.log(this.doc);
            };
        }
    }

    onItemSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    OnItemDeSelect(item: any) {
        console.log(item);
        console.log(this.selectedItems);
    }
    onItemSelectRecu(item: any) {
        console.log(item);
        console.log(this.selectedRecuItems);
    }
    OnItemDeSelectRecu(item: any) {
        console.log(item);
        console.log(this.selectedRecuItems);
    }
    onSelectAll(items: any) {
        console.log(items);
    }
    onDeSelectAll(items: any) {
        console.log(items);
    }
}
