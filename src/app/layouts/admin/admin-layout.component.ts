import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import 'rxjs/add/operator/filter';
import { state, style, transition, animate, trigger, AUTO_STYLE } from '@angular/animations';
import { MenuItems } from '../../shared/menu-items/menu-items';
import { AuthenticationService } from '../../authentication/authentication.service';
import { ProfileService } from '../../user/profile/profile.service';
import { LineaNegocio } from '../admin-layout.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { ServicesService } from '../../services/services.service';
import { Router } from '@angular/router';
import { getUrlEnvironment } from '../../shared/environment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

export interface Options {
  heading?: string;
  removeFooter?: boolean;
  mapHeader?: boolean;
}

export class Linea {
  id: number;
  name: string;
}

@Component({
  selector: 'app-layout',
  providers: [
    ProfileService,
    LineaNegocio
  ],
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.css'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('mobileMenuTop', [
      state('no-block, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('yes-block',
        style({
          height: AUTO_STYLE,
        })
      ),
      transition('no-block <=> yes-block', [
        animate('100ms ease-in-out')
      ])
    ]),
    trigger('visibility', [
      state('shown', style({
        opacity: 1
      })),
      state('hidden', style({
        opacity: 0
      })),
      transition('* => *', animate('.1s'))
    ])
  ],

})

export class AdminLayoutComponent implements OnInit {
  editProfile = true;
  editProfileIcon = 'icofont-edit';
  position = 'botton-right';
  title: string;
  msg: string;
  _bussiness: any;
  showClose = true;
  timeout = 5000;
  theme = 'bootstrap';
  type = 'default';
  closeOther = false;

  editAbout = true;
  editAboutIcon = 'icofont-edit';

  deviceType = 'desktop';
  verticalNavType = 'expanded';
  verticalEffect = 'shrink';
  innerHeight: string;
  isCollapsedMobile = 'no-block';
  isCollapsedSideBar = 'no-block';
  toggleOn = true;
  windowWidth: number;
  dataCont: any;
  dataLineas1: any;
  dataLineas2: any;
  dataLineas3: any;
  dataLineas4: any;
  dataLineas5: any;
  iddataLineas1: any;
  iddataLineas2: any;
  iddataLineas3: any;
  iddataLineas4: any;
  iddataLineas5: any;
  Linea: any;
  LineaActual: any;
  public data: string = '';

  public htmlButton: string;
  public logo;
  public logo1;
  public logo2;
  public logo3;
  public logo4;
  public idLinea;


  constructor(public menuItems: MenuItems, private auth: AuthenticationService,
    private ProfileService: ProfileService, private toastyService: ToastyService,
    private docServ: ServicesService, private router: Router, private LineaNegocio: LineaNegocio, private modalService: NgbModal) {
    const scrollHeight = window.screen.height - 150;
    this.innerHeight = scrollHeight + 'px';
    this.windowWidth = window.innerWidth;
    this.setMenuAttributs(this.windowWidth);
  }

  setLocalItem(key, id, time?) {
    time = time || 0;
    let timestamp = this.setTimeToLive(time);
    let storageObj = {
      'timestamp': timestamp,
      'value': id
    };
    sessionStorage.setItem(key, JSON.stringify(storageObj));
  }


  //funcion get
  getLocalItem(key) {
    let storage = sessionStorage.getItem(key);
    let item = null;

    if (storage) {
      item = JSON.parse(storage);
    }

    if (item) {
      let time = this.evaluateTimeToLive(item.timestamp);
      if (time) {
        return item.value;
      } else {
        sessionStorage.removeItem(key);
        return false;
      }
    }
    else {
      return false;
    }
  }

  setTimeToLive(lifespan) {
    let currentTime = new Date().getTime();
    let timeToLive;
    if (lifespan !== 0) {
      timeToLive = currentTime + lifespan;
    } else {
      timeToLive = 0;
    }
    return timeToLive;
  }
  //Evalua el tiempo determina para la sesión
  evaluateTimeToLive(timestamp) {
    let currentTime = new Date().getTime();
    if (currentTime <= timestamp || timestamp === 0) {
      return true;
    } else {
      return false;
    }
  }


  ngOnInit() {
    let SessionId = this.auth.getToken();
    this.dataCont = JSON.parse(this.ProfileService.getProfile());
    this.getListBussines();
    this.LineaNegocio.getIdlinea().subscribe(data => {
      //Envía lineas de Negocio del Cliente-Contacto
      for (var i = 0; i < data.length; i++) {
        if (i == 0) {
          this.data = data[i].id;
        } else {
          this.data += ',' + data[i].id;
        }
      }
      sessionStorage.setItem("LineasDeNegocio", this.data);
      //Para seleccionar la linea de Negocio a consultar (Si se agregan más lineas hay que agregar otra posición)
      var arreglodatos = [{
        id: '',
        name: ''
      }, {
        id: '',
        name: ''
      }, {
        id: '',
        name: ''
      }, {
      }, {
        id: '',
        name: ''
      }, {
        id: '',
        name: ''
      }, {
        id: '',
        name: ''
      }];
      for (var i = 0; i < data.length; i++) {
        arreglodatos[i] = {
          'id': data[i]['id'],
          'name': data[i]['name']
        };

      }
      /*
      * Envía nombre y Id de lineas de negocio
      */
      this.dataLineas1 = arreglodatos[0]['name']; this.iddataLineas1 = arreglodatos[0]['id'];
      this.logo = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=${this.iddataLineas1}&fieldName=Logo_Linea`;
      this.dataLineas2 = arreglodatos[1]['name']; this.iddataLineas2 = arreglodatos[1]['id'];

      this.logo1 = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=${this.iddataLineas2}&fieldName=Logo_Linea`;
      this.dataLineas3 = arreglodatos[2]['name']; this.iddataLineas3 = arreglodatos[2]['id'];

      this.logo2 = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=${this.iddataLineas3}&fieldName=Logo_Linea`;
      this.dataLineas4 = arreglodatos[3]['name']; this.iddataLineas4 = arreglodatos[3]['id'];

      this.logo3 = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=${this.iddataLineas4}&fieldName=Logo_Linea`;

      this.dataLineas5 = arreglodatos[4]['name']; this.iddataLineas4 = arreglodatos[4]['id'];
      this.logo4 = `${getUrlEnvironment()}rest/api/getBinaryData?sessionId=${SessionId}&id=${this.iddataLineas5}&fieldName=Logo_Linea`;
      this.inicio();
      this.Linea = sessionStorage.getItem('LineaNegocio');
      this.LineaActual = sessionStorage.getItem("LineaActual");
      //alert(this.LineaActual);
    })

    //this.parpadea(sessionStorage.getItem('LineaNegocio'));

    this.LineaNegocio.getLinea().subscribe(data => {
      console.log(data[0][1]);
      sessionStorage.setItem("LineaActual", data[0][1]);
    })

    if (!(sessionStorage.getItem('ln'))) {
      sessionStorage.setItem('ln', '1');
      location.reload();
    }
  }
  open(content) {
    this.modalService.open(content);
  }
  inicio() {
    let linea = sessionStorage.getItem("LineaNegocio");
    //  console.log(linea);
    if (linea == "") {
      //Alerta de Lineas de Negocio
      this.addToast({
        title: 'Información',
        msg: 'Seleccione una línea de negocio para poder continuar',
        showClose: true,
        theme: 'bootstrap',
        type: 'warning',
        position: 'top-center',
        closeOther: true
      })
    }
  }
  /*parpadea(idLinea) {
    if (idLinea != "") {
      document.getElementById('texto').className = 'btn btn-primary';
    }
  }*/

  onClickedOutside(e: Event) {
    if (this.windowWidth < 768 && this.toggleOn && this.verticalNavType !== 'offcanvas') {
      this.toggleOn = true;
      this.verticalNavType = 'offcanvas';
    }
  }

  onResize(event) {
    this.innerHeight = event.target.innerHeight + 'px';
    /* menu responsive */
    this.windowWidth = event.target.innerWidth;
    let reSizeFlag = true;
    if (this.deviceType === 'tablet' && this.windowWidth >= 768 && this.windowWidth <= 1024) {
      reSizeFlag = false;
    } else if (this.deviceType === 'mobile' && this.windowWidth < 768) {
      reSizeFlag = false;
    }

    if (reSizeFlag) {
      this.setMenuAttributs(this.windowWidth);
    }
  }

  setMenuAttributs(windowWidth) {
    if (windowWidth >= 768 && windowWidth <= 1024) {
      this.deviceType = 'tablet';
      this.verticalNavType = 'collapsed';
      this.verticalEffect = 'push';
    } else if (windowWidth < 768) {
      this.deviceType = 'mobile';
      this.verticalNavType = 'offcanvas';
      this.verticalEffect = 'overlay';
    } else {
      this.deviceType = 'desktop';
      this.verticalNavType = 'expanded';
      this.verticalEffect = 'shrink';
    }
  }

  toggleOpened() {
    if (this.windowWidth < 768) {
        this.toggleOn = this.verticalNavType === 'offcanvas' ? true : this.toggleOn;
        this.verticalNavType = this.verticalNavType === 'expanded' ? 'offcanvas' : 'expanded';
    } else {
        this.verticalNavType = this.verticalNavType === 'expanded' ? 'collapsed' : 'expanded';
    }
  }

  toggleOpenedSidebar() {
    this.isCollapsedSideBar = this.isCollapsedSideBar === 'yes-block' ? 'no-block' : 'yes-block';
  }

  onMobileMenu() {
    this.isCollapsedMobile = this.isCollapsedMobile === 'yes-block' ? 'no-block' : 'yes-block';
  }

  public logout() {
    this.auth.logout();
  }

  contact() {
    this.addToast(
      {
        title: 'Información',
        msg: 'Te llevaremos al formulario para registrar tu intención',
        timeout: this.timeout,
        theme: this.theme,
        position: 'botton-right',
        type: 'info'
      });

    setTimeout(() => {
      this.router.navigate(['/cotizacion/create']);
    }, 2000);
  }

  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added removed!');
      }
    };

    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }


  public enviaLinea(idLinea, nombre) {
  //  alert("clic " + idLinea);
    sessionStorage.setItem("LineaNegocio", idLinea);
    sessionStorage.setItem("LineaActual", nombre);
    location.reload();
  }
  getListBussines() {
    this.docServ.getListBussines().subscribe(bussines => {
      this._bussiness = bussines;
    }, (err) => {
      console.error(err);
    });
  }
  get bussines() {
    return this._bussiness;
  }
}
