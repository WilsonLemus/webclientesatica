import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { getUrlEnvironment } from '../shared/environment';
import { AuthenticationService } from '../authentication/authentication.service';

export interface Linea {
    id: number;
    name: string;
}

@Injectable()
export class LineaNegocio {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    /*
    *Trae ID linea de negocio
    */

    public getIdlinea() {
        let idContacto = JSON.parse(sessionStorage.getItem('contactoId')).value;
        // if (getUrlEnvironment() == "http://qa.i4soluciones.com/") {
            // var url = `${getUrlEnvironment()}rest/api/getRelationships?objName=Contacos&id=${idContacto}&relName=R10852641&fieldList=id,name,&sessionId=${this.auth.getToken()}`;
        // } else {
          //  alert(idContacto); 
            //var url = `${getUrlEnvironment()}rest/api/getRelationships?&objName=Contacos&id=${idContacto}&relName=R12388522&fieldList=id,name&output=json&sessionId=${this.auth.getToken()}`;
            var url = `${getUrlEnvironment()}rest/api/getRelationships?objName=Contactos&id=` + idContacto + '&relName=R12388522&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json';           
            
            //selectQuery?query= SELECT id FROM Calificaciones_de_ANS WHERE R10858093 =` + idSucursal + ` AND Vigente=1&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
            // }
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Linea) => {
                return data;
            })
        );
        return request;
    }
    /**
     * Trae nombre Linea Actual
     */

    public getLinea() {
        let lineaNegocio = sessionStorage.getItem("LineaNegocio");
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Linea_de_Negocio where id=` + lineaNegocio + '&sessionId=' + this.auth.getToken() + '&output=json&maxRows=2000');
        const request = base.pipe(
            map(data => {
                //  alert(data[0][0]);
                return data;
            })
        );
        return request;
    }


}