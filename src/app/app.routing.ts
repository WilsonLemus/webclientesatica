import {Routes} from '@angular/router';
import {AdminLayoutComponent} from './layouts/admin/admin-layout.component';
import {AuthLayoutComponent} from './layouts/auth/auth-layout.component';

/**
 * Punto de partida, a continuación se definen las rutas y los módulos asociados a cada uno
 */
export const AppRoutes: Routes = [{
  path: '',
  component: AdminLayoutComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    }, {
      path: 'dashboard',
      loadChildren: './dashboard/dashboard.module#DashboardModule'
    },
    {
      path: 'servicios',
      loadChildren: './services/services.module#ServicesModule',
    },
    {
      path: 'pqs',
      loadChildren: './pqs/pqs.module#PqsModule',
    },
    {
      path: 'pqs',
      loadChildren: './ans/ans.module#AnsModule',
    },
    {
      path: 'pqs',
      loadChildren: './encuestas/encuestas.module#EncuestasModule',
    },
    {
      path: 'invoice',
      loadChildren: './invoice/invoice.module#InvoiceModule',
    },
    {
      path: 'invoicepre',
      loadChildren: './invoicepre/invoicepre.module#InvoicepreModule',
    },
    {
      path: 'materiales',
      
      loadChildren: './materials/materials.module#MaterialsModule',
    },
    {
      path: 'certificados',
      loadChildren: './certificates/certificates.module#CertificatesModule',
    },
    {
      path: 'manifiestos',
      loadChildren: './manifest/manifest.module#ManifestModule',
    },
    {
      path: 'documentos',
      loadChildren: './documents/documents.module#DocumentsModule',
    },
    {
      path: 'cotizacion',
      loadChildren: './quotation/quotation.module#QuotationModule',
    },
    {
      path: 'perfil',
      loadChildren: './user/profile/profile.module#ProfileModule',
    }
  ]
}, {
  path: '',
  component: AuthLayoutComponent,
  children: [
    {
      path: 'authentication',
      loadChildren: './authentication/authentication.module#AuthenticationModule'
    }
  ]
}, {
  path: '**',
  redirectTo: 'error/404'
}];
