import { Routes } from '@angular/router';
import { ListpreComponent } from './list/list.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const InvoicepreRoutes: Routes = [
  {
    path: 'prefacturas',
    component: ListpreComponent,
    data: {
      breadcrumb: 'preFacturas',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
