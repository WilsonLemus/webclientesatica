import { Component, OnInit } from '@angular/core';
import {  Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';
import { InvoicepreService, Services } from '../invoicepre.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { trigger, state, animate, transition, style } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { getUrlEnvironment } from '../../shared/environment';
@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
  animations: [
    trigger('visibility', [
      state('shown', style({
        opacity: 1
      })),
      state('hidden', style({
        opacity: 0
      })),
      transition('* => *', animate('.5s'))
    ])
  ],
})
export class ListpreComponent implements OnInit {

  public form: FormGroup;
  public reason: FormGroup;
  public data: Services;
  public service: InvoicepreService;
  public filterQuery = '';
  public filterQueryDate = '';
  public filterQuerySol = '';
  public rowsOnPage = 20;
  public sortBy = 'invoiceId';
  public sortOrder = 'desc';
  Save = 'hidden';
  public numFactura;
  public id;

  position = 'bottom-right';
  title: string;
  msg: string;
  showClose = true;
  timeout = 5000;
  theme = 'bootstrap';
  type = 'default';
  public doc;
  public vall;
  toastyService: any;

  constructor(
    private fb: FormBuilder,
    private router: Router,
    private docServ: InvoicepreService,
    private auth: AuthenticationService,
    private modalService: NgbModal,

  ) { }

  /**
   * Lista las facturas del cliente
   */
  ngOnInit() {

    this.form = this.newElement();
    this.reason = this.newElementR();
    this.Save = ([0]['dato'] == 1) ? 'shown' : 'hidden';
    this.docServ.list().subscribe(invoices => {

      for (let i = 0; i < invoices.length; i++) {
        this.docServ.solicitudes(invoices[i][0]).subscribe(services => {
          let sol = '';
          for (let k = 0; k < services.length; k++) {
            sol += (services.length == k + 1) ? services[k].name : services[k].name + ' - ';
          }
          invoices[i].push(sol);

        }, (err) => {
          console.error(err);
        });
      }
      this.data = invoices;
    }, (err) => {
      console.error(err);
    });
  }
  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
  }
  /**
 * Prepara los formulario
 */
  newElement() {
    return this.fb.group({
      Numero: [null, Validators.compose([Validators.required])],
      secure: ['']
    });
  }
  newElementR() {
    return this.fb.group({
      motivo: [null, Validators.compose([Validators.required])],
      secure: ['']
    });
  }


  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  open(content, factura, id) {
    this.modalService.open(content);
    this.numFactura = factura;
    this.id = id;
    //console.log("No.Factura" + this.numFactura);
    //console.log("id" + id);
  }

  /**
   * Selecciona el archivo 
   * @param id
   */
  onFileChange(event, id) {
    console.log("Hola", id);
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      console.log(file);
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.doc = {
          id: id,
          fieldName: 'Adjunto_Orden_de_compra',
          fileName: file.name,
          contentType: file.type,
          value: reader.result.split(',')[1]
        }
        this.docServ.loadSecure(this.doc);
      };
    }
  }
  getOut() {
    setTimeout(() => {
      this.router.navigate(['/servicios/solicitudes']);
    }, 3000);
  }

  save(form: any, id: any) {
    this.sendaction(form, id);
  }

  //Cambia estado: aprobado
  sendaction(form: any, id: any) {
    if (form.Numero != '') {
      this.docServ.UpdateNo(form.Numero, id).subscribe(Services => {
        if (Services.status === 'ok') {

          this.addToast({
            title: 'Acción exitosa',
            msg: 'La Pre-Factura se aprobo exitosamente',
            timeout: 5000,
            theme: 'default',
            position: 'top-right',
            type: 'success'
          });
        }
      })
    }
   location.reload();
  }
  saveR(reason: any, id: any) {
    this.sendactionR(reason, id);
  }

  //Cambia estado: Rechazado
  sendactionR(reason: any, id: any) {
    if (reason.motivo != '') {
      this.docServ.updateReason(reason.motivo, id).subscribe(Services => {
        if (Services.status === 'ok') {

          this.addToast({
            title: 'Acción exitosa',
            msg: 'La Pre-Factura se aprobo exitosamente',
            timeout: 5000,
            theme: 'default',
            position: 'top-right',
            type: 'success'
          });
        }
      })
    }
    location.reload();
  }
  openPdf(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Factura1&id=` + id + "&fieldName=Doc_Fact&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }
}
