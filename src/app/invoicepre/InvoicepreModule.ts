import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { HttpModule } from '@angular/http';
import { SharedModule } from '../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { AuthenticationService } from '../authentication/authentication.service';
import { DataTableModule } from 'angular2-datatable';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AngularEchartsModule } from 'ngx-echarts';
import { DataFilterPipe } from './data-filter.pipe';
import { InvoicepreRoutes } from './invoicepre.routing';
import { InvoicepreService } from './invoicepre.service';
import { ListpreComponent } from './list/list.component';
@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(InvoicepreRoutes),
    SharedModule,
    HttpClientModule,
    AuthenticationModule,
    DataTableModule,
    AngularEchartsModule
  ],
  providers: [
    InvoicepreService,
    AuthenticationService,
    AuthGuardService
  ],
  declarations: [ListpreComponent, DataFilterPipe]
})
export class InvoicepreModule {
}
