import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

export interface InvoicesDetail {
    id: String;
    invoiceId: String;
    fecha: String;
    estado: String;
    solicitudes: String;
}
export interface Result {
    status: string;
    id: string;
    result: string;
}

export interface Services {
    Services: Array<InvoicesDetail>;
}

@Injectable()
export class InvoicepreService {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    private listServices(): Observable<any> {
        let base;
        var url = this.getQueryServices();
        base = this.http.get(url);
        const request = base.pipe(
            map((data: any) => {
                return this.proccessRequestListServices(data);
            })
        );
        return request;
    }

    private proccessRequestListServices(data: any) {
        for (let i = 0; i < data.length; i++) {
            let from: Date = new Date(data[i][3]);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 9) ? '0' + from.getDate() : from.getDate();
            data[i][3] = from.getFullYear() + '/' + mon + '/' + day;
        }
        return data;
    }

    private getQueryServices() {
        var query = `select id, Factura_SAP, status, createdAt from Factura1 where R9311861=${this.auth.getId()} and status in(8739141,10936835,10936869) and Factura_SAP is not null ORDER BY id`;
        query = encodeURI(query);
        var url = `${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=1000&output=json&query=${query}&sessionId=${this.auth.getToken()}`;
        console.log("Quey", url);
        return url;

    }


    private listSolicitudes(id): Observable<any> {
        let base;
        var url = `${getUrlEnvironment()}rest/api/getRelationships?&output=json&id=${id}&relName=R8713917&fieldList=name&sessionId=${this.auth.getToken()}`;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Services) => {
                return data;
            })
        );
        return request;
    }

    public UpdateNo(No: string, id): Observable<any> {
        const url = `${getUrlEnvironment()}rest/api/updateRecord?output=json&useIds=true&objName=Factura1&sessionId=` + this.auth.getToken() + `&id=` + id + `&NumeroOrdenCompra=` + No + `&status=10936835`;
        return this.http.post(url, null)
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });
    }
    public updateReason(reason: string, id): Observable<any> {
        const url = `${getUrlEnvironment()}rest/api/updateRecord?output=json&useIds=true&objName=Factura1&sessionId=` + this.auth.getToken() + `&id=` + id + `&MotivoRechazo=` + reason + `&status=10936869`;
        return this.http.post(url, null)
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });
    }

    public list(): Observable<any> {
        return this.listServices();
    }

    public solicitudes(id): Observable<any> {
        return this.listSolicitudes(id);
    }

    public loadSecure(content) {
        const tok = this.auth.getToken();
        var xhr = new XMLHttpRequest();
        var url = `${getUrlEnvironment()}rest/api/setBinaryData?output=json&sessionId=` + tok;
        var params = this.buildUrlParams(content);
        xhr.open("POST", url, true);

        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {

            }
        }
        var eqd = this.encodeQueryData(content);

        xhr.onprogress = function (e) {
            try {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                }
            } catch (f) {
                alert("f " + f);
            }
        };

        xhr.onload = function (a) {
            console.log("onload");
        };
        xhr.onerror = function () {
            console.log("error");
        };
        xhr.onabort = function () {
            console.log("abort");
        };
        xhr.ontimeout = function () {
            console.log("timeout");
        };
        xhr.onloadstart = function () {
        };
        xhr.onloadend = function () {
        };
        xhr.send(eqd);
    }

    encodeQueryData(data) {
        try {
            var ret = [];
            for (var d in data)
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            return ret.join('&');
        } catch (s) {
            alert("s " + s);
        }
    }
    buildUrlParams(object) {
        try {
            var urlParams = [];
            for (var key in object) {
                if (object.hasOwnProperty(key)) {
                    urlParams.push(key + "=" + object[key]);
                }
            }
            return urlParams.join('&');
        } catch (r) {
            alert(r);
        }
    }
}