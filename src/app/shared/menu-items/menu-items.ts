import { Injectable } from '@angular/core';

export interface BadgeItem {
    type: string;
    value: string;
}

export interface ChildrenItems {
    state: string;
    target?: boolean;
    name: string;
    type?: string;
    children?: ChildrenItems[];
}

export interface MainMenuItems {
    state: string;
    main_state?: string;
    target?: boolean;
    name: string;
    type: string;
    icon: string;
    badge?: BadgeItem[];
    children?: ChildrenItems[];
}

export interface Menu {
    label: string;
    main: MainMenuItems[];
}

const MENUITEMS = [
    {
        label: 'Inicio',
        main: [
            {
                state: 'dashboard',
                name: 'Dashboard',
                type: 'link',
                icon: 'ti-home'
            }
        ]
    },
    {
        label: 'Menú General',
        main: [
            {
                state: 'perfil',
                name: 'Perfil Cliente',
                type: 'sub',
                icon: 'ti-user',
                children: [
                    {
                        state: 'detalle',
                        name: 'Datos generales',
                        type: 'link',
                        icon: 'ti-receipt'
                    }
                ]
            },
            {
                state: 'servicios',
                name: 'Servicios',
                type: 'sub',
                icon: 'ti-truck',
                children: [
                    {
                        state: 'solicitudes',
                        name: 'Solicitudes',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    },
                    {
                        state: 'crear-solicitud',
                        name: 'Crear solicitud',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    }/*,
                    {
                        state: 'aprobacion-pesos',
                        name: 'Aprobación de Pesos',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    }
                    */
                ]

            },
            {
                state: 'materiales',
                name: 'Materiales',
                type: 'sub',
                icon: 'ti-package',
                children: [
                    {
                        state: 'crear-material',
                        name: 'Material por Tratamiento',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    },
                    {
                        state: 'materiales',
                        name: 'Precios de venta',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    },
                    {
                        state: 'compra',
                        name: 'Precios de compra',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    }

                ]

            },

            {
                state: 'cotizacion',
                name: 'Cotización',
                type: 'sub',
                icon: 'ti-briefcase',
                children: [
                    {
                        state: 'create',
                        name: 'Solicitar',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged',
                        reload: true
                    },
                    {
                        state: 'listado-cotizaciones',
                        name: 'Listado de cotizaciones',
                        type: 'link',
                        icon: 'ti-layout-accordion-merged'
                    }

                ]

            },
            /*{
                state: 'invoicepre',
                name: 'PreFacturas',
                type: 'sub',
                icon: 'ti-files',
                children: [
                    {
                        state: 'prefacturas',
                        name: 'Listado Pre-facturas',
                        type: 'link',
                        icon: 'ti-clipboard'
                    }

                ]

            },*/
            {
                state: 'invoice',
                name: 'Facturas',
                type: 'sub',
                icon: 'ti-receipt',
                children: [
                    {
                        state: 'facturas',
                        name: 'Listado',
                        type: 'link',
                        icon: 'ti-clipboard'
                    }

                ]

            },
            {
                state: 'certificados',
                name: 'Certificados',
                type: 'sub',
                icon: 'ti-file',
                children: [
                    /*{
                        state: 'lineas-negocio',
                        name: 'Generar Certificados',
                        type: 'link',
                        icon: 'ti-clipboard'
                    },*/
                    {
                        state: 'certificados',
                        name: 'Consultar certificados',
                        type: 'link',
                        icon: 'ti-layout-media-overlay'
                    },

                ]
            },
            {
                state: 'manifiestos',
                name: 'Manifiestos',
                type: 'sub',
                icon: 'ti-receipt',
                children: [
                    {
                        state: 'manifiestos',
                        name: 'Consultar manifiestos',
                        type: 'link',
                        icon: 'ti-layout-media-overlay'
                    }/*,
                    {
                        state: 'manifiestosmra',
                        name: 'Manifiestos MRA',
                        type: 'link',
                        icon: 'ti-layout-media-overlay'
                    }*/
                ]
            },
            {
                state: 'documentos',
                name: 'Documentos',
                type: 'sub',
                icon: 'ti-zip',
                children: [
                    {
                        state: 'documentos',
                        name: 'Planes contingencia',
                        type: 'link',
                        icon: 'ti-layout-media-overlay'
                    },
                    {
                        state: 'licencias',
                        name: 'Licencias de tratamiento',
                        type: 'link',
                        icon: 'ti-layout-media-overlay'
                    }
                ]
            },

            {
                state: 'pqs',
                name: 'Servicio al Cliente',
                type: 'sub',
                icon: 'ti-headphone',
                children: [
                    {
                        state: 'pqs',
                        name: 'PQS',
                        type: 'sub',
                        icon: 'ti-direction-alt',
                        children: [
                            {
                                state: 'crear-pqs',
                                name: 'Crear PQS',
                                type: 'link',
                                icon: 'ti-layout-media-overlay'
                            },
                            /*{
                                state: 'historial-pqs',
                                name: 'Historial PQS',
                                type: 'link',
                                icon: 'ti-layout-media-overlay'
                            },*/
                            // {
                            //     state: 'detail-pqs',
                            //     name: 'Detalle PQS',
                            //     type: 'link',
                            //     icon: 'ti-layout-media-overlay'
                            // },
                        ]
                    },
                    {
                        state: 'pqs',
                        name: 'ANS',
                        type: 'sub',
                        icon: 'ti-layout-media-overlay',
                        children: [
                            {
                                state: 'evaluar-ans',
                                name: 'Evaluar ANS',
                                type: 'link',
                                icon: 'ti-layout-media-overlay'
                            },
                        ]
                    },
                    {
                        state: 'pqs',
                        name: 'Encuesta',
                        type: 'sub',
                        icon: 'ti-layout-media-overlay',
                        children: [
                            {
                                state: 'encuestas',
                                name: 'Evaluar encuesta',
                                //   target: true
                            },
                        ]
                    },

                ]
            },

            /*{
                 state: 'servicio-al-cliente',
                 name: 'Servicio al cliente',
                 type: 'sub',
                 icon: 'ti-headphone',
                 children: [
                     {
                         state: 'acuerdo-servicio',
                         name: 'Acuerdo de servicio',
                         type: 'link',
                         icon: 'ti-layout-media-overlay'
                     },
                     {
                         state: 'encuenta',
                         name: 'Encuesta',
                         type: 'link',
                         icon: 'ti-layout-media-overlay'
                     },
                     {
                         state: 'pqs',
                         name: 'PQS',
                         type: 'link',
                         icon: 'ti-clipboard'
                     },
                     {
                         state: 'visita-seguimiento',
                         name: 'Visita de seguimiento',
                         type: 'link',
                         icon: 'ti-layout-media-overlay'
                     },
                 ]
            },*/
            // {
            //     state: 'reportes',
            //     name: 'Reportes',
            //     type: 'sub',
            //     icon: 'ti-blackboard',
            //     children: [
            //         {
            //             state: 'lineas-negocio',
            //             name: 'Generar reportes',
            //             type: 'link',
            //             icon: 'ti-write'
            //         },
            //         {
            //             state: 'solicitudes',
            //             name: 'Consultar reportes',
            //             type: 'link',
            //             icon: 'ti-layout-media-overlay'
            //         },

            //     ]

            // },z

            {

                state: 'authentication',
                name: 'Salir',
                type: 'link',
                icon: 'ti-na'
            }
        ]
    },

];

@Injectable()
export class MenuItems {
    getAll(): Menu[] {
        return MENUITEMS;
    }
}
