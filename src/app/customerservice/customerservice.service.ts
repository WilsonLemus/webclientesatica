import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

export interface Generic {
    id: string;
    label: string;
    data: string;
}
export interface Result {
    status: string;
    id: string;
    result: string;
}

@Injectable()
export class CustomerservicesService {
    private datos;

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    private getQueryRelation(type: string, id: any, or?) {
        let query = '';
        switch (type) {
            case 'procesos':
                query = 'objName=Linea_de_Negocio&relName=R9960761&fieldList=id,name&id='+ id;
                break;
            case 'servicios':
                query = 'objName=Linea_de_Negocio&relName=R9960765&fieldList=id,name&id='+ id;
                break;

            case 'motivos':
                query = 'objName=Tipo_de_producto_pqs&relName=R9562872&fieldList=id,name&id='+ id;
                break;
        }

        return query;
    }

    private getQuery(type: string, id: any, or?) {
        let query = '';
        switch (type) {
            case 'motivos':
                query = 'select id, name from Motivo_PQS where R9562872 in(' + or + ') and R9960048 = ' + id;
                break;
        }
        return query;
    }

    private generics(type: 'motivos' | 'servicios' | 'procesos' | 'unidad' | 'groupers' | 'materials' | 'materialesNormal' | 'packages' | 'sucursal' | 'groupuser' | 'groupersNormal' | 'groupuserCompra' | 'materialuser' | 'materialusercompra', idnum: string, or?: string): Observable<any> {
        let base;
        const query: string = encodeURI(this.getQuery(type, idnum, or));
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=100000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());

        this.datos = base.pipe(
            map(data => {
                let s = JSON.stringify(data);
                s = JSON.parse(s);
                const result = new Array<Generic>();
                for (let i = 0; i < s.length; i++) {
                    let as;
                    let oth = (s[i][2]) ? s[i][2] : '';
                    as = { 'id': s[i][0], 'label': s[i][1], 'data': oth };
                    result.push(as);
                }
                return result;
            })
        );
        return this.datos;
    }

    private genericsRelationShips(type: 'motivos' | 'servicios' | 'procesos', idnum: string, or?: string): Observable<any> {
        let base;
        const query: string = encodeURI(this.getQueryRelation(type, idnum, or));
        base = this.http.get(`${getUrlEnvironment()}/rest/api/getRelationships?startRow=0&maxRows=100000&output=json&${query}&sessionId=${this.auth.getToken()}`);
        this.datos = base.pipe(
            map(data => {
                return data;
            })
        );
        return this.datos;
    }

    public getListBussines(): Observable<any> {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select Nombre_Linea,Descripcin_Lnea_de_Negocio from Linea_de_Negocio where Mostrar=true&sessionId=` + this.auth.getToken() + '&output=json&maxRows=2000'); // =a858106871ad4e6888f46abe2a7305bb@8012571&output=json&maxRows=2000
        this.datos = base.pipe(
            map(data => {
                return data;
            })
        );
        return this.datos;
    }

    public savePqs(pqs): Observable<any> {
        const token = this.auth.getToken();
        let params = this.jsonToParams(pqs);
        let data = `createRecord?&output=json&objName=PQS&useIds=false&sessionId=${token}&${params}`;
        var url = `${getUrlEnvironment()}rest/api/${data}`;
        let base = this.http.get(url);

        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private jsonToParams(data) {
        let url = Object.keys(data).map(function (k) {
            return encodeURIComponent(k) + '=' + encodeURIComponent(data[k])
        }).join('&')
        return url;
    }

    public procesos(id): Observable<any> {
        return this.genericsRelationShips('procesos', id);
    }

    public servicios(id): Observable<any> {
        return this.genericsRelationShips('servicios', id);
    }

    public motivos(serv, id): Observable<any> {
        return this.generics('motivos', id, serv);
    }
}
