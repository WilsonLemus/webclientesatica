import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './polls.component.html',
    styleUrls: ['./polls.component.css']
})
export class PollsComponent implements OnInit {
    // http://plnkr.co/edit/PxBaZs?p=preview
    public data: any;
    public filterQuery = '';
    public rowsOnPage = 20;
    public sortBy = 'Consecutivo_SS';
    public sortOrder = 'desc';

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    public EnterpriseData;
    pagoAnticipado = true;

    constructor(
        private router: Router,
        private toastyService: ToastyService
    ) { }

    ngOnInit() {
        // Se pasa por parametro el tipo de facturas a cargar
        this.listRequest();
        this.EnterpriseData = (sessionStorage.getItem("EntrepriseData")).split(",");
        this.pagoAnticipado = (this.EnterpriseData[13] == 1) ? true : false;
    }

    private listRequest() {
        this.data = [
            {Sucursal: '25/09/2018', OS: 'Vigente', Solicitud: '4.4', Estado: 'Director Regional', Material: 'Detalle >', Reportada: 'Ver'},
            {Sucursal: '24/09/2018', OS: 'Cerrada', Solicitud: '3.9', Estado: 'Coordinador', Material: 'Dtalle >', Reportada: 'Ver'}
        ]
    }

    public view() {
        this.router.navigate(['/encuesta/crear-encuenta']);
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

}
