import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerservicesService, Generic, Result } from '../customerservice.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';

import { Subscription } from 'rxjs'
import { HttpClient, HttpRequest, HttpResponse, HttpEvent } from '@angular/common/http'
import { AuthenticationService } from '../../authentication/authentication.service';
@Component({
    selector: 'app-datatables-products',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css'],
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],
})
// https://github.com/ackerapple/angular-file
export class DetailComponent implements OnInit {
    myFormData: FormData;
    public invoiceForm: FormGroup;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    public data: Generic;
    public sucursal: any;
    public result: Result;
    public EnterpriseData;
    public tipos;
    public procesos;
    public servicios;
    public motivos;
    accept = '*'
    files: File[] = []
    progress: number
    url = 'https://evening-anchorage-3159.herokuapp.com/api/'
    hasBaseDropZoneOver: boolean = false
    httpEmitter: Subscription
    httpEvent: HttpEvent<any>
    lastFileAt: Date
    proc;
    serv;   

    sendableFormData: FormData


    cancel() {
        this.progress = 0
        if (this.httpEmitter) {
            this.httpEmitter.unsubscribe()
        }
    }

    uploadFiles(files: File[]): Subscription {
        const req = new HttpRequest<FormData>('POST', this.url, this.sendableFormData, {
            reportProgress: true//, responseType: 'text'
        })

        return this.httpEmitter = this.HttpClient.request(req)
            .subscribe(
                event => {
                    this.httpEvent = event

                    if (event instanceof HttpResponse) {
                        delete this.httpEmitter
                    }
                },
                error => console.log('Error Uploading', error)
            )
    }

    getDate() {
        return new Date()
    }

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private docServ: CustomerservicesService,
        private toastyService: ToastyService,
        private _fb: FormBuilder,
        public HttpClient: HttpClient,
        private auth: AuthenticationService
    ) { }

    /**
     * Carga el listado de la opción elegida
     * @memberof DetailComponent
     * 
     * CREAR PQS: Tipo PQS. Campo integración: Tipo_PQS (lista despegable: Petición, Queja, Sugerencia). Objeto: PQS
    CREAR PQS: Cliente. Campo integración: R9493390 (relación ID clientes).  Objeto: PQS
    CREAR PQS: Proceso. Campo integración: R9551230 (relación. Objeto: Proceso_PQS). Objeto: PQS
    CREAR PQS: Servicio/producto. Campo integración: R9562842 (relación. Objeto: Tipo_de_producto_pqs). Objeto: PQS
    CREAR PQS: Procedencia. Campo integración: Procedencia (lista desplegable: Auditoría del cliente, Aviso de calidad, Correo del cliente, Llamada del cliente, Visita a planta). Objeto: PQS
    CREAR PQS: Contacto. Campo integración: R9493393 (relacion ID contactos). Objeto: PQS
    CREAR PQS: Motivo. Campo integración: R9551212 (relacion. Objeto: Motivo_PQS). Objeto: PQS
    CREAR PQS: Detalle. Campo integración: Detalle (area de texto). Objeto: PQS
    CREAR PQS:Evidencias. Campo integración: R9961708 (relacion. Objeto: Evidencia_PQS). Objeto: PQS
    VER PQS: Compromiso. Campo integración: Tarea (Área de texto). Objeto: Tareas
    VER PQS: Tiene entregable. Campo integración: Tiene_Entregable_ (botones de radio). Objeto: Tareas
    VER PQS: Entregable. Campo integración: Entregable (area de texto). Objeto: Tareas
     * 
     */
    ngOnInit() {

        this.proc = 0;
        this.serv = 0;   

        let row = this._fb.group({
            Tipo_PQS: [0, [<any>Validators.required, <any>Validators.min(1)]],
            R9493390: [ this.auth.getId(), [<any>Validators.required]],
            R9551230: ['', [<any>Validators.required]],
            R9562842: [0, [<any>Validators.min(1)]],
            Procedencia: [ 'Web clientes', [<any>Validators.required, <any>Validators.min(1)]],
            R9493393: [ this.auth.getId(), [<any>Validators.requiredTrue]],
            R9551212: [''],
            Detalle: [''],
            R9961708: [''],
            Tarea: [true],
            Tiene_Entregable_: [false],
            Entregable: [''],
            Compromiso: [''],
            Servicio_Posfechado_fecha: ['']
        });

        this.invoiceForm = this._fb.group({
            itemRows: this._fb.array([row])
        });

        this.initData();
    }

    initData() {
        this.tipos = [{ id: 'Petición', label: 'Petición' }, { id: 'Queja', label: 'Queja' }, { id: 'Sugerencia', label: 'Sugerencia' }];
        this.loadProcesos();
        this.loadServicios();
    }


    onChangePackage() { }

    onChangeProcess(event) {
        this.proc = event.target.value;
        let serv = this.servicios.map(function (proce) {return proce.id});
        this.loadMotivos(serv, this.proc);
         
    }

    onChangeServices(event) {
        
    }

    loadProcesos() {
        let idLine = sessionStorage.getItem("LineaNegocio");
        this.docServ.procesos(idLine).subscribe(procesos => {
            this.procesos = procesos;
        }, (err) => {
            console.error(err);
        });
    }

    loadServicios() {
        let proc =   sessionStorage.getItem("LineaNegocio");
        this.docServ.servicios(proc).subscribe(servicios => {
            this.servicios = servicios;
        }, (err) => {
            console.error(err);
        });
    }

    loadMotivos(serv, prod) {
        this.docServ.motivos(serv, prod).subscribe(motivos => {
            this.motivos = motivos;
        }, (err) => {
            console.error(err);
        });
    }


    save(){
        if (this.invoiceForm.value.itemRows[0].Tipo_PQS
            && this.invoiceForm.value.itemRows[0].R9551230 
            && this.invoiceForm.value.itemRows[0].R9562842
            && this.invoiceForm.value.itemRows[0].Detalle) {
        }else {
            alert('Por favor complete todos los campos con requeridos identificados con * para realizar la acción');
            return false;
        }
        this.docServ.savePqs(this.invoiceForm.value.itemRows[0]);
    }

    getOut() {
        setTimeout(() => {
            this.router.navigate(['/servicios/solicitudes']);
        }, 3000);
    }


    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
