import { LinesComponent } from './lines/lines.component';
import { Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { VisitsComponent } from './visits/visits.component';
import { PollsComponent } from './polls/polls.component';
import { DetailpqsComponent } from './list/detailpqs.component';
import { CreatepollComponent } from './polls/createpoll.component';
import { ExecutepollComponent } from './polls/executepoll.component';
import { AgreementComponent } from './agreement/agreement.component';

export const CustomerservicesRoutes: Routes = [
  /*{
    path: '',
    redirectTo: 'facturas',
    pathMatch: 'full'
  },*/
  {
    path: 'pqs',
    component: ListComponent,
    data: {
      breadcrumb: 'PQS',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'crear-pqs',
    component: DetailComponent,
    data: {
      breadcrumb: 'Nueva PQS',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'detalle-pqs',
    component: DetailpqsComponent,
    data: {
      breadcrumb: 'Detalle PQS',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  /*{
    path: 'visita-operativa',
    component: DetailComponent,
    data: {
      breadcrumb: 'Visita operativa',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },*/
  {
    path: 'visita-seguimiento',
    component: VisitsComponent,
    data: {
      breadcrumb: 'Visita de seguimiento',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  /*{
    path: 'acuerdo-servicio',
    component: DetailComponent,
    data: {
      breadcrumb: 'Acuerdo de servicio',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },*/
  {
    path: 'encuenta',
    component: PollsComponent,
    data: {
      breadcrumb: 'Encuenta',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'crear-encuenta',
    component: CreatepollComponent,
    data: {
      breadcrumb: 'Encuenta de satisfacción',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'resolver-encuenta',
    component: ExecutepollComponent,
    data: {
      breadcrumb: 'Encuenta de satisfacción',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'acuerdo-servicio',
    component: AgreementComponent,
    data: {
      breadcrumb: 'Acuerdo de nivel de servicio',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
