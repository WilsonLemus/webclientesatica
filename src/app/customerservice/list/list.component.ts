import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerservicesService } from '../customerservice.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    // http://plnkr.co/edit/PxBaZs?p=preview
    public data: any;
    public filterQuery = '';
    public rowsOnPage = 20;
    public sortBy = 'Consecutivo_SS';
    public sortOrder = 'desc';

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    public EnterpriseData;
    pagoAnticipado = true;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private docServ: CustomerservicesService,
        private auth: AuthenticationService,
        private toastyService: ToastyService
    ) { }

    ngOnInit() {
        // Se pasa por parametro el tipo de facturas a cargar
        this.listRequest();
        this.EnterpriseData = (sessionStorage.getItem("EntrepriseData")).split(",");
        this.pagoAnticipado = (this.EnterpriseData[13] == 1) ? true : false;
    }

    private listRequest() {
        this.data = [
            {Sucursal: '25/09/2018', OS: 'P-003245', Solicitud: 'PETICIÓN', Estado: 'LOGÍSTICA', Material: 'Ver detalle >', Reportada: 'ABIERTA'},
            {Sucursal: '24/09/2018', OS: 'P-003243', Solicitud: 'QUEJA', Estado: 'COMERCIAL', Material: 'Ver detalle >', Reportada: 'CERRADA'}
        ]
    }

    public view() {
        this.router.navigate(['/servicio-al-cliente/detalle-pqs']);
    }

    openPdf(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Factura1&id=` + id + "&fieldName=Doc_Fact&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }

    public sortByWordLength = (a: any) => {
        return a.id.length;
    }


    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

}
