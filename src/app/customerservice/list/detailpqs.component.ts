import { Component, OnInit, ViewChild, trigger, state, animate, transition, style } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CustomerservicesService } from '../customerservice.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { FormBuilder } from '@angular/forms';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './detailpqs.component.html',
    styleUrls: ['./list.component.css'],
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],
})

export class DetailpqsComponent implements OnInit {
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;

    constructor(
        private router: Router,
        private toastyService: ToastyService
    ) { }
    /**
     * Carga el listado de la opción elegida
     * @memberof DetailComponent
     */
    ngOnInit() {
    }
    
    getOut() {
        setTimeout(() => {
            this.router.navigate(['/servicios/solicitudes']);
        }, 3000);
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
