import { LinesComponent } from './lines/lines.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { CustomerservicesRoutes } from './customerservice.routing';
import { HttpModule } from '@angular/http';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { SharedModule } from '../shared/shared.module';
import { CustomerservicesService } from './customerservice.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { AuthenticationService } from '../authentication/authentication.service';
import {DataTableModule} from 'angular2-datatable';
import { DataFilterPipe } from './data-filter.pipe';
import { AuthGuardService } from '../authentication/auth-guard.service';
import {AngularEchartsModule} from 'ngx-echarts';
import { PollsComponent } from './polls/polls.component';
import { VisitsComponent } from './visits/visits.component';
import { DetailpqsComponent } from './list/detailpqs.component';
import { CreatepollComponent } from './polls/createpoll.component';
import { ExecutepollComponent } from './polls/executepoll.component';
import { AgreementComponent } from './agreement/agreement.component';
//import { ngfModule } from 'angular-file';

@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(CustomerservicesRoutes),
    SharedModule,
    HttpClientModule,
    AuthenticationModule,
    DataTableModule,
    AngularEchartsModule,
    //ngfModule
  ],
  providers: [
    CustomerservicesService,
    AuthenticationService,
    AuthGuardService
  ],
  declarations: [
    ListComponent, 
    DetailComponent, 
    LinesComponent, 
    DataFilterPipe, 
    PollsComponent,
    CreatepollComponent, 
    VisitsComponent,
    DetailpqsComponent,
    ExecutepollComponent,
    AgreementComponent
  ]
})

export class CustomerservicesModule {}

