import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { animate, style, transition, trigger } from '@angular/animations';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import '../../../assets/echart/echarts-all.js';

@Component({
	selector: 'app-datatables-products',
	templateUrl: './lines.component.html',
	styleUrls: ['./lines.component.css']
})
export class LinesComponent implements OnInit {
	editProfile = true;
	editProfileIcon = 'icofont-edit';
	position = 'botton-right';
	title: string;
	msg: string;
	showClose = true;
	timeout = 5000;
	theme = 'bootstrap';
	type = 'default';
	closeOther = false;

	editAbout = true;
	editAboutIcon = 'icofont-edit';

	public editor;
	public data: any;
	public bussines: any;
	public rowsOnPage = 10;
	public filterQuery = '';
	public sortBy = '';
	public sortOrder = 'desc';
	profitChartOption: any;

	constructor(
		private route: ActivatedRoute,
		private router: Router,
		private toastyService: ToastyService
	) { }

	ngOnInit() {
		setTimeout(() => {
			this.profitChartOption = {
				tooltip: {
					trigger: 'item',
					formatter: function (params) {
						const date = new Date(params.value[0]);
						let data = date.getFullYear() + '-' + (date.getMonth() + 1) + '-' + date.getDate() + ' ';
						data += date.getHours() + ':' + date.getMinutes();
						return data + '<br/>' + params.value[1] + ', ' + params.value[2];
					},
					responsive: true
				},
				dataZoom: {
					show: true,
					start: 70
				},
				legend: {
					data: ['Profit']
				},
				grid: {
					y2: 80
				},
				xAxis: [{
					type: 'time',
					splitNumber: 10
				}],
				yAxis: [{
					type: 'value'
				}],
				series: [{
					name: 'Profit',
					type: 'line',
					showAllSymbol: true,
					symbolSize: function (value) {
						return Math.round(value[2] / 10) + 2;
					},
					data: (function () {
						const d: any = [];
						let len = 0;
						const now = new Date();
						while (len++ < 200) {
							const random1: any = (Math.random() * 30).toFixed(2);
							const random2: any = (Math.random() * 100).toFixed(2);
							d.push([new Date(2014, 9, 1, 0, len * 10000), random1 - 0, random2 - 0]);
						}
						return d;
					})()
				}]
			};
		}, 1);
	}

	toggleEditProfile() {
		this.editProfileIcon = (this.editProfileIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
		this.editProfile = !this.editProfile;
	}

	toggleEditAbout() {
		this.editAboutIcon = (this.editAboutIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
		this.editAbout = !this.editAbout;
	}

	onEditorBlured(quill) {
		console.log('editor blur!', quill);
	}

	onEditorFocused(quill) {
		console.log('editor focus!', quill);
	}

	onEditorCreated(quill) {
		this.editor = quill;
		console.log('quill is ready! this is current quill instance object', quill);
	}

	onContentChanged({ quill, html, text }) {
		console.log('quill content is changed!', quill, html, text);
	}

	contact(id) {
		this.addToast(
			{
				title: 'Acción exitosa',
				msg: 'La solicitud se creo exitosamente',
				timeout: this.timeout,
				theme: this.theme,
				position: this.position,
				type: 'success'
			});
	}

	addToast(options) {
		if (options.closeOther) {
			this.toastyService.clearAll();
		}
		this.position = options.position ? options.position : this.position;
		const toastOptions: ToastOptions = {
			title: options.title,
			msg: options.msg,
			showClose: options.showClose,
			timeout: options.timeout,
			theme: options.theme,
			onAdd: (toast: ToastData) => {
				console.log('Toast ' + toast.id + ' has been added!');
			},
			onRemove: (toast: ToastData) => {
				console.log('Toast ' + toast.id + ' has been added removed!');
			}
		};

		switch (options.type) {
			case 'default': this.toastyService.default(toastOptions); break;
			case 'info': this.toastyService.info(toastOptions); break;
			case 'success': this.toastyService.success(toastOptions); break;
			case 'wait': this.toastyService.wait(toastOptions); break;
			case 'error': this.toastyService.error(toastOptions); break;
			case 'warning': this.toastyService.warning(toastOptions); break;
		}
	}
}
