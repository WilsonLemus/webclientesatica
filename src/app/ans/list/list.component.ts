import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AnsService, Generic, Result } from '../ans.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-pqs',
    templateUrl: './list.component.html',
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],

})

export class AnsListComponent implements OnInit {
    public data: any;
    public porcentajeAprobado: any;
    public calificaciones = [];
    public rowsOnPage = 5;
    public sortBy = 'name';
    public sortOrder = 'desc';
    public result: Result;
    public nombreContacto = this.auth.getContactoNombre();
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    public porcentajeCliente = 0;
    public porcentajeCoordinador: any;

    constructor(private AnsService: AnsService, private toastyService: ToastyService, private _fb: FormBuilder, private modalService: NgbModal, private router: Router, private auth: AuthenticationService) { }

    // Funciones que se realizaran al cargar la pantalla.

    ngOnInit() {
        this.obtenerCompromisos();
    }

    // Trae los compromisos de rollbase y realiza un porcentaje de la calificación del coordinador.

    obtenerCompromisos() {
        this.AnsService.getCompromisos().subscribe(compromisos => {
            this.data = compromisos;
            var contador = 0;
            for(let i = 0; i < this.data.length; i++){
                if (this.data[i][2] == 12660290){
                    contador++;
                }
            }
            var formula = contador * 100 / this.data.length;;
            this.porcentajeCoordinador = Math.round(formula);
        }, (err) => {
            console.error(err);
        });
    }

    // Guarda la calificación en un JSON.

    guardarCalificacion(idCompromiso) {
        var split = idCompromiso.split("-"   );
        if (this.calificaciones.length == 0) {
            this.calificaciones.push({
                idCompromiso: split[0],
                idCalificacion: split[1],
                observacion: 'Sin justificación'
            });
        } else {
            var valido = 0;
            for (var i = 0; i < this.calificaciones.length; i++) {
                if (valido == 0) {
                    if (split[0] == this.calificaciones[i].idCompromiso) {
                        this.calificaciones[i].idCalificacion = split[1];
                        valido = 1;
                    }
                }
            }
            if (valido == 0) {
                this.calificaciones.push({
                    idCompromiso: split[0],
                    idCalificacion: split[1],
                    observacion: 'Sin justificación'
                });
            }
        }
        this.porcentajeCalificado();
        // console.log(this.calificaciones);
    }

    // Guarda las observaciones en un JSON.

    guardarObservacion(Obs) {
        var split = Obs.split("-");
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (split[0] == this.calificaciones[i].idCompromiso) {
                this.calificaciones[i].observacion = split[1];
            }
        }
        // console.log(this.calificaciones);
    }

    // Realiza un porcentaje de la calificación del cliente.

    porcentajeCalificado(){
        var aprobados = 0;
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].idCalificacion == 12660287){
                aprobados++;
            }
        }
        var formula = (aprobados*100)/this.data.length;
        this.porcentajeCliente = Math.round(formula);
    }

    // Valida si hay compromisos por calificar.

    porCalificado(){
        var aprobados = 0;
        for (var i = 0; i < this.calificaciones.length; i++) {
            if (this.calificaciones[i].idCalificacion == 12660287){
                aprobados++;
            }
        }
        var formula = (aprobados*100)/this.calificaciones.length;
        this.porcentajeAprobado = Math.round(formula);
    }

    // VENTANA DE NOTIFICACIÓN

    open(content){
        var totalCompromisos = 0;
        for (let i = 0; i < this.data.length; i++){
            if (this.data[i][3] == 1){
                totalCompromisos++;
            }
        }
        if (this.calificaciones.length != totalCompromisos){
            let Mens = 'Tiene compromisos por calificar';
            this.addToast({
                title: 'Error',
                msg: Mens,
                timeout: 5000,
                theme: 'default',
                position: 'bottom.right',
                type: 'warning'
            });
        } else {
            this.porCalificado();
            this.modalService.open(content);
        }
    }

    // Salir al dashboard principal

    getOut() {
        setTimeout(() => {
            this.router.navigate(['/dashboard']);
        }, 1000);
    }

    // GUARDAR ANS

    saveGetOut() {
        this.createANS();
    }

    // Crea el ANS en rollbase.

    createANS(){
        for (let i = 0; i < this.calificaciones.length; i++){
            var calificacion = [{
                idCompromiso: this.calificaciones[i].idCompromiso,
                idCalificacion: this.calificaciones[i].idCalificacion,
                observacion: this.calificaciones[i].observacion    
            }];
            this.AnsService.saveAns(calificacion).subscribe(services =>{
                this.result = services;
            })
        }
        this.AnsService.idAnsPendiente().subscribe(id => {
            this.AnsService.pendienteCalificacion(id, this.porcentajeAprobado).subscribe(pendiente => {
                if (pendiente == 'ok'){
                    let Mens = 'Su ANS se guardo correctamente';
        
                this.addToast({
                    title: 'Acción exitosa',
                    msg: Mens,
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom.right',
                    type: 'success'
                });
                }
            })
        })
    }

    // Funcion para notificaciones flotantes

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}