import { Injectable } from "@angular/core";
import { getUrlEnvironment } from '../shared/environment';
import { AuthenticationService } from '../authentication/authentication.service';
import { map } from 'rxjs/operators/map';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';

export interface Generic {
    id: number;
    label: string;
    data: string;
}

export interface Result {
    status: string;
    result: string;
    id: string;
}


@Injectable()
export class AnsService {

    constructor(private http: HttpClient, private auth: AuthenticationService) { }

    // Trae las sucursales del cliente en rollbase.

    public getSucursales(){
        const cliente = this.auth.getIdUserLoggedIn();
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + cliente + '&relName=R8173502&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
        const request = base.pipe(
            map ((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Guarda la id de la sucursal que se esta calificando en ANS en un item de sesión.

    public saveIdSucursal(ans: any, Formlen: number){
        for (let i = 0; i < Formlen; i++) {
            sessionStorage.setItem('idSucursalAns', ans.itemRows[i].sucursalAns);
            return 0;
        }
    }

    // Obtiene la id de la sucursal que se esta calificando en ANS.

    public getIdSucursal(){
        let idSucursal = sessionStorage.getItem("idSucursalAns");
        return idSucursal;
    }

    // Trae los compromisos de la sucursal que se esta calificando en ANS.

    public getCompromisos(){
        let idSucursal = sessionStorage.getItem("idSucursalAns");
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT id,Observaciones,Calificacin_Cordinador,Vigente FROM Calificacion_de_Compromiso WHERE R10870600 =` + idSucursal + ` AND Vigente=1&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Valida sii la ANS se encuentra pendiente por calificar.

    public comprobarAns(idSucursal){
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT Pendiente_de_calificacin FROM Calificaciones_de_ANS WHERE R10858093 =` + idSucursal + ` AND Vigente=1 &sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Guardar ANS

    public idAnsPendiente(){
        let idSucursal = sessionStorage.getItem("idSucursalAns");
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query= SELECT id FROM Calificaciones_de_ANS WHERE R10858093 =` + idSucursal + ` AND Vigente=1&sessionId=` + this.auth.getToken() + '&output=json&maxRows=1000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // Cambia el check de pendiente por calificar de la ANS.

    public pendienteCalificacion(id, porcentaje){
        return this.letPendiente(id, porcentaje);
    }

    private letPendiente(id, porcentaje){
        let base;
        let url = this.getQueryPendiente(id, porcentaje);
        console.log(url);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    private getQueryPendiente(id, porcentaje){
        const token = this.auth.getToken();
        let params = `&Porcentaje_cliente=${porcentaje}`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Calificaciones_de_ANS&id=${id}&sessionId=` + token + params;
        return url;
    }

    // Guarda el ANS en rollbase.

    public saveAns(calificaciones = []){
        return this.letAns(calificaciones);
    }

    private letAns(calificaciones = []){
        let base;
        for (let i = 0; i < calificaciones.length; i++){
            let url = this.getSetQuery(calificaciones, i);
            console.log(url);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }
    }

    private getSetQuery(calificaciones = [], i: number){
        const token = this.auth.getToken();
        let params = `&Calificacin_Cliente=${+calificaciones[i].idCalificacion}&Observacion_cliente=${calificaciones[i].observacion}`;
        var url = `${getUrlEnvironment()}rest/api/update2?output=json&useIds=true&objName=Calificacion_de_Compromiso&id=${calificaciones[i].idCompromiso}&sessionId=` + token + params;
        return url;
    }
}