import { Component, OnInit, trigger, state, animate, transition, style } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AnsService, Generic, Result } from '../ans.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../authentication/authentication.service';

@Component({
    selector: 'app-pqs',
    templateUrl: './ans.component.html',
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],

})

export class AnsDetailComponent implements OnInit {
    ansForm = new FormGroup({
        sucursalAns: new FormControl(''), 
    });
    public sucursalAns: any;
    public nombreContacto = this.auth.getContactoNombre();
    public tipoContacto = sessionStorage.getItem("tipoContacto");
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;

    constructor(private AnsService: AnsService, private toastyService: ToastyService, private _fb: FormBuilder, private modalService: NgbModal, private router: Router, private auth: AuthenticationService) { }

    // Funciones que se realizaran al cargar la pantalla.

    ngOnInit() {

        // Inicia el formulario

        this.ansForm = this._fb.group({
            itemRows: this._fb.array([this.initItemRows()])
        });

        // Obtiene las sucursales

        this.AnsService.getSucursales().subscribe(sucursales => {
            this.sucursalAns = sucursales;
        }, (err) => {
            console.error(err);
        });
    }

    // Validación de sucursal, revisa en rollbase si ya se encuentra calificada.

    calificacionAns(){
        this.AnsService.comprobarAns(this.ansForm.value.itemRows[0].sucursalAns).subscribe(prueba => {
            console.log(prueba[0][0]);
            if (prueba[0][0] == 150763){
                const formlen = this.ansForm.value.itemRows.length;
                this.AnsService.saveIdSucursal(this.ansForm.value, formlen);
                this.router.navigate(['pqs/callificar-ans']);
                console.log("Califica");
            } else {
                console.log("No califica");
                let Mens = 'La ANS ya se ha calificado';
                this.addToast({
                    title: 'Error',
                    msg: Mens,
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom.right',
                    type: 'warning'
                });
            }
        }, (err) => {
            console.error(err);
        });
    }

    
    // Inicia el formulario con el modelo de solicitud
    
    initItemRows() {
        return this._fb.group({
            sucursalAns: ['', [<any>Validators.required]],
        });
    }

    // Agrega notificación flotante

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}

