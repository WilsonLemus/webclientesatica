import { NgModule } from "@angular/core";
import { HttpModule } from "@angular/http";
import { CommonModule } from "@angular/common";
import { RouterModule } from "@angular/router";
import { SharedModule } from "../shared/shared.module";
import { AnsDetailComponent } from './detail/ans.component';
import { AnsListComponent } from './list/list.component';
import { DataFilterPipe } from './data-filter.pipe';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AnsRoutes } from './ans.routing';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { DataTableModule } from 'angular2-datatable';
import { AngularEchartsModule } from 'ngx-echarts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AnsService } from './ans.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    imports: [
        HttpModule,
        CommonModule,
        RouterModule.forChild(AnsRoutes),
        SharedModule,
        HttpClientModule,
        AuthenticationModule,
        DataTableModule,
        AngularEchartsModule,
        FormsModule,
        ReactiveFormsModule,
    ],
    providers: [
        AuthGuardService,
        AnsService,
        NgbActiveModal,
        NgbModal,
        AnsDetailComponent,
        AnsListComponent, 
    ],
    declarations: [AnsDetailComponent, AnsListComponent, DataFilterPipe]
})

export class AnsModule { }