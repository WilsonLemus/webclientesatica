import { Routes } from '@angular/router';
import { AnsDetailComponent } from './detail/ans.component';
import { AnsListComponent } from './list/list.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const AnsRoutes: Routes = [
  {
    path: 'pqs/evaluar-ans',
    component: AnsDetailComponent,
    data: {
      breadcrumb: 'Evaluar ANS',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'callificar-ans',
    component: AnsListComponent,
    data: {
      breadcrumb: 'Calificar ANS',
    },
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
]