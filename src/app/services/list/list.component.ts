import { Component, OnInit } from '@angular/core';
import { ServicesService, Services } from '../services.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    // http://plnkr.co/edit/PxBaZs?p=preview
    public data: Services;
    public filterQuery = '';
    public filterQuerySuc = '';
    public filterQueryFecha = '';
    public rowsOnPage = 20;
    public sortBy = 'Consecutivo_SS';
    public sortOrder = 'desc';

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    public EnterpriseData;
    pagoAnticipado = true;

    constructor(
        private docServ: ServicesService,
        private auth: AuthenticationService,
        private toastyService: ToastyService
    ) { }

    ngOnInit() {
        // Se pasa por parametro el tipo de facturas a cargar
        this.listRequest();
        this.EnterpriseData = (sessionStorage.getItem("EntrepriseData")).split(",");
        this.pagoAnticipado = (this.EnterpriseData[13] == 1) ? true : false;
    }

    /**
     * Lista las solicitudes
     */
    private listRequest() {
        // optiene las sucursales
        this.docServ.sucursal().subscribe(sucursal => {
            //     // optiene las unidades
            this.docServ.getUni().subscribe(unidades => {
                // optiene las solicitudes
                this.docServ.list(unidades, sucursal).subscribe(services => {
                    // guarda en data el listado para imprimir en pantalla
                    this.data = services;
                }, (err) => {
                    console.error(err);
                });
            }, (err) => {
                console.error(err);
            });
        }, (err) => {
            console.error(err);
        });
    }


    /**
     * Abre el pdf
     * @param id id de la solicitud
     */
    openPdf(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Factura1&id=` + id + "&fieldName=Doc_Fact&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }

    public sortByWordLength = (a: any) => {
        return a.id.length;
    }

    /**
     * Borra la solicitud si cumple con las condiciones
     * @param id id de la solicitud
     */
    delete(id) {
        if (confirm("Esta seguro de anular la solicitud ")) {
            this.docServ.deleteRequest(id).subscribe(result => {
                if (result) {
                    return (result);
                }
            });
            this.listRequest();
            // Se pasa por parametro el tipo de facturas a cargar
            /*
            this.docServ.deleteRequest(id).subscribe(result => {
                if (result.status == "ok") {
                    this.addToast(
                        {
                            title: 'Acción exitosa',
                            msg: 'La solicitud se anuló exitosamente',
                            timeout: 5000,
                            theme: 'default',
                            position: 'bottom-right',
                            type: 'success'
                        });
                    this.listRequest();
                }
            }, (err) => {
                console.log(err);
            });
            */
        }
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }

}
