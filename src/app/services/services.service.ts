import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

export interface ServicesDetail {
    No_Os: String;
    Consecutivo_SS: String;
    Nom_Material: String;
    Cantidad_en_Kg: String;
    Nom_Embalaje: String;
    Cantidad_Embalaje: String;
    status: string;
    Peso_Recogidos_Kg: string;
    Peso_Aproximado_Kg: string;
    Compromiso_Servicio: string;
}

export interface Generic {
    id: number;
    label: string;
    data: string;
}

export interface Result {
    status: string;
    id: string;
    result: string;
}

export interface Status {
    id: string;
    status: string;
    motive: String;
}

export interface Services {
    Services: Array<ServicesDetail>;
}
export interface References {
    name: string;
    lote: string;
    cantidad: number;
    un: string;
    embalaje: string;
}
export interface datos {
    query: string;
    output: string;
    startRow: number;
    maxRows: number;
    sessionId: string;
    url: string;
}

@Injectable()
export class ServicesService {
    private datos;
    public retdatos;

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    private listServices(unidades, sucursales): Observable<any> {
        let base;
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        //alert("LINEA: " + idLineaN);
        const url = this.getQueryListServices(idSucursal, idLineaN);
        base = this.http.get(url);
        const request = base.pipe(
            map((data: any) => {
                return this.processListServices(data, unidades, sucursales);
            })
        );
        return request;
    }


    private processListServices(data: any, unidades: any, sucursales: any) {
        var servicios = [];
        for (let i = 0; i < data.length; i++) {
            var resFecha = '';
            if (data[i][11]) {
                let from: Date = new Date(data[i][11]);
                var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
                const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
                const day = (from.getDate() < 9) ? '0' + from.getDate() : from.getDate();
                resFecha = from.getFullYear() + '/' + mon + '/' + day;
            } else {
                resFecha = 'Pendiente de asignación';
            }
            let un = data[i][12];
            let un2 = data[i][13];
            let uniName = '';
            const uni = unidades.filter(tipo => ((tipo.id == un) || (tipo.id == un2)));
            if (uni.length > 0) {
                uniName = uni[0].label;
            }
            let sucName = data[i][14];
            const suc = data[i][14];
            const res = sucursales.filter(tipo => tipo.id == suc);
            if (res.length > 0) {
                sucName = res[0].label;
            }

            if ((data[i][6] == 'Programada') || (data[i][6] == 'Asignada a ruta') || (data[i][6] == 'Cargada') || (data[i][6] == 'Cerrada')) {
                data[i][15] = '';
                data[i][16] = '';
            } else if ((data[i][6] == 'Almacenada') || ((data[i][6] == 'Tratada') && (data[i][16] == ''))) {
                data[i][16] = '';
            } else if ((data[i][6] == 'Subcontratada') || ((data[i][6] == 'Tratada') && (data[i][16] != ''))) {
                data[i][15] = '';
            }

            let resu = {
                Sucursal: sucName,
                OS: data[i][0],
                Solicitud: data[i][1],
                Estado: data[i][6],
                almacen: data[i][15],
                dispositor: data[i][16],
                Material: data[i][2],
                Reportada: data[i][3],
                Recogida: data[i][9],
                Embalaje: data[i][4],
                CtnEmbalaje: data[i][5],
                servicio: resFecha,
                Valor: data[i][8],
                id: data[i][7]
            };
            servicios.push(resu);
        }
        return servicios;
    }

    private getQueryListServices(idSucursal, idLineaN) {
        var query = 'select No_Os,Consecutivo_SS,Nom_Material,Cantidad_en_Kg,Nom_Embalaje,Cantidad_Embalaje,status';
        var query2 = 'value, id, Valor_solicitud, Peso_Recogidos_Kg,Peso_Aproximado_Kg,';
        var query5 = 'Fecha_Prestar_Servicio,UM_P_Venta,UM_P_Compra,Sucursal,Nombre_Almacen,Nombre_Dispositor from Solicitud_Servicio where R8634058=' + this.auth.getId() + ' and Sucursal in(' + idSucursal + ') and Lnea_de_Negocio=' + idLineaN + ' and status';
        let query6 = 'value is not null order by id Desc&sessionId=' + this.auth.getToken();
        let totQue = encodeURI(query) +
            encodeURIComponent('#') + encodeURI(query2) +
            encodeURI(query5) +
            encodeURIComponent('#') + encodeURI(query6);
        const url = `${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=1000&output=json&query=` + totQue;

        return url;
    }

    private getQuery(type: string, id: any, or?) {
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        //   console.log("SUC", idSucursal);
        let query = '';
        const userId = this.auth.getIdUserLoggedIn();
        switch (type) {
            // por tratamiento
            case 'groupers':
                query = 'select id, name from Agrupador order by name';
                break;

            case 'groupersNormal':
                query = 'select id, name from Agrupador where id in(' + id + ', ' + or + ') order by name';
                break;

            case 'groupuserCompra':
                query = `select distinct(RPCompra_Agrup) from Precios_de_Compra where RPCompra_Cte=${userId} and R9084365=${id[0].id}`;
                break;

            case 'materials':
                query = 'select id, name, Sustancia_Controlada from Material where Cdigo_SAP is not null and R8146476=' + id + ' order by name';
                break;

            case 'materialesNormal':
                query = 'select id, name, Sustancia_Controlada from Material where R8146476=' + id + ' and id in (' + or + ') and Vterk=' + idLineaN + ' order by name';
                break;

            case 'packages':
                query = 'select id, name from Tipo_Embalaje order by name';
                break;

            case 'treatment':
                query = 'select id, name from R9482977,Nombre_Tto from Tratamientos_Negociados where R9482973 = ' + userId + ' order by name';
                break;
            case 'sucursal':
                query = 'select id,name,Principal from Sucursal1 where Cdigo_SAP is not null and Deudor is not null and R8173502=' + userId + ' and id in(' + idSucursal + ') order by name';
                break;

            case 'groupuser':
                if (id[0].data) {
                    query = `select distinct R9955813 from Tabla_Precios where R9955813 is not null and R_Cliente_TP=${userId} and (R8698961=${id[0].id} or R8698961 is null)`;
                } else {
                    query = `select distinct R9955813 from Tabla_Precios where R9955813 is not null and R_Cliente_TP=${userId} and R8698961=${id[0].id}`;
                }
                break;

            case 'materialuser':
                if (id[0].data) {
                    query = `select distinct R_Material_TP, UnidadMedida from Tabla_Precios where R_Material_TP is not null and R_Cliente_TP=${userId} and (R8698961=${id[0].id} or R8698961 is null)`;
                } else {
                    query = `select distinct R_Material_TP, UnidadMedida from Tabla_Precios where R_Material_TP is not null and R_Cliente_TP=${userId} and R8698961=${id[0].id}`;
                }
                break;

            case 'materialusercompra':
                query = `select distinct RPCompra_Material, Unidad_de_Medida from Precios_de_Compra where RPCompra_Cte=${userId} and R9084365=${id[0].id}`;
                break;

            case 'unidad':
                query = 'select id,name from Unidades_Medidas order by name';
                break;

            case 'centerGroup':
                query = 'select R9955799 from Agrupador_Material_Centro where id in(' + id + ') order by R9955799';
                break;
        }
        return query;
    }

    private generics(type: 'centerGroup' | 'unidad' | 'groupers' | 'materials' | 'materialesNormal' | 'packages' | 'sucursal' | 'groupuser' | 'groupersNormal' | 'groupuserCompra' | 'materialuser' | 'materialusercompra' | 'SolDescargada', idnum: string, or?: string): Observable<any> {
        let base;
        const query: string = encodeURIComponent(this.getQuery(type, idnum, or));
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=100000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());
        //console.log(query);
        this.datos = base.pipe(
            map(data => {
                // console.log(data);
                let s = JSON.stringify(data);
                s = JSON.parse(s);
                const result = new Array<Generic>();
                for (let i = 0; i < s.length; i++) {
                    let as;
                    let oth = (s[i][2]) ? s[i][2] : '';
                    as = { 'id': s[i][0], 'label': s[i][1], 'data': oth };
                    result.push(as);
                }
                return result;
            })
        );
        return this.datos;

        /* let base;
        const query: string = this.getQuery(type, idnum, or);
        const url = `${getUrlEnvironment()}rest/api/selectQuery`;
        let data: datos = {
            query: query,
            output: 'json',
            maxRows: 99999,
            startRow: 0,
            sessionId: this.auth.getToken(),
            url: url
        }*/

        // this.retdatos = this.loadPost(data);
        // alert(this.retdatos);
        //console.log(query);
        //  this.datos = base.pipe(
        //      map(data => {
        //          console.log(data);
        //          let s = JSON.stringify(data);
        //          s = JSON.parse(s);
        //          const result = new Array<Generic>();
        //          for (let i = 0; i < s.length; i++) {
        //              let as;
        //              let oth = (s[i][2]) ? s[i][2] : '';
        //              as = { 'id': s[i][0], 'label': s[i][1], 'data': oth };
        //              result.push(as);
        //          }
        //          return result;
        //      })
        //  );

    }

    public getListBussines(): Observable<any> {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select Nombre_Linea,Descripcin_Lnea_de_Negocio from Linea_de_Negocio where Mostrar=true order by Nombre_Linea&sessionId=` + this.auth.getToken() + '&output=json&maxRows=2000'); // =a858106871ad4e6888f46abe2a7305bb@8012571&output=json&maxRows=2000
        this.datos = base.pipe(
            
            map(data => {
                console.log(data,"--------------Linea--------");
                return data;
            })
        );
        return this.datos;
    }
    public getDatoSociedad(idLineaN, sucursal): Observable<any> {
        let sessionid = this.auth.getToken();
        let base;
        let query = encodeURI(`SELECT id from DatosSociedad where R9179275=${idLineaN} and R10775237=${sucursal}` + "&sessionId=" + sessionid + "&output=json");
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=` + query);
        const request = base.pipe(
            map((data: any) => {
                return data;
            })
        );
        return request;
    }

    private createRequest(service: any, formlen, branch: string, orderId, datoSociedad): Observable<any> {
        if (orderId == 0) {
            let base;
            var url = this.getQueryCreteRequest(service, branch, datoSociedad);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    let as;
                    for (let i = 0; i < formlen; i++) {
                        as = this.CreateService(data.id, service.itemRows[i]);
                    }
                    return data;
                })
            );
            return request;
        } else {
            this.CreateService(orderId, service.itemRows[0]);
        }
    }

    private getQueryCreteRequest(service: any, branch: string, datoSociedad) {
        const userId = this.auth.getIdUserLoggedIn();
        const token = this.auth.getToken();
        let params = `&Recoleccin=${+service.itemRows[0].Recoleccion}&Presencial=${+service.itemRows[0].Presenciales}&Recepcin=${+service.itemRows[0].Recepcion}&Posfechar_Servicios=${service.itemRows[0].Servicio_Posfechado_fecha}&Anticipo=${+service.itemRows[0].Pago_Anticipo}&R10775472=${datoSociedad}`;
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Orden_de_Servicio&sessionId=` + token + `&R8183919=` + userId + '&R9026340=' + branch + '&Aceptacin_Responsabilidad_de_residuos=true' + params;
        return url;
    }

    private CreateService(OrderId: string, service: any) {
        try {
            let url = this.getQueryCreateService(OrderId, service);
            this.http.get(url).subscribe(res => {
                if (res) {
                    return (res);
                }
            });
        } catch (err) {
            console.log(err);
        }
    }

    private getQueryCreateService(OrderId: string, service: any) {
        const userId = this.auth.getIdUserLoggedIn();
        const token = this.auth.getToken();
        let url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Solicitud_Servicio&sessionId=` + token + `&R8634058=` + userId;
        url = url + '&R8206105=' + OrderId + '&R8602650=' + service.grouper + '&R8206161=' + service.materials +
            '&Cantidad_en_Kg=' + service.qty + '&R8208895=' + service.pk + '&Cantidad_Embalaje=' + service.qtyPack;
        return url;
    }

    public createQuotation(quota): Observable<any> {

        let base;
        const userId = this.auth.getIdUserLoggedIn();
        const token = this.auth.getToken();
        const url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Cotizacin_Nuevo_Material&sessionId=` + token + `&R8604192=` + userId + '&Material_a_Cotizar=' + quota;
        base = this.http.get(encodeURI(url));
        const request = base.pipe(
            map((data: Result) => {
                return data;
            })
        );
        return request;
    }

    public listQoute(): Observable<any> {
        let base;
        const userId = this.auth.getIdUserLoggedIn();
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, Material_a_Cotizar from Cotizacin_Nuevo_Material where R8604192=` + userId + ' order by id&sessionId=' + this.auth.getToken() + '&output=json&maxRows=2000');
        this.datos = base.pipe(
            map(data => {
                return data;
            })
        );
        return this.datos;
    }
    public listSolicitudesDescargadas(): Observable<any> {
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        let sessionid = this.auth.getToken();
        let query = encodeURI("select id, name, Nombre_Sucursal, Orden_Servicio, Estado_web") + encodeURIComponent("#") + encodeURI("id, Nom_Material, Lote, Peso_Recogidos_Kg, Nom_Embalaje,Unidad_de_Medida  from Solicitud_Servicio where status=10911156 and Sucursal in(" + idSucursal + ") and Lnea_de_Negocio=" + idLineaN + " ");
        let url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionid + "&output=json&maxRows=100000";
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data) => {
                return data;
            })
        );
        return request;
    }
    public listReferencias(idSolicitud): Observable<any> {
        let sessionid = this.auth.getToken();
        let query = encodeURI("select name, Lote_Cliente, Cantidad, Unidad_de_Medida,Nom_Embalaje from Referencia  where R11159413=" + idSolicitud);
        let url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionid + "&output=json&maxRows=100000";;
        let base = this.http.get(url);
        const request = base.pipe(
            map((data) => {
                return data;
            })
        );
        return request;
    }

    public updateAproved(id, fecha): Observable<any> {
        const url = `${getUrlEnvironment()}rest/api/updateRecord?output=json&useIds=true&objName=Solicitud_Servicio&sessionId=` + this.auth.getToken() + `&id=` + id + `&Cantidad_Aprobada=true&Estado_Web=Aprobada&Fecha_Aprobacion_Cantidad=` + fecha;
        return this.http.post(url, null)
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });
    }
    public updateRechazado(id, motivo): Observable<any> {
        const url = `${getUrlEnvironment()}rest/api/updateRecord?output=json&useIds=true&objName=Solicitud_Servicio&sessionId=` + this.auth.getToken() + `&id=` + id + `&Cantidad_Aprobada=false&Estado_Web=Rechazada&Motivo_Rechazo=` + motivo;
        return this.http.post(url, null)
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });     
    }

    encodeQueryData(data) {
        try {
            const ret = [];
            for (const d in data)
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            return ret.join('&');
        } catch (s) {
            alert('s ' + s);
        }
    }

    buildUrlParams(object) {
        try {
            const urlParams = [];
            for (const key in object) {
                if (object.hasOwnProperty(key)) {
                    urlParams.push(key + '=' + object[key]);
                }
            }
            return urlParams.join('&');
        } catch (r) {
            alert(r);
        }
    }

    /*  public loadPost(content) {
          const xhr = new XMLHttpRequest();
          const url = content.url;
          const params = this.buildUrlParams(content);
          xhr.open('POST', url, true);
          xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
          xhr.responseType = 'json';
          let resultadoPro = null;
         let buena = function () {
              if (xhr.readyState === 4 && xhr.status === 200) {
                  let responseObj = xhr.response;
                  console.log(responseObj);
                  let s = JSON.stringify(responseObj);
                  s = JSON.parse(s);
                  alert(s);
                  const result = new Array<Generic>();
                  for (let i = 0; i < s.length; i++) {
                      let as;
                      let oth = (s[i][2]) ? s[i][2] : '';
                      as = { 'id': s[i][0], 'label': s[i][1], 'data': oth };
                      console.log("as" + as);
                      result.push(as);
                  }
                  alert("RESULT: " + result);
                  resultadoPro = result;
                  
                 // funcionQueRecibeResult(result);
              }
          };
          alert("EL PRO: "+resultadoPro);
         // let resultado = null;
          // xhr.onreadystatechange(function (_result: Observable<any>) {
          //     resultado = _result;
          //     alert(resultado);
          // });
  
          /*  for (let i = 0; i < responseObj.length; i++) {
                //       this.generics.cc(responseObj[0]);
                console.log(responseObj[i][0]);
                let result = responseObj[i][0];
                console.log("result: " + result);
   
            }
            return responseObj;*/

    /* xhr.onload = function () {
         alert(`Loaded: ${xhr.status} ${xhr.response}`);
     };
     const eqd = this.encodeQueryData(content);

     xhr.onprogress = function (e) {
         try {
             if (e.lengthComputable) {
                 const percentComplete = (e.loaded / e.total) * 100;
             }
         } catch (f) {
             alert('f ' + f);
         }
     };

     xhr.onload = function (a) {
         console.log('onload');
     };
     xhr.onerror = function () {
         console.log('error');
     };
     xhr.onabort = function () {
         console.log('abort');
     };
     xhr.ontimeout = function () {
         console.log('timeout');
     };
     xhr.onloadstart = function () {

     };
     xhr.onloadend = function () {

     };
     xhr.send(eqd);

}*/

    public loadSecure(content) {
        const tok = this.auth.getToken();
        const xhr = new XMLHttpRequest();
        const url = `${getUrlEnvironment()}rest/api/setBinaryData?output=json&sessionId=` + tok;
        const params = this.buildUrlParams(content);
        xhr.open('POST', url, true);

        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4 && xhr.status === 200) {

            }
        };
        const eqd = this.encodeQueryData(content);

        xhr.onprogress = function (e) {
            try {
                if (e.lengthComputable) {
                    const percentComplete = (e.loaded / e.total) * 100;
                }
            } catch (f) {
                alert('f ' + f);
            }
        };

        xhr.onload = function (a) {
            console.log('onload');
        };
        xhr.onerror = function () {
            console.log('error');
        };
        xhr.onabort = function () {
            console.log('abort');
        };
        xhr.ontimeout = function () {
            console.log('timeout');
        };
        xhr.onloadstart = function () {

        };
        xhr.onloadend = function () {

        };
        xhr.send(eqd);
    }

    public updatePassword(correo, pass): Observable<any> {
        const url = this.getQueryPassword(correo, pass);
        return this.http.post(encodeURI(url), {})
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                console.log(details);
                return Observable.throw(details);
            });
    }


    private getQueryPassword(correo: any, pass: any) {
        const userId = JSON.parse(sessionStorage.getItem('contactoId'));
        const token = this.auth.getToken();
        let params = `updateRecord?output=json&objName=Contactos&id=${userId.value}&Correo_Electronico=${correo}&Contrasea=${pass}&sessionId=${token}`;
        const url = `${getUrlEnvironment()}rest/api/${params}`;
        return url;
    }

    public deleteRequest(id: number): Observable<any> {
        const token = this.auth.getToken();
        let query = `setDataField?output=json&useIds=false&fieldName=status&value=Cancelada&sessionId=${token}&id=${id}`;
        //const url = `${getUrlEnvironment()}rest/api/updateRecord?bjName=Solicitud_Servicio&output=json&useIds=false&ostatus=Cancelada&sessionId=`
            //+ token + `&id=` + id;
        const url = `${getUrlEnvironment()}rest/api/${query}`;
        return this.http.get(url, {})
            .map(data => {
                return data;
            }
            ).catch((err: Response) => {
                const details = err;
                return Observable.throw(details);
            });
    }

    private Sucursales() {
        const cliente = this.auth.getIdUserLoggedIn();
        let idSucursal = sessionStorage.getItem('idSucursales');
        //const userId = this.auth.getIdUserLoggedIn();
        const userId = JSON.parse(sessionStorage.getItem('contactoId'));
        let base;
        var query = 'select id,name,Principal from Sucursal1 where Cdigo_SAP is not null and Deudor is not null and R8173502=' + userId + ' and id in(' + idSucursal + ') order by name';
        //base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=100000&output=json&query=` + query + `&sessionId=` + this.auth.getToken());
        
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Contactos&id=` + userId.value + '&relName=R12388444&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
        //base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + cliente + '&relName=R8173502&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    public list(unidades, sucursales): Observable<any> {
        return this.listServices(unidades, sucursales);
    }

    public getUni(): Observable<any> {
        return this.generics('unidad', '');
    }

    public getSolDesc(): Observable<any> {
        return this.generics('SolDescargada', '');
    }

    public grouper(id: string): Observable<any> {
        return this.generics('groupers', id);
    }

    public groupersNormal(id: string, idb: string): Observable<any> {
        return this.generics('groupersNormal', id, idb);
    }

    public centerGroup(id: string): Observable<any> {
        return this.generics('centerGroup', id);
    }


    public materials(id: string, or: string): Observable<any> {
        return this.generics('materials', id, or);
    }


    public materialesNormal(id: string, or: string): Observable<any> {
        return this.generics('materialesNormal', id, or);
    }

    public package(id: string): Observable<any> {
        return this.generics('packages', id);
    }

    public sucursal(): Observable<any> {
        return this.generics('sucursal', '');
    }

    public saveReq(service: any, Formlen: number, branch: string, orderId, datoSociedad): Observable<any> {
        return this.createRequest(service, Formlen, branch, orderId, datoSociedad);
    }

    public groupuser(id): Observable<any> {
        return this.generics('groupuser', id);
    }

    public groupuserCompra(id): Observable<any> {
        return this.generics('groupuserCompra', id);
    }

    public materialuser(id): Observable<any> {
        return this.generics('materialuser', id);
    }

    public materialusercompra(id): Observable<any> {
        return this.generics('materialusercompra', id);
    }

    public getSucursales() {
        return this.Sucursales();
    }
}
