import { Component, OnInit, ViewChild, trigger, state, animate, transition, style } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService, Services, Generic, Result, Status } from '../services.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { FormGroup, FormArray, FormControl, FormBuilder, Validators } from '@angular/forms';
import { GenericBrowserDomAdapter } from '@angular/platform-browser/src/browser/generic_browser_adapter';


@Component({
    selector: 'app-datatables-products',
    templateUrl: './detail.component.html',
    styleUrls: ['./detail.component.css'],
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],
})

export class DetailComponent implements OnInit {
    public invoiceForm: FormGroup;
    public statusView: boolean;
    public statusSuccess: boolean;
    public rechazarDiv: boolean;
    public groupVis = 'hidden';
    public visibility = 'hidden';
    public materialVis = 'hidden';
    public tipoEmba = 'hidden';
    public cantEmba = 'hidden';
    public branch;
    public numSol = 0;
    public orderId;
    public stop = 'hidden';
    public sucursalC = 'shown';
    public valueCheck = 0;
    public materiales = [];
    public tipo = '';
    public arrMate = [];
    public datoSociedad ='';

    status: Status = {
        status: '',
        id: '',
        motive: ''
    };

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    public data: Generic;
    public material: Generic;
    public package: any;
    public sucursal: any;
    public groupuser: any;
    public result: Result;
    public EnterpriseData;
    public gSelect;
    PagoAnticipo = 'shown';
    Servicio_Posfechado;
    Presenciales;
    Recoleccion;
    Recepcion;
    sucursalSelect;
    doc;
    Pago_Anticipo_ena = false;
    servicioPostF = false;
    recoSta = false;
    receSta = true;

    constructor(
        private router: Router,
        private docServ: ServicesService,
        private toastyService: ToastyService,
        private _fb: FormBuilder
    ) { }
    /**
     * Carga el listado de la opción elegida
     * @memberof DetailComponent
     */
    ngOnInit() {
        
        let as = JSON.parse(sessionStorage.getItem("EntrepriseDataJSON"));
        //console.log('asdasd' + as[12]['dato']);
        this.gSelect = {'id': 0, 'label': ''};

        this.EnterpriseData = (sessionStorage.getItem("EntrepriseData")).split(",");
        this.PagoAnticipo = (as[13]['dato'] == 1) ? 'shown' : 'hidden';
        this.Servicio_Posfechado = (as[14]['dato'] == 1) ? 'show' : 'hidden';
        this.Presenciales = (as[15]['dato'] == 1) ? 'shown' : 'hidden';
        this.Recoleccion = (as[16]['dato'] == 1) ? 'shown' : 'hidden';
        this.Recepcion = (as[17]['dato'] == 1) ? 'shown' : 'hidden';

        this.invoiceForm = this._fb.group({
            itemRows: this._fb.array([this.initItemRows()])
        });

        // Obtiene las sucursales del cliente actual
        this.docServ.getSucursales().subscribe(sucursal => {
            this.sucursal = sucursal;
            //console.log(this.sucursal);
        }, (err) => {
            console.error(err);
        });

        this.orderId = 0;
    }

    /**
     * Inicia el formulario con el modelo de solicitud
     */
    initItemRows() {
        this.stop = 'hidden';
        return this._fb.group({
            qty: [0, [<any>Validators.required, <any>Validators.min(1)]],
            grouper: ['', [<any>Validators.required]],
            materials: ['', [<any>Validators.required]],
            pk: [0, [<any>Validators.min(0)]],
            qtyPack: [0, [<any>Validators.required, <any>Validators.min(1)]],
            accept: [this.valueCheck, [<any>Validators.requiredTrue]],
            Pago_Anticipo: [''],
            Servicio_Posfechado: [''],
            Presenciales: [''],
            Recoleccion: [false],
            Recepcion: [false],
            Pago_Anticipo_file: [''],
            Servicio_Posfechado_fecha: ['']
        });
    }

    /**
     * Inicia el formulario con el modelo de solicitud
     * cuando cambia el agrupador
     */
    initItemRowsFromGrouper() {
        this.invoiceForm.controls.itemRows.patchValue([{qty: 0}]);
        this.invoiceForm.controls.itemRows.patchValue([{pk: 0}]);
        this.invoiceForm.controls.itemRows.patchValue([{materials: ''}]);
        this.invoiceForm.controls.itemRows.patchValue([{qtyPack: 0}]);
        this.invoiceForm.controls.itemRows.patchValue([{accept: false}]);
        this.invoiceForm.controls.itemRows.patchValue([{Pago_Anticipo: ''}]);
        this.invoiceForm.controls.itemRows.patchValue([{Servicio_Posfechado: ''}]);
        this.invoiceForm.controls.itemRows.patchValue([{Presenciales: ''}]);
        this.invoiceForm.controls.itemRows.patchValue([{Recoleccion: false}]);
        this.invoiceForm.controls.itemRows.patchValue([{Recepcion: false}]);
        this.invoiceForm.controls.itemRows.patchValue([{Pago_Anticipo_file: ''}]);
        this.invoiceForm.controls.itemRows.patchValue([{Servicio_Posfechado_fecha: ''}]);
        this.package = [{'id': 0, 'label': '', 'data': ''}];
        this.stop = 'hidden';
    }

    /**
     * Crea una solicitud nueva
     */
    addNewRow(i) {
        if (this.createReq()) {
            this.stop = 'hidden';
            this.sucursalC = 'hidden';
            this.valueCheck = 0;
        };

        return;
    }

    /**
     * Guardar y salir
     */
    saveGetOut() {
        if (this.createReq())
            this.getOut();
        return;
    }

    /**
     *
     * @param index borra un registro
     */
    deleteRow(index: number) {
        const control = <FormArray>this.invoiceForm.controls['itemRows'];
        control.removeAt(index);
    }

    /**
     * Al seleccionar la sucursal debe consultar los agrupadores
     * @param event id de la sucursal
     */
    public onChangeSucursal(event) {
        this.invoiceForm = this._fb.group({
            itemRows: this._fb. array([this.initItemRows()])
        });
        this.branch = event.target.value;
        let brancho = event.target.value;
        this.sucursalSelect = this.sucursal.filter(tipo => tipo.id == brancho);
        this.getGroupsByUser(this.sucursalSelect);
        let idLineaN = sessionStorage.getItem('LineaNegocio');
        this.docServ.getDatoSociedad(idLineaN,this.branch).subscribe(datos => {
            this.datoSociedad = datos;
           // alert("hay que enviar: "+this.datoSociedad);
        });
    }

    /**
     * Consulta los agrupadores por sucursal
     * @param brancho id de la sucursal
     */
    getGroupsByUser(brancho) {
        this.groupuser = this.docServ.groupuser(brancho).subscribe(groupuser => {
            this.groupuser = this.docServ.groupuserCompra(brancho).subscribe(groupuserC => {

                let or = [];
                for (let i = 0; i < groupuser.length; i++) {
                    or.push(groupuser[i]['id']);
                }

                for (let i = 0; i < groupuserC.length; i++) {
                    or.push(groupuserC[i]['id']);
                }
                if (or.length >= 0) {
                    this.loadGroup(or.toString());
                }
            });
        }, (err) => {
            console.error(err);
        });
    }

    /**
     * Consulta la info de los agrupadores
     * @param groupsids ids de los agrupadores
     */
    loadGroup(groupsids) {

        if(groupsids === ""){
            let arr = [{
                id : "",
                label : "",
                data : ""
            }];
            this.docServ.groupersNormal("1", "1").subscribe(groupersre => {
                this.data = groupersre;
            }, (err) => {
                console.error(err);
            });
        }else{
            this.docServ.centerGroup(groupsids).subscribe(groupers => {
                let idb = groupsids;
                let ids = [];
                for (let i = 0; i < groupers.length; i++) {
                    ids.push(groupers[i].id);
    
                }
    
                this.visibility = 'shown';
                if (ids.length > 0) {
                    this.docServ.groupersNormal(ids.toString(), idb).subscribe(groupersre => {
                        this.data = groupersre;
                    }, (err) => {
                        console.error(err);
                    });
                }
            }, (err) => {
                console.error(err);
            });
        }   
    }

    /**
     * Al cambiar el agrupador, consulta los materiales por agrupador
     * @param $event agrupador
     */
    onChangeGroup($event) {
        this.docServ.materialuser(this.sucursalSelect).subscribe(materialuserres => {
            this.docServ.materialusercompra(this.sucursalSelect).subscribe(materialcompra => {
                let or = [];

                this.createOrMat(materialuserres, or);
                this.createOrMat(materialcompra, or);
                this.addMaterial(materialuserres, this.arrMate);
                this.addMaterial(materialcompra, this.arrMate);
                this.loadMaterial($event, or);
                this.initItemRowsFromGrouper();
            });
        }, (err) => {
            console.error(err);
        });
    }

    /**
         * Optiene los ids para realizar el in de materiales
         * @param material materiales
         * @param arreglo listado
         */
    private addMaterial(material: any, arreglo) {
        for (let i = 0; i < material.length; i++) {
            let mat = {
                id: material[i]['id'],
                um: material[i]['label']
            }
            arreglo.push(mat);
            //console.log(mat);
        }
        return arreglo;

    }
    /**
     * Optiene los ids para realizar el in de materiales
     * @param materialuserres materiales
     * @param or listado
     */
    private createOrMat(materialuserres: any, or) {
        for (let i = 0; i < materialuserres.length; i++) {
            const res = this.materiales.includes(materialuserres[i]['id'].toString());
            if (res !== true) {
                or.push(materialuserres[i]['id']);
            }

        }
        return or;
    }

    /**
     * Materiales
     * @param id materiales
     * @param or listado
     */
    loadMaterial(id, or) {
        this.groupVis = 'shown';
        this.docServ.materialesNormal(id, or).subscribe(materials => {
            this.material = materials;
        }, (err) => {
            console.error(err);
        });
    }

    /**
     * Embalajes
     * @param $event materiales
     * @param $eventAlert Sustancia controlada
     */
    onChangeMaterial($event, $eventAlert) {
        console.log('cuando cambia el material');
        console.log($event.target.value);
        let controlada = ($eventAlert.split(','))[1];
        let idMat = ($eventAlert.split(','))[0];
        this.materialVis = 'shown';
        this.tipoEmba = 'shown';
        this.docServ.package($event).subscribe(packages => {
            this.package = packages;
        }, (err) => {
            console.error(err);
        });      
        for(var i=0; i<this.arrMate.length; i++){
            if(this.arrMate[i]['id'] == idMat){
                document.getElementById("um").innerHTML = this.arrMate[i]['um'];
             }
        };
        if (controlada == 1) {
            this.addToast({
                title: 'Sustancia controlada',
                msg: 'El material que eligió es una sustancia controlada',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'warning'
            });
        }
    }
    
    onChangePackage($event) {
        console.log('cuando cambia el paquete');
        console.log($event);
        this.cantEmba = 'shown';
    }
    onChangeCantEmbla() {
        if (this.valueCheck === 0) {
            this.stop = 'shown';
        }
    }

    getOut() {
        setTimeout(() => {
            this.router.navigate(['/servicios/solicitudes']);
        }, 3000);
    }

    pagoAction() {
        this.Pago_Anticipo_ena = !this.Pago_Anticipo_ena;;
    }

    servicioPostFAction() {
        this.servicioPostF = !this.servicioPostF;
    }

    recolAction() {

    }

    receptAction() {

    }

    /**
     * Realizart la accion de rechazo
     * @param {NgForm} myForm Formulario con motivo de rechazo
     * @memberof DetailComponent
     */
    createReq() {
        const formlen = this.invoiceForm.value.itemRows.length;
        const control = <FormArray>this.invoiceForm.controls['itemRows'];
      
        if (this.tipo == '') {

            if (this.invoiceForm.value.itemRows[0].Recepcion && this.invoiceForm.value.itemRows[0].Recoleccion) {
                alert('No puede seleccionar los parámetros de Recolección y el de Recepción a la vez, solo puede seleccionar uno de ellos');
                return false;
            }

            if (!this.invoiceForm.value.itemRows[0].Recepcion && !this.invoiceForm.value.itemRows[0].Recoleccion) {
                alert('Debe seleccionar un tipo de solicitud, esta debe ser de recepción o de recolección');
                return false;
            }
        }
      
        try {
            this.docServ.saveReq(this.invoiceForm.value, formlen, this.branch, this.orderId, this.datoSociedad).subscribe(Services => {
                this.result = Services;
                console.log(Services);
                this.tipo = 'recep recol';
                if (Services.status === 'ok') {
                    if (this.invoiceForm.value.itemRows[0].Pago_Anticipo_file) {
                        this.doc.id = this.result.id;
                        this.docServ.loadSecure(this.doc);
                    }
                    console.log('el nuevo material' + this.invoiceForm.value.itemRows[0].materials);
                    this.materiales.push(this.invoiceForm.value.itemRows[0].materials);
                    console.log(JSON.stringify(this.materiales));
                    this.orderId = this.result.id;
                    this.addToast({
                        title: 'Acción exitosa',
                        msg: 'La solicitud se creó exitosamente',
                        timeout: 5000,
                        theme: 'default',
                        position: 'bottom-right',
                        type: 'success'
                    });
                    this.statusView = false;
                    this.statusSuccess = false;
                    this.numSol++;
                    control.removeAt(0);
                    control.push(this.initItemRows());
                    return true;
                } else {
                    this.addToast(
                        {
                            title: 'Error en el proceso',
                            msg: 'No fue posible crear la solicitud',
                            timeout: 5000,
                            theme: 'default',
                            position: 'bottom-right',
                            type: 'error'
                        });
                    return false;
                }
            }, (err) => {
                console.error(err);
            });
        } catch (e) {
            this.materiales.push(this.invoiceForm.value.itemRows[0].materials);
            this.addToast({
                title: 'Acción exitosa',
                msg: 'La solicitud se creó exitosamente',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'success'
            });

            this.numSol++;
            control.removeAt(0);
            control.push(this.initItemRows());
            return true;
        }

        return true;
    }


    onFileChange(event) {
        let reader = new FileReader();
        if (event.target.files && event.target.files.length > 0) {
            let file = event.target.files[0];
            reader.readAsDataURL(file);
            reader.onload = () => {
                this.doc = {
                    id: '',
                    fieldName: 'Soporte_de_Pago',
                    objName: 'Orden_de_Servicio',
                    fileName: file.name,
                    contentType: file.type,
                    value: reader.result.split(',')[1]
                }
            };
        }
    }

    public sortByWordLength = (a: any) => {
        return a.id.length;
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
