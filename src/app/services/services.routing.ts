import { LinesComponent } from './lines/lines.component';
import { Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { ListApproval } from './list-approval/list-approval.component';

export const ServicesRoutes: Routes = [
  /*{
    path: '',
    redirectTo: 'facturas',
    pathMatch: 'full'
  },*/
  {
    path: 'solicitudes',
    component: ListComponent,
    data: {
      breadcrumb: 'Solicitudes',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'crear-solicitud',
    component: DetailComponent,
    data: {
      breadcrumb: 'Crear solicitud',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'aprobacion-pesos',
    component: ListApproval,
    data: {
      breadcrumb: 'Aprobación de Pesos',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'lineas-negocio',
    component: LinesComponent,
    data: {
      breadcrumb: 'Líneas de negocios',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'detalle/:id',
    component: DetailComponent,
    data: {
      breadcrumb: 'Detalle',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
