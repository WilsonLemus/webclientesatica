import { LinesComponent } from './lines/lines.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ServicesRoutes } from './Services.routing';
import { HttpModule } from '@angular/http';
import { ListComponent } from './list/list.component';
import { DetailComponent } from './detail/detail.component';
import { SharedModule } from '../shared/shared.module';
import { ServicesService } from './services.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { AuthenticationService } from '../authentication/authentication.service';
import {DataTableModule} from 'angular2-datatable';
import { DataFilterPipe } from './data-filter.pipe';
import { AuthGuardService } from '../authentication/auth-guard.service';
import {AngularEchartsModule} from 'ngx-echarts';
import { ListApproval } from './list-approval/list-approval.component';

@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(ServicesRoutes),
    SharedModule,
    HttpClientModule,
    AuthenticationModule,
    DataTableModule,
    AngularEchartsModule
  ],
  providers: [
    ServicesService,
    AuthenticationService,
    AuthGuardService
  ],
  declarations: [ListComponent, DetailComponent, LinesComponent, DataFilterPipe, ListApproval]
})

export class ServicesModule {}

