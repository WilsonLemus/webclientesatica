import * as _ from 'lodash';
import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
    name: 'dataFilter'
})
/*export class DataFilterPipe implements PipeTransform {
    /**
     * Ejecuta los filtros en el grid
     * @param array
     * @param query
     * @param field
     */
   /* transform(array: any[], query: string, field: string): any {
        if (query) {
            return _.filter(array, row=>row.OS.indexOf(query) > -1);
        }
       	return array;
    }
}*/
export class DataFilterPipe implements PipeTransform {

    transform(array: any[], query: string, field: string): any {
        let ret = array;
        //query = query.toUpperCase();  => Pasa a Mayuscula.
        if (query) {
            ret = _.filter(array, row => row[field].indexOf(query) > -1);
        }
        return ret;
    }
}