import { Component, OnInit } from '@angular/core';
import { ServicesService, References } from '../services.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { trigger, state, animate, transition, style } from '@angular/core';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './list-approval.component.html',
    styleUrls: ['./list.component.css'],
    animations: [
        trigger('visibility', [
            state('shown', style({
                opacity: 1
            })),
            state('hidden', style({
                opacity: 0
            })),
            transition('* => *', animate('.5s'))
        ])
    ],
})
export class ListApproval implements OnInit {
    toastyService: any;
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';

    public referencias: References;
    public data;

    public reason: FormGroup;
    public aprobacion: FormGroup;
    public id;
    public n;
    public y;
    public m;
    public d;
    public fecha;

    constructor(private docServ: ServicesService, private modalService: NgbModal, private fb: FormBuilder) { }
    ngOnInit() {
        this.ListRequest();
        this.reason = this.newElementR();
        this.aprobacion = this.newElementA();
        this.fechas();
    }
    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
    }
    public ListRequest() {
        this.docServ.listSolicitudesDescargadas().subscribe(datos => {
            this.data = datos;
        });


    }
    fechas() {
        this.n = new Date();
        //Año
        this.y = this.n.getFullYear();
        //Mes
        this.m = this.n.getMonth() + 1;
        //Día
        this.d = this.n.getDate();

        //Formato fecha
        this.fecha = this.d + "/" + this.m + "/" + this.y;
    }
    open(content, idSolicitud) {
        this.modalService.open(content, { size: 'lg' });
        this.ListReferences(idSolicitud);
        
    }

    confirm(content, id) {
        this.modalService.open(content);
        this.id = id;
    }

    public ListReferences(idSolicitud) {
        this.docServ.listReferencias(idSolicitud).subscribe(references => {
            this.referencias = references;
        })
    }
    /*Prepara el formulario de rechazo*/
    newElementR() {
        return this.fb.group({

            motivo: [null, Validators.compose([Validators.required])],
        });
    }
    newElementA() {
        return this.fb.group({
            id: [''],
        });
    }

    saveAproved(id) {
        this.docServ.updateAproved(id, this.fecha).subscribe(Services => {
            if (Services.status === 'ok') {
                alert("muestra aler");
                this.addToast({
                    title: 'Acción exitosa',
                    msg: 'Los pesos se aprobaron exitosamente',
                    timeout: 5000,
                    theme: 'default',
                    position: 'top-right',
                    type: 'success'
                });
            }
        })

        location.reload();
    }
    saveRechazo(rechazo: any, id) {
        this.docServ.updateRechazado(id, rechazo.motivo).subscribe(services => {
            if (services.status === 'ok') {
                this.addToast({
                    title: 'Acción exitosa',
                    msg: 'Se rechazo correctamente',
                    timeout: 5000,
                    theme: 'default',
                    position: 'top-right',
                    type: 'success'
                });
            }
        })
        location.reload();
    }
}