import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { QuillEditorModule } from 'ngx-quill-editor';
import { HttpModule } from '@angular/http';
import { DataTableModule } from 'angular2-datatable';
import { AngularEchartsModule } from 'ngx-echarts';
import { ListComponent } from './list/list.component';
import { DocumentsService } from './documents.service';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ServicesService } from '../services/services.service';
import { DataFilterPipe } from './data-filter.pipe';
import { ListLicenses } from './licenses/licenses.component';
export const documentsRoutes: Routes = [
  {
    path: 'documentos',
    component: ListComponent,
    data: {
      breadcrumb: 'Documentos',
      status: true
    },
    canActivate: [AuthGuardService]
  },
  {
    path: 'licencias',
    component: ListLicenses,
    data: {
      breadcrumb: 'Licencias',
      status: true
    },
    canActivate: [AuthGuardService]
  },
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(documentsRoutes),
    SharedModule,
    FormsModule,
    QuillEditorModule,
    HttpModule,
    DataTableModule,
    AngularEchartsModule,
    AngularMultiSelectModule
  ],
  providers: [
    DocumentsService,
    AuthGuardService,
    ServicesService
  ],
  declarations: [ListComponent, ListLicenses, DataFilterPipe]
})
export class DocumentsModule { }