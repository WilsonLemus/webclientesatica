import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

@Injectable()
export class DocumentsService {
    private datos;

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    public getDocuments(sucursales, tipo: string): Observable<any> {
        console.log(tipo);
        let query = "";
        let base;
        if (tipo == "planes") {
            query = `select id, Descripcion, R10819356 from  PlanesDeContingencia WHERE R10819356 = ${this.auth.getIdUserLoggedIn()} ORDER BY id`;
            base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=${query}&sessionId=` + this.auth.getToken() + '&output=json&maxRows=2000');
            this.datos = base.pipe(
                map((data: any) => {
                    // console.log(data);
                    let tot = [];
                    for (let i = 0; i < data.length; i++) {
                        let suc = data[i][1];
                        let sucName = '';
                        const res = sucursales.filter(tipo => tipo.id == suc);
                        if (res.length > 0) {
                            sucName = res[0].label;
                        }
                        tot.push({
                            id: data[i][0],
                            Descripcion: data[i][1],
                            cliente: data[i][2],
                            Document: data[i][3]
                        });
                    }
    
                    return tot;
                })
            );
        } else {
            base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + this.auth.getIdUserLoggedIn() + '&relName=R12128607&fieldList=id,name&sessionId=' + this.auth.getToken() + '&output=json');
            //query = `select id, name, Documentos from Proveedor_Tratamiento WHERE id = 11717500 ORDER BY id`;
            this.datos = base.pipe(
                map((data: any) => {
                    // console.log(data);
                    let tot = [];
                    for (let i = 0; i < data.length; i++) {
                        let suc = data[i][1];
                        let sucName = '';
                        const res = sucursales.filter(tipo => tipo.id == suc);
                        if (res.length > 0) {
                            sucName = res[0].label;
                        }
                        tot.push({
                            id: data[i]["id"],
                            Descripcion: data[i]["name"],
                            cliente: data[i][2],
                            Document: data[i]["name"]
                        });
                    }
    
                    return tot;
                })
            );
        }
        //  console.log(query);
        //base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=${query}&sessionId=` + this.auth.getToken() + '&output=json&maxRows=2000');
        /*this.datos = base.pipe(
            map((data: any) => {
                // console.log(data);
                let tot = [];
                for (let i = 0; i < data.length; i++) {
                    let suc = data[i][1];
                    let sucName = '';
                    const res = sucursales.filter(tipo => tipo.id == suc);
                    if (res.length > 0) {
                        sucName = res[0].label;
                    }
                    tot.push({
                        id: data[i][0],
                        Descripcion: data[i][1],
                        cliente: data[i][2],
                        Document: data[i][3]
                    });
                }

                return tot;
            })
        );*/
        return this.datos;
    }

    formatDate(fecha) {
        if (fecha) {
            let from: Date = new Date(fecha);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 10) ? '0' + from.getDate() : from.getDate();
            return from.getFullYear() + '/' + mon + '/' + day;
        }

        return '';
    }
}