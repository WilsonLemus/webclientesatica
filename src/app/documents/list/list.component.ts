import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../../services/services.service';
import { DocumentsService } from '../documents.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public filterQuery = '';
  public filterQueryMaterial = '';
  public filterQuerySolicitud = '';
  public filterQueryFecha = '';
  public rowsOnPage = 5;
  public sortBy = 'id';
  public sortOrder = 'asc';
  public data;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private docServ: DocumentsService,
    private auth: AuthenticationService,
    private servServ: ServicesService
  ) { }

  /**
   * Obtiene las sucursales y los certificados por sucursal
   */
  ngOnInit() {
    this.servServ.sucursal().subscribe(sucursal => {
      this.docServ.getDocuments(sucursal, "planes").subscribe(Certificates => {
        this.data = Certificates;
      }, (err) => {
        console.error(err);
      });
    });

  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * Abre el pdf
   * @param id
   */
  goToUrl(id) {
   // console.log(id);
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=PlanesDeContingencia&id=` + id + "&fieldName=Documento&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }
  public doc;

}
