import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService } from '../../services/services.service';
import { DocumentsService } from '../documents.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
    selector: 'app-datatables-products',
    templateUrl: './licenses.component.html',
    styleUrls: ['./licenses.component.css']
})
export class ListLicenses implements OnInit {
    public data;
    public filterQuery = '';
    public rowsOnPage = 5;
    public sortBy = 'id';
    public sortOrder = 'asc';

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private docServ: DocumentsService,
        private auth: AuthenticationService,
        private servServ: ServicesService
    ) { }

    ngOnInit() {
        this.servServ.sucursal().subscribe(sucursal => {
            this.docServ.getDocuments(sucursal, "licencias").subscribe(Certificates => {
                this.data = Certificates;
            }, (err) => {
                console.error(err);
            });
        });

    }

    /**
   * Abre el pdf
   * @param id
   */
    goToUrl(id) {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Proveedor_Tratamiento&id=` + id + "&fieldName=Documentos&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
    }
    public doc;
}
