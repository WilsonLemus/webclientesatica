import { Component, OnInit } from '@angular/core';
import { ServicesService } from '../../services/services.service';
import { CertificatesService } from '../Certificates.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';


@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  public filterQuery = '';
  public filterQueryMaterial = '';
  public filterQuerySolicitud = '';
  public filterQueryFecha = '';
  public rowsOnPage = 5;
  public sortBy = 'fecha';
  public sortOrder = 'desc';
  public data;
  private datos;

  constructor(
    private docServ: CertificatesService,
    private auth: AuthenticationService,
    private servServ: ServicesService
  ) { }

  /**
   * Obtiene las sucursales y los certificados por sucursal
   */
  ngOnInit() {
    this.servServ.sucursal().subscribe(sucursal => {
      this.docServ.getCertificates(sucursal).subscribe(Certificates => {
        this.data = Certificates;
      }, (err) => {
        console.error(err);
      });
    });

  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * Abre el pdf
   * @param id
   */
  goToUrl(id) {
    
    this.docServ.getCert(id).subscribe(cert => {
      this.datos = cert;
      if (this.datos != "") {
        let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Certificado&id=` + id + "&fieldName=Documento_de_Tratamiento&sessionId=" + this.auth.getToken();
        window.open(url, '_blank');
      }
    })

  }

  goToUrl2(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Solicitud_Servicio&id=` + id + "&fieldName=Certificado_del_Dispositor&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }
  goToUrl3(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Certificado&id=` + id + "&fieldName=Documento_de_Recoleccion&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }

  public doc;

}
