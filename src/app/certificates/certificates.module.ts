import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {FormsModule} from '@angular/forms';
import {QuillEditorModule} from 'ngx-quill-editor';
import {HttpModule} from '@angular/http';
import {DataTableModule} from 'angular2-datatable';
import {AngularEchartsModule} from 'ngx-echarts';
import { ListComponent } from './list/list.component';
import { CertificatesService } from './Certificates.service';
import { AuthGuardService } from '../authentication/auth-guard.service';
import { AngularMultiSelectModule } from 'angular2-multiselect-dropdown/angular2-multiselect-dropdown';
import { ServicesService } from '../services/services.service';
import { DataFilterPipe } from './data-filter.pipe';
import { AdminLayoutComponent } from '../layouts/admin/admin-layout.component';
import { ProfileService } from '../user/profile/profile.service';
import { LineaNegocio } from '../layouts/admin-layout.service';
export const materialRoutes: Routes = [
  {
    path: 'certificados',
    component: ListComponent,
    data: {
      breadcrumb: 'Certificados',
      status: true
    },
    canActivate: [ AuthGuardService ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(materialRoutes),
    SharedModule,
    FormsModule,
    QuillEditorModule,
    HttpModule,
    DataTableModule,
    AngularEchartsModule,
    AngularMultiSelectModule
  ],
  providers: [
    CertificatesService,
    AuthGuardService,
    ServicesService,
    AdminLayoutComponent, 
    ProfileService,
    LineaNegocio
  ],
  declarations: [ListComponent, DataFilterPipe]
})
export class CertificatesModule { }
