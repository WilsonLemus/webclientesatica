import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { AuthenticationService } from '../authentication/authentication.service';
import { AdminLayoutComponent } from '../layouts/admin/admin-layout.component';
import { getUrlEnvironment } from '../shared/environment';

@Injectable()
export class CertificatesService {
    private datos;

    constructor(private http: HttpClient, private auth: AuthenticationService, private AdminLayoutComponent: AdminLayoutComponent) { }

    public getCertificates(sucursales): Observable<any> {
        let base;
        let idSucursal = sessionStorage.getItem('idSucursales');
        let idLinea = sessionStorage.getItem('LineaNegocio');
        let query = `SELECT id, Sucursal, RCertificados, Certificado_del_Dispositor, No_Os,Consecutivo_SS, Nom_Material, Fecha_Prestar_Servicio, Certificado_Intermediarios FROM Solicitud_Servicio WHERE Estado_Certificado = 9231294 AND R8634058 = ${this.auth.getIdUserLoggedIn()} and Sucursal in(${idSucursal}) and Lnea_de_Negocio=${idLinea} ORDER BY Fecha_Prestar_Servicio DESC`;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=${query}&sessionId=` + this.auth.getToken() + '&output=json&maxRows=3000');
        this.datos = base.pipe(
            map((data: any) => {
                let tot = [];
                for (let i = 0; i < data.length; i++) {
                    let suc = data[i][1];
                    let sucName = '';
                    const res = sucursales.filter(tipo => tipo.id == suc);
                    if (res.length > 0) {
                        sucName = res[0].label;
                    }
                    tot.push({
                        id: data[i][0],
                        Sucursal: sucName,
                        RCertificados: data[i][2],
                        Certificado_del_Dispositor: data[i][3],
                        No_Os: data[i][4],
                        Consecutivo_SS: data[i][5],
                        material: data[i][6],
                        fecha: this.formatDate(data[i][7]),
                        CertificadoIntermediarios: data[i][8],
                        otro: this.getCert(data[i][2])
                    });
                }
                return tot;

            })
        );
        return this.datos;
    }
    public getCert(id): Observable<any> {
        let base;
        let query = `SELECT Documento_de_Tratamiento from Certificado WHERE id=${id}`;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=${query}&sessionId=` + this.auth.getToken() + '&output=json&maxRows=3000');
        this.datos = base.pipe(
            map((data: any) => {
                return data;
            })
        );
        return this.datos;
    }

    formatDate(fecha) {
        if (fecha) {
            let from: Date = new Date(fecha);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 10) ? '0' + from.getDate() : from.getDate();
            return from.getFullYear() + '/' + mon + '/' + day;
        }

        return '';
    }
}
