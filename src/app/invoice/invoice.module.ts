import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { InvoiceRoutes } from './invoice.routing';
import { HttpModule } from '@angular/http';
import { ListComponent } from './list/list.component';
import { SharedModule } from '../shared/shared.module';
import { InvoiceService } from './invoice.service';
import { HttpClientModule } from '@angular/common/http';
import { AuthenticationModule } from '../authentication/authentication.module';
import { AuthenticationService } from '../authentication/authentication.service';
import {DataTableModule} from 'angular2-datatable';
import { AuthGuardService } from '../authentication/auth-guard.service';
import {AngularEchartsModule} from 'ngx-echarts';
import { DataFilterPipe } from './data-filter.pipe';

@NgModule({
  imports: [
    HttpModule,
    CommonModule,
    RouterModule.forChild(InvoiceRoutes),
    SharedModule,
    HttpClientModule,
    AuthenticationModule,
    DataTableModule,
    AngularEchartsModule
  ],
  providers: [
    InvoiceService,
    AuthenticationService,
    AuthGuardService
  ],
  declarations: [ListComponent, DataFilterPipe]
})

export class InvoiceModule {}

