import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

export interface InvoicesDetail {
    id: String;
    invoiceId: String;
    fecha: String;
    estado: String;
    solicitudes: String;
}

export interface Services {
    Services: Array<InvoicesDetail>;
}

@Injectable()
export class InvoiceService {

    constructor(private http: HttpClient, private router: Router, private auth: AuthenticationService) { }

    private listServices(): Observable<any> {
        let base;
        var url = this.getQueryServices();
        base = this.http.get(url);
        const request = base.pipe(
            map((data: any) => {
                return this.proccessRequestListServices(data);
            })
        );
        return request;
    }

    private proccessRequestListServices(data: any) {
        for (let i = 0; i < data.length; i++) {
            let from: Date = new Date(data[i][3]);
            var startDate = from.getFullYear() + " - " + from.getMonth() + " - " + from.getDate() + " - " + from.getHours() + ": " + from.getMinutes() + ":" + from.getSeconds();
            const mon = (from.getMonth() < 9) ? '0' + (from.getMonth() + 1) : (from.getMonth() + 1);
            const day = (from.getDate() < 9) ? '0' + from.getDate() : from.getDate();
            data[i][3] = from.getFullYear() + '/' + mon + '/' + day;
        }
        return data;
    }

    private getQueryServices() {
        var query = `select id, Factura_SAP,status, createdAt from Factura1 where R9311861=${this.auth.getId()} and (status=8739141 or status=8715053) and Factura_SAP is not null ORDER BY id`;
        query = encodeURI(query);
        var url = `${getUrlEnvironment()}rest/api/selectQuery?startRow=0&maxRows=1000&output=json&query=${query}&sessionId=${this.auth.getToken()}`;
        return url;
    }

    private listSolicitudes(id): Observable<any> {
        let base;
        var url = `${getUrlEnvironment()}rest/api/getRelationships?&output=json&id=${id}&relName=R8713917&fieldList=name&sessionId=${this.auth.getToken()}`;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Services) => {
                return data;
            })
        );
        return request;
    }

    public list(): Observable<any> {
        return this.listServices();
    }

    public solicitudes(id): Observable<any> {
        return this.listSolicitudes(id);
    }

}