import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InvoiceService, Services } from '../invoice.service';
import { AuthenticationService } from '../../authentication/authentication.service';
import { getUrlEnvironment } from '../../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
    // http://plnkr.co/edit/PxBaZs?p=preview
    public data: Services;
    public filterQuery = '';
    public filterQueryDate = '';
    public filterQuerySol = '';
    public rowsOnPage = 20;
    public sortBy = 'invoiceId';
    public sortOrder = 'desc';

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private docServ: InvoiceService,
    private auth: AuthenticationService,
  ) {}

  /**
   * Lista las facturas del cliente
   */
  ngOnInit() {
    this.docServ.list().subscribe(invoices => {
      for(let i = 0; i<invoices.length; i++){
          this.docServ.solicitudes(invoices[i][0]).subscribe(services => {
            let sol = '';
            for (let k = 0; k < services.length; k++) {
              sol += (services.length == k+1) ? services[k].name: services[k].name + ' - ';

            }
            invoices[i].push(sol);
          }, (err) => {
              console.error(err);
          });
      }
      this.data = invoices;
    }, (err) => {
        console.error(err);
    });
  }

  detail(detail: Services) {
  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  /**
   * Abre el PDF
   * @param id
   */
  openPdf(id) {
    let url = `${getUrlEnvironment()}rest/api/getBinaryData?objName=Factura1&id=` + id + "&fieldName=Doc_Fact&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }
}
