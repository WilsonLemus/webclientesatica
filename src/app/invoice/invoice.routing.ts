import { Routes } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const InvoiceRoutes: Routes = [
  {
    path: 'facturas',
    component: ListComponent,
    data: {
      breadcrumb: 'Facturas',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
