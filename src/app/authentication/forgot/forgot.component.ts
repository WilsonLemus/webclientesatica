import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TokenPayload, AuthenticationService } from '../authentication.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';

@Component({
    templateUrl: './forgot.component.html'
})
export class ForgotComponent implements OnInit {

    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;

    constructor(private router: Router,
        private auth: AuthenticationService, private toastyService: ToastyService) { }
        credentials: TokenPayload = {
        email: '',
        password: ''
    };

    ngOnInit() {

    }

    login() {
        this.auth.login(this.credentials).subscribe((data) => {
            if (data) {
                this.getIdUser(data);
            }
        }, (err) => {
            console.log('Error rec' + JSON.stringify(err));
        });
    }

    private getIdUser(data: any) {
        this.auth.getIdUserByEmail(this.credentials.email, data.sessionId).subscribe((dataId) => {
            if (dataId[0]) {
                this.getEnterpraise(dataId, data);
            } else {
                alert('El usuario ingresado no se encuentra en nuestra base de datos');
                /*this.addToast({
                    title: 'Error al recuperar la cuenta',
                    msg: 'El usuario ingresado no se encuentra en nuestra base de datos ',
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom-right',
                    type: 'warning'
                });*/
            }
        }, (err) => {
            alert('El usuario ingresado no se encuentra en nuestra base de datos');
           /*this.addToast({
                title: 'Error al recuperar la cuenta',
                msg: 'El usuario ingresado no se encuentra en nuestra base de datos!',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'error'
            });*/
            return;
        });
    }

    private getEnterpraise(dataId: any, data: any) {
        this.auth.runActionService(dataId[0][0], data.sessionId).subscribe((dataEnt) => {
            if (dataEnt) {
                alert('Por favor verifique su correo electrónico donde recibirá la contraseña para ingresar');
                this.router.navigateByUrl('/');
            }else {
                alert('El usuario ingresado no se encuentra en nuestra base de datos');
                this.addToast({
                    title: 'Error al recuperar la cuenta',
                    msg: 'El usuario ingresado no se encuentra en nuestra base de datos',
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom-right',
                    type: 'warning'
                });
                return;
            }
        }, (err) => {
            this.addToast({
                title: 'Error al recuperar la cuenta',
                msg: 'El usuario ingresado no se encuentra en nuestra base de datos',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'warning'
            });
            return;
        });
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}
