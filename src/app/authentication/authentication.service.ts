import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators/map';
import { Router } from '@angular/router';
import { getUrlEnvironment } from '../shared/environment';

export interface UserDetails {
    id: string;
    streetAddr1: string;
    email: string;
    phone: string;
    Pagina_Web: string;
    R8184516: string;
    sucursal: string;
}

interface TokenResponse {
    sessionId: string;
    status: number;
}

interface TokenResponseid {
    id: string;
}
export interface Sucursal {
    id: string;
    name: string;
}

export interface TokenPayload {
    email: string;
    password: string;
    name?: string;
}

export interface Result {
    status: string;
    result: string;
    id: string;
}

export interface Generic {
    id: number;
    label: string;
    data: string;
}

@Injectable()
export class AuthenticationService {
    private id: string;
    private token: string;

    constructor(private http: HttpClient, private router: Router) { }

    private saveToken(token: string): void {
        this.setLocalItem('mean-token', token, this.getTimeExpire());
        this.token = token;
    }

    public getToken(): string {
        let LocalItem = this.getLocalItem('mean-token');
        return LocalItem;
    }

    private saveId(id: string): void {
        this.setLocalItem('mean-id', id, this.getTimeExpire());
        this.id = id;
    }
    private saveSucursal(id: string): void {
        this.setLocalItem('sucursal', id);
       // console.log(this.setLocalItem('sucursal', id));
        this.id = id;
    }

    public getLinea(): string {
        let id = this.getLocalItem('linea');
        return id;
    }

    private saveContactoId(id: string): void {
        this.setLocalItem('contactoId', id, this.getTimeExpire());
    }

    private saveContactoNombre(nombre: string): void {
        this.setLocalItem('nombreContacto', nombre);
    }

    public getContactoNombre(): string{
        let nombre = this.getLocalItem('nombreContacto');
        return nombre;
    }

    setLocalItem(key, id, time?) {
        time = time || 0;
        let timestamp = this.setTimeToLive(time);
        let storageObj = {
            'timestamp': timestamp,
            'value': id
        };
        sessionStorage.setItem(key, JSON.stringify(storageObj));
    }

    //funcion get
    getLocalItem(key) {
        let storage = sessionStorage.getItem(key);
        let item = null;

        if (storage) {
            item = JSON.parse(storage);
        }

        if (item) {
            let time = this.evaluateTimeToLive(item.timestamp);
            if (time) {
                return item.value;
            } else {
                sessionStorage.removeItem(key);
                return false;
            }
        }
        else {
            return false;
        }
    }

    setTimeToLive(lifespan) {
        let currentTime = new Date().getTime();
        let timeToLive;
        if (lifespan !== 0) {
            timeToLive = currentTime + lifespan;
        } else {
            timeToLive = 0;
        }
        return timeToLive;
    }

    // obtiene la fecha actual y suma el tiempo limite de sesión
    getTimeExpire() {
        var min = 60 * 1000;
        var after = 30 * min;
        return after;
    }

    //Evalua el tiempo determina para la sesión
    evaluateTimeToLive(timestamp) {
        let currentTime = new Date().getTime();
        if (currentTime <= timestamp || timestamp === 0) {
            return true;
        } else {
            return false;
        }
    }

    // tslint:disable-next-line:comment-format
    //remove the item session from the storage
    removeLocalItem(key) {
        return sessionStorage.removeItem(key);
    }

    public getId(): string {
        let id = this.getLocalItem('mean-id');
        return id;
    }



    public getUserDetails(): UserDetails {
        const token = this.getToken();
        let payload;
        if (token) {
            payload = token.split('.')[1];
            payload = window.atob(payload);
            return JSON.parse(payload);
        } else {
            return null;
        }
    }

    public getIdUserLoggedIn(): string {
        return this.getId();
    }


    public isLoggedIn(): boolean {
        const user = this.getToken();
        if (user) {
            return true;
        } else {
            return false;
        }
    }

    private request(method: 'post' | 'get', type: 'login' | 'register' | 'profile', user?: TokenPayload): Observable<any> {
        let base;

        if (method === 'get') {
            base = this.http.get(`${getUrlEnvironment()}rest/api/login?loginName=industria.cliente&password=Cliente&output=json`); //, user);
        }

        const request = base.pipe(
            map((data: TokenResponse) => {
                return data;
            })
        );
        return request;
    }

    public getIdUser(username: string, password: string, sessionId: string): Observable<any> {
        //let query = encodeURI("select R8184516,name,Correo_Electronico,Telefono_movil,NombreCiudad,contrasea,id,NombreDepartamento,R10736995 from Contactos where Correo_Electronico='") + username + encodeURI("'") + " and Contrasea='" + password + encodeURI("'") + " ORDER BY name WITH (NOLOCK)";
        let query = encodeURI("select R8184516,name,Correo_Electronico,Telefono_movil,NombreCiudad,contrasea,id,NombreDepartamento,R10852641,Tipo_Contacto from Contactos where Correo_Electronico='") + username + encodeURI("'") + " and Contrasea='" + password + encodeURI("'") + " ORDER BY name";
        let url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionId + "&output=json&maxRows=1";
        let base = this.http.get(url);
        const request = base.pipe(
            map((data) => {
                if ((data != "") && (data != undefined)) {
                    this.SaveProfile(JSON.stringify(data[0]));
                    this.saveContactoId(data[0][6]);
                    this.saveContactoNombre(data[0][1]);
                    this.saveId(data[0][0]);
                    this.saveToken(sessionId);
                    this.getSuc(data[0][6], sessionId);
                    sessionStorage.setItem("tipoContacto", data[0][9]);
                    sessionStorage.setItem("LineaNegocio", data[0][8]);
                    sessionStorage.setItem("correoUsuario", data[0][2]);
                } else {
                    this.saveContactoId('');
                    this.SaveProfile("");
                    this.saveId("");
                    this.saveToken("");
                    this.saveSucursal("");
                    return Observable.throw("Error al loguear");
                }
                return data;
            })
        );
        return request;
    }
    public getSuc(idContacto, sessionId): Observable<any> {
        //let url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionId + "&output=json&maxRows=1";
        // if (getUrlEnvironment() == "http://qa.i4soluciones.com/") {
            // var url = `${getUrlEnvironment()}rest/api/getRelationships?&output=json&id=${idContacto}&relName=R10736995&fieldList=id,name&sessionId=${sessionId}`;
        // } else {
            // var url = `${getUrlEnvironment()}rest/api/getRelationships?&output=json&id=${idContacto}&relName=R12388444&fieldList=id,name&sessionId=${sessionId}`;
        // }
        //  console.log(url);
        var url = `${getUrlEnvironment()}rest/api/getRelationships?objName=Contactos&id=` + idContacto + '&relName=R12388444&fieldList=id,name&sessionId=' + this.getToken() + '&output=json';           

        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: any) => {
                let ids1 = "";
                for (var i = 0; i < data.length; i++) {
                    if (i == ((data.length) - 1)) {
                        ids1 += data[i]["id"];
                    } else {
                        ids1 += data[i]["id"] + ",";
                    }
                }
                //alert("sucursales:"+ids1);
                sessionStorage.setItem("idSucursales", ids1);
                return data;
            })
        );
        return request;
    }


    public getIdUserByEmail(username: string, sessionId: string): Observable<any> {
        let query = encodeURI("select id,name,Correo_Electronico,Telefono_movil,NombreCiudad,contrasea,id,NombreDepartamento from Contactos where Correo_Electronico='") + username + encodeURI("'");
        let url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionId + "&output=json&maxRows=1";
        let base = this.http.get(url);
        const request = base.pipe(
            map((data) => {
                if ((data != "") && (data != undefined)) {
                    return data;
                } else {
                    return Observable.throw("Error al loguear");
                }
            })
        );
        return request;
    }

    public runActionService(id: string, token: string): Observable<any> {
        try {
            const url = `${getUrlEnvironment()}rest/api/runAction?sessionId=${token}&objName=Contactos&id=${id}&actionId=0V05E-s7SLC6lcY1YzdZUg&output=json`;
            return this.http.post(url, {})
                .map(response => {
                    return response;
                });
        } catch (err) {
            console.log(err);
        }
    }

    public getEnterpriseId(idCustomer: string, sessionid: string): Observable<any> {
        // solo status = Creado o con este id: 8168024 lo deja pasar
        // para pasar a prod se debe quitar R9961501 del querie
        const query = encodeURI(`select id,Razon_Social,streetAddr1,email, phone,Pagina_Web,R8184516,R8156824,R8098496,Nit,Cod__Verificacion,NombreDepartamento,NombreCiudad,Pago_Anticipo,Servicio_Posfechado,Presenciales,Recoleccion,Recepcion,1, R8173502 from Clientes where id=${idCustomer} and status = 8168024 ORDER BY id`);
        //const query = encodeURI(`select id,Razon_Social,streetAddr1,email, phone,Pagina_Web,R8184516,R8156824,R8098496,Nit,Cod__Verificacion,NombreDepartamento,NombreCiudad,Pago_Anticipo,Servicio_Posfechado,Presenciales,Recoleccion,Recepcion,1, R8173502 from Clientes where id=${idCustomer} ORDER BY id WITH (NOLOCK)`);
        const url = `${getUrlEnvironment()}rest/api/selectQuery?query=` + query + "&sessionId=" + sessionid + "&output=json&maxRows=10000";
        let base;
        base = this.http.get(url);
        return base.pipe(
            map((res) => {
                if (res[0]) {
                    this.saveEnterpriseData(res);
                    this.getContactData(res[0][8], sessionid);
                }
                return res;
            })
        );
    }

    private saveEnterpriseData(entData) {

        var arreglodatos = [];
        for (var i = 0; i < entData[0].length; i++) {
            arreglodatos.push({
                llave: i,
                dato: entData[0][i]
            })
        }
        sessionStorage.setItem("EntrepriseDataJSON", JSON.stringify(arreglodatos));
        sessionStorage.setItem("EntrepriseData", entData);
    }

    public GetEnterpriseData() {
        return sessionStorage.getItem("EntrepriseData");
    }

    private getContactData(CommercialId: string, sessionid: string) {
        let query = encodeURI("select name,phone,mobilePhone,email from usuarios where id='") + CommercialId + encodeURI("'") + " ORDER BY name";
        let url = `${getUrlEnvironment()}rest/api/selectQuery?&query=` + query + "&sessionId=" + sessionid + "&output=json&maxRows=10000";
        this.http.get(url).subscribe(res => {
            if (res[0]) {
                this.SaveContactData(JSON.stringify(res[0]));
            }
        });
    }

    private SaveContactData(ContactData) {
        sessionStorage.setItem("ContactData", ContactData);
    }

    public GetContactData() {
        return (sessionStorage.getItem("ContactData"));
    }

    public setEmailLogin(email) {
        sessionStorage.setItem("emailLogin", email);
    }

    public getEmailLogin() {
        return sessionStorage.getItem("emailLogin");
    }

    // public setLineaNegocio(linea) {
    //     sessionStorage.setItem("lineaNegocio", linea);
    // }

    // public getLineaNegocio() {
    //     return sessionStorage.getItem("lineaNegocio");
    // }

    public setIdEvidencia(id) {
        sessionStorage.setItem("idEvidencia", id);
    }

    public getIdEvidencia() {
        return sessionStorage.getItem("idEvidencia");
    }

    private SaveProfile(profile) {
        sessionStorage.setItem("Profile", profile);
    }
    public GetProfile() {
        var profile = sessionStorage.getItem("Profile");
        return profile;
    }

    public register(user: TokenPayload): Observable<any> {
        return this.request('post', 'register', user);
    }

    public login(user: TokenPayload): Observable<any> {
        return this.request('get', 'login', user);
    }

    public profile(): Observable<any> {
        return this.request('get', 'profile');
    }

    public logout(): void {
        this.SaveProfile("");
        this.saveId("");
        this.saveToken("");
        this.saveSucursal("");
        sessionStorage.setItem("LineaNegocio", "");
        sessionStorage.setItem("LineaActual", "");
        sessionStorage.setItem("ln", "");
    }

    // PQS NO CREDENTIALS - CONSULTAS EMPRESA CLIENTE

    getGrupoAtica(idCliente){
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Clientes&id=` + idCliente + '&relName=R8183744&fieldList=id,name&sessionId=' + this.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    getCiudad() {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id,name,R11156120 from Municipios Order by name&sessionId=` + this.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    getRegional(idCiudad){
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select R11156120 from Municipios where id = ${idCiudad}&sessionId=` + this.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    public getSociedadVentas(){
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Sociedad_de_Ventas Order by name&sessionId=` + this.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // PQS NO CREDENTIALS - CREACIÓN DE PQS

    public savePqsNoCredential(pqs: any, Formlen: number): Observable<any> {
        return this.createPqsNoCredential(pqs, Formlen);
    }

    private createPqsNoCredential(pqs: any, formlen): Observable<any> {
        let base;
        for (let i = 0; i < formlen; i++) {
            let url = this.getQueryCreateNoCredentials(pqs, i);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }
    }

    private getQueryCreateNoCredentials(pqs: any, i: number) {
        const token = this.getToken();
        let params = `&Nombre_y_Apellido=${pqs.itemRows[i].nombre}&Cdula_de_ciudadana=${pqs.itemRows[i].cedula}&email=${pqs.itemRows[i].email}&Celular=${pqs.itemRows[i].celular}&Empresa_a_la_que_representa=${pqs.itemRows[i].nombreEmpresa}&NIt_de_la_empresa=${pqs.itemRows[i].nit}&R11158973=${pqs.itemRows[i].ciudad}&R10778047=${pqs.itemRows[i].grupoAtica}`;
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Clientes_no_registrados&sessionId=` + token + params;
        return url;
    }

    // PQS NO CREDENTIALS - CONSULTAS PQS

    public getTipoPqs() {
        let url = `${getUrlEnvironment()}rest/api/getPicklist?output=json&objDefId=9483815&fieldDefId=9483935&sessionId=` + this.getToken();
        let base;
        base = this.http.get(url);
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    public getProceso() {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from Proceso_PQS order by name &sessionId=` + this.getToken() + '&output=json&maxRows=100000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    public getServicio(idProceso) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Proceso_PQS&id=` + idProceso + '&relName=R9562882&fieldList=id,name&sessionId=' + this.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }
    public getMotivo(idServicio) {
        let base;
        base = this.http.get(`${getUrlEnvironment()}rest/api/getRelationships?objName=Tipo_de_producto_pqs&id=` + idServicio + `&relName=R9562872&fieldList=id,name&sessionId=` + this.getToken() + '&output=json');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

    // PQS NO CREDENTIALS - CREACIÓN DE PQS

    public savePqs(pqs: any, Formlen: number, regional: number): Observable<any> {
        return this.createPqs(pqs, Formlen, regional);
    }

    private createPqs(pqs: any, formlen, regional): Observable<any> {
        let base;
        for (let i = 0; i < formlen; i++) {
            let url = this.getQueryCreate(pqs, i, regional);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                    return data;
                })
            );
            return request;
        }
    }

    private getQueryCreate(pqs: any, i: number, regional) {
        const token = this.getToken();
        let idSucursal = sessionStorage.getItem('idSucursales');
        const cliente = this.getIdUserLoggedIn();
        const contacto = JSON.parse(sessionStorage.getItem('contactoId')).value;
        const procedencia = 10994962;
        let params = `&R9976048=${+regional}&Cliente_no_registrado=True&Procedencia=${+procedencia}&R9493393=${+contacto}&Tipo_PQS=${+pqs.itemRows[i].tipoPqs}&R11159013=${+pqs.itemRows[i].proceso}&R11159017=${+pqs.itemRows[i].servicio}&R10821020=${+pqs.itemRows[i].motivo}&Detalle=${pqs.itemRows[i].detalle}&R9493390=${cliente}&R9976048 in(${idSucursal})`;
        var url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=PQS&sessionId=` + token + params;
        return url;
    }

    private saveDocument(id): Observable<any> {
        let base;
        try {
            let url = this.getSaveDocument(id);
            base = this.http.get(url);
            const request = base.pipe(
                map((data: Result) => {
                 //   console.log(data);
                    return data;
                })
            );
            return request;

        } catch (err) {
            console.log(err);
        }
    }

    private getSaveDocument(idPqs: any) {
        const token = this.getToken();
        const url = `${getUrlEnvironment()}rest/api/create2?output=json&useIds=true&objName=Evidencia_PQS&sessionId=${token}&Descripcion=Evidencia&R9961708=` + idPqs;
        return url;
    }

    public saveIdDocument(id): Observable<any> {
        return this.saveDocument(id);
    }

    encodeQueryData(data) {
        try {
            const ret = [];
            for (const d in data)
                ret.push(encodeURIComponent(d) + '=' + encodeURIComponent(data[d]));
            return ret.join('&');
        } catch (s) {
            alert('s ' + s);
        }
    }

    buildUrlParams(object) {
        try {
            const urlParams = [];
            for (const key in object) {
                if (object.hasOwnProperty(key)) {
                    urlParams.push(key + '=' + object[key]);
                }
            }
            return urlParams.join('&');
        } catch (r) {
            alert(r);
        }
    }

    public loadDocument(content) {
        const tok = this.getToken();
        var xhr = new XMLHttpRequest();
        var url = `${getUrlEnvironment()}rest/api/setBinaryData?output=json&sessionId=` + tok;
        var params = this.buildUrlParams(content);
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

        xhr.onreadystatechange = function () {
            if (xhr.readyState == 4 && xhr.status == 200) {

            }
        }
        var eqd = this.encodeQueryData(content);

        xhr.onprogress = function (e) {
            try {
                if (e.lengthComputable) {
                    var percentComplete = (e.loaded / e.total) * 100;
                }
            } catch (f) {
                alert("f " + f);
            }
        };

        xhr.onload = function (a) {
            console.log("onload");
        };
        xhr.onerror = function () {
            console.log("error");
        };
        xhr.onabort = function () {
            console.log("abort");
        };
        xhr.ontimeout = function () {
            console.log("timeout");
        };
        xhr.onloadstart = function () {
        };
        xhr.onloadend = function () {
        };
        xhr.send(eqd);
    }

    public getPqs(id) {
        //console.log("ELDELPLAN", id);
        let base = this.http.get(`${getUrlEnvironment()}rest/api/selectQuery?query=select id, name from PQS where id=${id} &sessionId=` + this.getToken() + '&output=json&maxRows=10000');
        const request = base.pipe(
            map((data: Generic) => {
                return data;
            })
        );
        return request;
    }

}
