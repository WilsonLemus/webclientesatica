    import { Component, OnInit } from '@angular/core';
    import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
    import { Router } from '@angular/router';
    import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
    import { AuthenticationService, Generic } from '../authentication.service';
    import { Result } from '../../services/services.service';
    import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

    @Component({
        selector: 'app-pqs',
        templateUrl: './pqs-no-credentials.component.html'
    })

    export class PqsNoCredential implements OnInit {
        pqsForm = new FormGroup({
            // DATOS PERSONALES 
            nombre: new FormControl(''),
            cedula: new FormControl(''),
            email: new FormControl(''),
            celular: new FormControl(''),
            // DATOS EMPRESA
            nombreEmpresa: new FormControl(''),
            nit: new FormControl(''),
            ciudad: new FormControl(''),
            grupoAtica: new FormControl(''),
            terminos: new FormControl(''),
            // DATOS PQS
            tipoPqs: new FormControl(''),
            proceso: new FormControl(''),
            servicio: new FormControl(''),
            motivo: new FormControl(''),
            detalle: new FormControl(''),
            evidencia: new FormControl('')
        });
        // DATOS PERSONALES 
        public nombre: any;
        public cedula: any;
        public email: any;
        public celular: any;
        // DATOS EMPRESA
        public nombreEmpresa: any;
        public ciudad: any;
        public grupoAtica: any;
        public terminos: any;
        public valueCheck = 0;
        // DATOS PQS
        public tipoPqs: any;
        public proceso: any;
        public servicio: any;
        public motivo: any;
        public detalle: any;
        public result: Result;
        public generic: Generic;
        public doc;
        public vall;
        public regional: any;
        pqs: Array<Generic>;
        position = 'bottom-right';
        title: string;
        msg: string;
        showClose = true;
        timeout = 5000;
        theme = 'bootstrap';
        type = 'default';
        closeOther = false;

        constructor(private AuthenticationService: AuthenticationService, private toastyService: ToastyService, private modalService: NgbModal,
            private router: Router, private auth: AuthenticationService, private _fb: FormBuilder, ) { }

        // Funciones que se realizaran al cargar la pantalla.

        ngOnInit() {
            this.pqsForm = this._fb.group({
                itemRows: this._fb.array([this.initItemRows()])
            });

            this.getCiudad();
            this.getTipoPqs();
            this.getProcesos();
            this.getSociedadVentas();
        }

        // Trae la sociedad de ventas de rollbase.

        getSociedadVentas(){
            this.auth.getSociedadVentas().subscribe(sociedad => {
                this.grupoAtica = sociedad;
            }, (err) =>{ 
                console.log(err);
            });
        }

        // REGIONAL

        getCiudad(){
            this.auth.getCiudad().subscribe(regional => {
                this.ciudad = regional;
            }, (err) => {
                console.log(err);
            });
        }

        // TIPO PQS

        getTipoPqs(){
            this.auth.getTipoPqs().subscribe(tipoPqs => {
                this.tipoPqs = tipoPqs;
            }, (err) => {
                console.error(err);
            });
        }

        // PROCESOS PQS

        getProcesos(){
            this.auth.getProceso().subscribe(procesos => {
                this.proceso = procesos;
            }, (err) => {
                console.log(err);
            });
        }

        // REGIONAL

        getRegional(idCiudad){
            this.auth.getRegional(idCiudad).subscribe(regional => {
                this.regional = regional[0];
            }, (err) => {
                console.log(err);
            });
        }

        // Validación de los caracteres del correo.
        
        verificarEmail(event){
            console.log(event);
            if (!this.validateEmail(event)) {
                alert("El formado de correo debe ser: xxxx@xxxx.xxxx \n Por favor, ingrese un correo valido.");
            }
        }

        validateEmail(email) {
            var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(email);
          }

        // Logout

        logout() {
            this.auth.logout();
            this.login();
        }

        login() {
            this.router.navigate(['/']);
        }

        /* Direccionar al formulario PQS */

        formularioPqs() {
            let Mens = 'Su PQS se registro correctamente';
            alert(Mens);
            this.auth.logout();
            this.login();
        }
        /**
        * Inicia el formulario con el modelo de solicitud
        */
        initItemRows() {
            return this._fb.group({
                nombre: ['', [<any>Validators.required]],
                cedula: ['', [<any>Validators.required, <any>Validators.min(1)]],
                email: ['', [<any>Validators.required, <any>Validators.email]],
                celular: [''],
                nombreEmpresa: ['', [<any>Validators.required]],
                nit: ['', [<any>Validators.required]],
                ciudad: ['', [<any>Validators.required]],
                grupoAtica: ['', [<any>Validators.required]],
                terminos: [this.valueCheck, [<any>Validators.requiredTrue]],
                tipoPqs: ['', [<any>Validators.required]],
                proceso: ['', [<any>Validators.required]],
                servicio: ['', [<any>Validators.required]],
                motivo: [''],
                detalle: ['', [<any>Validators.required]],
                evidencia: ['']
            });
        }

        // Guarda la PQS sin credenciales en rollbase

        createPQSNoCredential() {
            const formlen = this.pqsForm.value.itemRows.length;
            this.AuthenticationService.savePqsNoCredential(this.pqsForm.value, formlen).subscribe(
                Services => {
                    this.result = Services;
                }
            );
            this.AuthenticationService.savePqs(this.pqsForm.value, formlen, this.regional).subscribe(
                Services => {
                    this.result = Services;
                    this.AuthenticationService.saveIdDocument(Services.id).subscribe(
                        doc => {
                            if (this.doc) {
                                this.doc.id = doc.id;
                                this.AuthenticationService.loadDocument(this.doc);
                                this.formularioPqs();
                            }
                            this.result = doc;
                        }
                    );
                }
            )
        }

        // FORMULARIO PQS


        // Guarda documentos del formulario

        saveDocumento(id) {
            if (this.doc) {
                this.doc.id = id;
                this.auth.loadDocument(this.doc);
            }
        }
        onFileChange(event) {
            console.log("MY FILE: ", event.target.files)
            let reader = new FileReader();
            if (event.target.files && event.target.files.length > 0) {
                let file = event.target.files[0];
                reader.readAsDataURL(file);
                reader.onload = () => {
                    this.doc = {
                        id: '',
                        fieldName: 'Adjunto',
                        objName: 'Evidencia_PQS',
                        fileName: file.name,
                        contentType: file.type,
                        value: reader.result.split(',')[1]
                    }
                    this.vall = (this.doc) ? this.doc.value : '';
                };
            }
        }

        // Obtener el Motivo
        cargaMotivo(idServicio) {
            this.auth.getMotivo(idServicio).subscribe(motivo => {
                this.motivo = motivo;
            }, (err) => {
                console.error(err);
            });
        }

        // Carga los seriviocs de rollbase

        cargaServicio(idProceso) {
            this.auth.getServicio(idProceso).subscribe(servicio => {
                this.servicio = servicio;
            }, (err) => {
                console.error(err);
            });
        }

        // Notificaciones flotantes.

        addToast(options) {
            if (options.closeOther) {
                this.toastyService.clearAll();
            }
            this.position = options.position ? options.position : this.position;
            const toastOptions: ToastOptions = {
                title: options.title,
                msg: options.msg,
                showClose: options.showClose,
                timeout: options.timeout,
                theme: options.theme,
                onAdd: (toast: ToastData) => {
                    console.log('Toast ' + toast.id + ' has been added!');
                },
                onRemove: (toast: ToastData) => {
                    console.log('Toast ' + toast.id + ' has been added removed!');
                }
            };

            switch (options.type) {
                case 'default': this.toastyService.default(toastOptions); break;
                case 'info': this.toastyService.info(toastOptions); break;
                case 'success': this.toastyService.success(toastOptions); break;
                case 'wait': this.toastyService.wait(toastOptions); break;
                case 'error': this.toastyService.error(toastOptions); break;
                case 'warning': this.toastyService.warning(toastOptions); break;
            }
        }

        saveGetOut() {
            this.createPQSNoCredential();
        }
    }
