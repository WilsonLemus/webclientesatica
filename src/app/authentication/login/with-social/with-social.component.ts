import { Component, OnInit } from '@angular/core';
import { AuthenticationService, TokenPayload } from '../../authentication.service';
import { Router } from '@angular/router';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { Alert } from 'selenium-webdriver';

@Component({
    // selector: 'app-with-social',
    templateUrl: './with-social.component.html'
})

export class WithSocialComponent implements OnInit {
    position = 'bottom-right';
    title: string;
    msg: string;
    showClose = true;
    timeout = 5000;
    theme = 'bootstrap';
    type = 'default';
    closeOther = false;
    public sucursal;
    public pqsNoCredentials: any;

    credentials: TokenPayload = {
        email: localStorage.getItem("name"),
        password: ''
    };

    constructor(private auth: AuthenticationService, private router: Router, private toastyService: ToastyService) { }

    ngOnInit() {
        let user = localStorage.getItem("name");
        if (user) {
            var element = <HTMLInputElement>document.getElementById("check");
           // element.checked = true;
        }
        this.auth.logout();
    }
    obtenerCookie() {
        var userr = (<HTMLInputElement>document.getElementById("name")).value;
        var element = <HTMLInputElement>document.getElementById("check");
        var isChecked = element.checked;
        if (isChecked == true) {
            localStorage.setItem("name", userr);
        } else {
            localStorage.setItem("name", "");
        }
    }
    /**
     * Acción de loginn
     */
    login() {
        this.auth.login(this.credentials).subscribe((data) => {
            if (data) {
                this.getIdUser(data);
            }
        }, (err) => {
            console.log("Error rec" + JSON.stringify(err));
        });
    }

    loginPqsNoCredential() {
        this.pqsNoCredentials = 1;
        this.credentials = {
            email: 'cliente.temporal.atica@gmail.com',
            password: '12345678'
        }
        this.auth.login(this.credentials).subscribe((data) => {
            if (data) {
                this.getIdUserPqsNoCredential(data);
            }
        }, (err) => {
            console.log("Error rec" + JSON.stringify(err));
        });
    }

    /**
     * Obtener la info del usuario
     * @param data
     */
    private getIdUser(data: any) {
        this.auth.getIdUser(this.credentials.email, this.credentials.password, data.sessionId).subscribe((dataId) => {
            if (dataId[0]) {
                this.getEnterpraise(dataId, data);
            } else {
                this.addToast({
                    title: 'Error de inicio de sesión',
                    msg: 'El usuario ingresado no se encuentra habilitado ',
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom-right',
                    type: 'warning'
                });
            }
            this.getSuc(dataId[0][6], data.sessionId);
        }, (err) => {
            this.addToast({
                title: 'Error de inicio de sesión',
                msg: 'Datos de inicio de sesión incorrectos!',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'error'
            });
            return;
        });
    }

    private getIdUserPqsNoCredential(data: any) {
        this.auth.getIdUser(this.credentials.email, this.credentials.password, data.sessionId).subscribe((dataId) => {
            if (dataId[0]) {
                this.getEnterpraisePqsNoCredential(dataId, data);
            }
            this.getSuc(dataId[0][6], data.sessionId);
        });
    }
    //Obtienes la sucursal del contacto
    private getSuc(id, sessionId) {

        this.auth.getSuc(id, sessionId).subscribe(suc => {
            this.sucursal = suc;
            return suc;
        })
    }

    /**
     * Obtiene la info de la compañía del usuario
     * @param dataId
     * @param data
     */
    private getEnterpraise(dataId: any, data: any) {
        this.auth.getEnterpriseId(dataId[0][0], data.sessionId).subscribe((dataEnt) => {
            if (dataEnt[0]) {
                // this.auth.setLineaNegocio(dataEnt[0][7]);
                this.auth.setEmailLogin(this.credentials.email);
                this.router.navigateByUrl('/dashboard');
            } else {
                this.addToast({
                    title: 'Error de inicio de sesión',
                    msg: 'El usuario ingresado no a sido creado en SAP',
                    timeout: 5000,
                    theme: 'default',
                    position: 'bottom-right',
                    type: 'warning'
                });
                return;
            }

        }, (err) => {
            this.addToast({
                title: 'Error de inicio de sesión',
                msg: 'El usuario ingresado no a sido creado en SAP',
                timeout: 5000,
                theme: 'default',
                position: 'bottom-right',
                type: 'warning'
            });
            return;
        });
    }

    private getEnterpraisePqsNoCredential(dataId: any, data: any) {
        this.auth.getEnterpriseId(dataId[0][0], data.sessionId).subscribe((dataEnt) => {
            if (dataEnt[0]) {
                // this.auth.setLineaNegocio(dataEnt[0][7]);
                this.auth.setEmailLogin(this.credentials.email);
                setTimeout(() => {
                    this.router.navigateByUrl('/authentication/pqs-no-credentials');
                }, 5000);
            }
        });
    }

    addToast(options) {
        if (options.closeOther) {
            this.toastyService.clearAll();
        }
        this.position = options.position ? options.position : this.position;
        const toastOptions: ToastOptions = {
            title: options.title,
            msg: options.msg,
            showClose: options.showClose,
            timeout: options.timeout,
            theme: options.theme,
            onAdd: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added!');
            },
            onRemove: (toast: ToastData) => {
                console.log('Toast ' + toast.id + ' has been added removed!');
            }
        };

        switch (options.type) {
            case 'default': this.toastyService.default(toastOptions); break;
            case 'info': this.toastyService.info(toastOptions); break;
            case 'success': this.toastyService.success(toastOptions); break;
            case 'wait': this.toastyService.wait(toastOptions); break;
            case 'error': this.toastyService.error(toastOptions); break;
            case 'warning': this.toastyService.warning(toastOptions); break;
        }
    }
}

