import { Routes } from '@angular/router';
import { QuotationComponent } from './quotation.component';
import { ListComponent } from './list.component';
import { AuthGuardService } from '../authentication/auth-guard.service';

export const QuotationRoutes: Routes = [
  {
    path: 'create',
    component: QuotationComponent,
    data: {
      breadcrumb: 'Cotización',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: 'listado-cotizaciones',
    component: ListComponent,
    data: {
      breadcrumb: 'Listado de cotizaciones',
      status: true
    },
    canActivate: [ AuthGuardService ]
  },
  {
    path: '**',
    redirectTo: 'error/404'
  }
];
