import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ServicesService, Services, Generic, Result, Status } from '../services/services.service';
import { AuthenticationService } from '../authentication/authentication.service';
import { getUrlEnvironment } from '../shared/environment';

@Component({
  selector: 'app-datatables-products',
  templateUrl: './list.component.html',
  styleUrls: ['./quotation.component.css']
})
export class ListComponent implements OnInit {

    // http://plnkr.co/edit/PxBaZs?p=preview
    public data;
    public filterQuery = '';
    public rowsOnPage = 5;
    public sortBy = 'id';
    public sortOrder = 'asc';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private docServ: ServicesService,
    private auth: AuthenticationService
  ) {}

  ngOnInit() {
    this.docServ.listQoute().subscribe(materials => {
      this.data = materials;
    }, (err) => {
        console.error(err);
    });
  }

  public sortByWordLength = (a: any) => {
    return a.id.length;
  }

  goToUrl(id) {
    let url =`${getUrlEnvironment()}rest/api/getBinaryData?objName=Cotizacin_Nuevo_Material&id=`+id+"&fieldName=HojaSeguridad&sessionId=" + this.auth.getToken();
    window.open(url, '_blank');
  }

  openSecure(){
    let url = `${getUrlEnvironment()}storage/servlet/Image?c=8012571&fileName=tmp_1175069585030383508.pdf&contentType=application%2Fpdf&suggestedName=Hoja+de+Seguridad+de+Materiales.pdf`;
    window.open(url, '_blank');
  }

}
