import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { QuotationComponent } from './quotation.component';
import { ListComponent } from './list.component';
import { RouterModule } from '@angular/router';
import { QuotationRoutes } from './quotation.routing';
import { AuthenticationModule } from '../authentication/authentication.module';
import { AuthenticationService } from '../authentication/authentication.service';
import { HttpModule } from '@angular/http';
import { SharedModule } from '../shared/shared.module';
import { ServicesService } from '../services/services.service';
import { HttpClientModule } from '@angular/common/http';
import {DataTableModule} from 'angular2-datatable';
import { AuthGuardService } from '../authentication/auth-guard.service';
import {AngularEchartsModule} from 'ngx-echarts';

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(QuotationRoutes),
    SharedModule,
    HttpClientModule,
    AuthenticationModule,
    DataTableModule,
    AngularEchartsModule
  ],
  providers: [
    ServicesService,
    AuthenticationService,
    AuthGuardService
  ],
  declarations: [QuotationComponent, ListComponent]
})
export class QuotationModule { }
