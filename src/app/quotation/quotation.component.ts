import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { ServicesService, Services, Generic, Result, Status } from '../services/services.service';
import { ToastyService, ToastOptions, ToastData } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-quotation',
  templateUrl: './quotation.component.html',
  styleUrls: ['./quotation.component.css']
})
export class QuotationComponent implements OnInit {

  public form: FormGroup;
  position = 'bottom-right';
  title: string;
  msg: string;
  showClose = true;
  timeout = 5000;
  theme = 'bootstrap';
  type = 'default';
  closeOther = false;
  public doc;
  public vall;

  constructor(private fb: FormBuilder, private docServ: ServicesService, private toastyService: ToastyService, private router: Router) { }


  ngOnInit() {
    this.form = this.newElement();
  }

  /**
   * Prepara el formulario para la cotización
   */
  newElement(){
    return this.fb.group({
      cotizacion: [null, Validators.compose([Validators.required])],
      secure: ['']
    });
  }

  /**
   * carga el adjunto de la cotización
   * @param event
   */
  onFileChange(event) {
    let reader = new FileReader();
    if (event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.doc = {
          id: '',
          fieldName: 'HojaSeguridad',
          fileName: file.name,
          contentType: file.type,
          value: reader.result.split(',')[1]
        }
        this.vall = (this.doc) ? this.doc.value : '';
      };
    }
  }

  deleteRow(index: number) {
    const control = <FormArray>this.form.controls['itemRows'];
    control.removeAt(index);
  }

  addNewRow(form: any) {
    alert(form);
    this.sendaction(form, false);
  }

  save(form: any) {
    this.sendaction(form, true);
  }

  /**
   * Guarda la cotizacion
   * @param form
   * @param redirect
   */
  sendaction(form: any, redirect){
    if (form.cotizacion != '') {
      this.docServ.createQuotation(form.cotizacion).subscribe(Services => {
        if (Services.status === 'ok') {
          if(this.doc){
            this.doc.id = Services.id;
            this.docServ.loadSecure(this.doc);
          }
          this.addToast({
            title: 'Acción exitosa',
            msg: 'La cotización se creó exitosamente',
            timeout: 5000,
            theme: 'default',
            position: 'top-right',
            type: 'success'
          });
        }
        if(redirect){
          setTimeout(() => {
            this.router.navigate(['/cotizacion/listado-cotizaciones']);
          }, 2000);
        }else{
          this.ngOnInit();
          this.addToast({
            title: 'Información',
            msg: 'Puede agregar la nueva cotización',
            timeout: 5000,
            theme: 'default',
            position: 'top-right',
            type: 'info'
          });

        }
      });
    } else {
      this.addToast({
        title: 'acción requerida',
        msg: 'Se debe ingresar la información de la cotización',
        timeout: 5000,
        theme: 'default',
        position: 'bottom-right',
        type: 'warning'
      });
    }
  }

  addToast(options) {
    if (options.closeOther) {
      this.toastyService.clearAll();
    }
    this.position = options.position ? options.position : this.position;
    const toastOptions: ToastOptions = {
      title: options.title,
      msg: options.msg,
      showClose: options.showClose,
      timeout: options.timeout,
      theme: options.theme,
      onAdd: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added!');
      },
      onRemove: (toast: ToastData) => {
        console.log('Toast ' + toast.id + ' has been added removed!');
      }
    };

    switch (options.type) {
      case 'default': this.toastyService.default(toastOptions); break;
      case 'info': this.toastyService.info(toastOptions); break;
      case 'success': this.toastyService.success(toastOptions); break;
      case 'wait': this.toastyService.wait(toastOptions); break;
      case 'error': this.toastyService.error(toastOptions); break;
      case 'warning': this.toastyService.warning(toastOptions); break;
    }
  }
}
